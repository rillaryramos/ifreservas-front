import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-visualizar-auditoria',
  templateUrl: 'visualizar-auditoria.component.html',
})
export class VisualizarAuditoriaComponent {

  constructor(
    public dialogRef: MatDialogRef<VisualizarAuditoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

}
