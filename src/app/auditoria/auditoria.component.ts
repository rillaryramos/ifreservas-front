import { UsuarioService } from './../core/services/usuario.service';
import { FormControl } from '@angular/forms';
import { VisualizarAuditoriaComponent } from './visualizar-auditoria/visualizar-auditoria.component';
import { TrilhaAuditoriaService } from './../core/services/trilha-auditoria.service';
import { TrilhaAuditoria } from './../core/models/trilha-auditoria.model';
import { MotoristaService } from './../core/services/motorista.service';
import { Title } from '@angular/platform-browser';
import { Motorista } from './../core/models/motorista.model';
import { MatTableDataSource, MatDialog, MatPaginator } from '@angular/material';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import * as moment from 'moment';
import { Usuario } from '../core/models/usuario.model';

@Component({
  selector: 'app-auditoria',
  templateUrl: './auditoria.component.html',
  styleUrls: ['./auditoria.component.scss']
})
export class AuditoriaComponent implements OnInit, AfterViewInit {

  usuarios: Usuario[];
  selectedCategoria = 'all';
  selectedModulo = 'all';
  usuarioSelecionado: Usuario;
  maxDateInicial: Date;
  dataInicial = new FormControl(null);
  dataFinal = new FormControl(null);
  displayedColumns: string[] = ['data', 'categoria', 'modulo', 'descricao', 'usuario', 'actions'];
  trilhas = new MatTableDataSource<TrilhaAuditoria>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('busca') busca: ElementRef;

  constructor(
    private trilhaAuditoriaService: TrilhaAuditoriaService,
    private title: Title,
    public dialog: MatDialog,
    private usuarioService: UsuarioService
  ) {
    this.maxDateInicial = new Date();
  }

  ngOnInit() {
    this.title.setTitle('Trilha de auditoria');
    this.listarTrilhas();
    this.listarUsuarios();
  }

  ngAfterViewInit() {
    this.trilhas.paginator = this.paginator;
  }

  listarTrilhas() {
    this.trilhaAuditoriaService.getTrilhas()
      .subscribe((trilhas: any) => {
        console.log(trilhas);
        this.trilhas.data = trilhas;
      });
  }

  listarUsuarios() {
    this.usuarioService.getTodosUsuarios()
      .subscribe((usuarios: Usuario[]) => {
        this.usuarios = usuarios;
      });
  }

  visualizar(trilha) {
    const dialogRef = this.dialog.open(VisualizarAuditoriaComponent, {
      data: trilha,
      width: '600px'
    });
  }

  aoSelecionarUsuario(arg) {
    this.usuarioSelecionado = arg;
    this.filtroStatus();
  }

  filtroStatus() {
    const busca = this.busca.nativeElement.value.toLowerCase();
    this.trilhas.filterPredicate = (trilha: TrilhaAuditoria) => {
      return ((
        moment(trilha.data)
      )
        .isAfter(moment(this.dataInicial.value)) || this.dataInicial.value === null) &&
        ((
          moment(trilha.data)
        )
          .isBefore(moment(this.dataFinal.value).add(1, 'day')) || this.dataFinal.value === null) &&
        ((
          trilha.categoria
        )
          .includes(this.selectedCategoria) || this.selectedCategoria === 'all') &&
        ((
          trilha.tipo
        )
          .includes(this.selectedModulo) || this.selectedModulo === 'all') &&
          ((
            trilha.usuario.login
          )
            .toLowerCase()
            .includes(busca));
    };
    this.trilhas.filter = this.selectedCategoria + this.selectedModulo;
    if (this.trilhas.paginator) {
      this.trilhas.paginator.firstPage();
    }
  }

  alterarFiltro(arg) {
    console.log(this.dataInicial.value);
  }
}
