import { AuditoriaComponent } from './auditoria/auditoria.component';
import { RecuperacaoSenhaComponent } from './recuperacao-senha/recuperacao-senha.component';
import { NaoAutorizadoComponent } from './core/nao-autorizado.components';
import { AuthGuard } from './seguranca/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { PaginaNaoEncontradaComponent } from './core/pagina-nao-encontrada.component';
import { PerfilComponent } from './perfil/perfil.component';

const routes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/reservas-dependencias',
        pathMatch: 'full'
      },
      {
        path: 'dependencias',
        loadChildren: './dependencias/dependencias.module#DependenciasModule',
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_MASTER'] }
      },
      {
        path: 'blocos',
        loadChildren: './blocos/blocos.module#BlocosModule',
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_MASTER'] }
      },
      {
        path: 'motoristas',
        loadChildren: './motoristas/motoristas.module#MotoristasModule',
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_MASTER', 'ROLE_APPROVER'] }
      },
      {
        path: 'turmas',
        loadChildren: './turmas/turmas.module#TurmasModule',
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_MASTER'] }
      },
      {
        path: 'usuarios',
        loadChildren: './usuarios/usuarios.module#UsuariosModule',
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_MASTER'] }
      },
      {
        path: 'veiculos',
        loadChildren: './veiculos/veiculos.module#VeiculosModule',
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_MASTER', 'ROLE_APPROVER'] }
      },
      {
        path: 'reservas-veiculos',
        loadChildren: './reservas-veiculo/reservas-veiculo.module#ReservasVeiculoModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'reservas-dependencias',
        loadChildren: './reservas-dependencia/reservas-dependencia.module#ReservasDependenciaModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'aprovacao',
        loadChildren: './aprovacao/aprovacao.module#AprovacaoModule',
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_APPROVER'] }
      },
      {
        path: 'relatorios',
        loadChildren: './relatorios/relatorios.module#RelatoriosModule',
        canActivate: [AuthGuard],
        // data: { roles: ['ROLE_APPROVER'] }
      },
      {
        path: 'perfil',
        component: PerfilComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'auditoria',
        component: AuditoriaComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_MASTER'] }
      },
      {
        path: 'login',
        loadChildren: './seguranca/seguranca.module#SegurancaModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'recuperar-senha',
        component: RecuperacaoSenhaComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'recuperar-senha/:token',
        component: RecuperacaoSenhaComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'pagina-nao-encontrada',
        component: PaginaNaoEncontradaComponent,
        canActivate: [AuthGuard]
      },
      // {
      //   path: 'nao-autorizado',
      //   component: NaoAutorizadoComponent
      // },
      {
        path: '**',
        redirectTo: 'pagina-nao-encontrada'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
