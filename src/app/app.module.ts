import { VisualizarAuditoriaComponent } from './auditoria/visualizar-auditoria/visualizar-auditoria.component';
import { AuditoriaComponent } from './auditoria/auditoria.component';
import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LocationStrategy, PathLocationStrategy, registerLocaleData, DatePipe } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { ToastrModule } from 'ngx-toastr';

import { NgxMaskModule } from 'ngx-mask';
import { FullCalendarModule } from '@fullcalendar/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullComponent } from './layouts/full/full.component';
import { AppHeaderComponent } from './layouts/full/header/header.component';
import { AppSidebarComponent } from './layouts/full/sidebar/sidebar.component';
import { DemoMaterialModule } from './demo-material-module';
import { SharedModule } from './shared/shared.module';
import { PerfilComponent } from './perfil/perfil.component';
import { SegurancaModule } from './seguranca/seguranca.module';

import localePt from '@angular/common/locales/pt';
import localePtExtra from '@angular/common/locales/extra/pt';
import { CoreModule } from './core/core.module';
import { JwtModule } from '@auth0/angular-jwt';
import { MatPaginatorIntl } from '@angular/material';
import { getPortuguesePaginatorIntl } from './core/services/mat-paginator-translate';

registerLocaleData(localePt, localePtExtra);

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    FullComponent,
    AppHeaderComponent,
    AppSidebarComponent,
    // PaginaNaoEncontradaComponent,
    PerfilComponent,
    AuditoriaComponent,
    VisualizarAuditoriaComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      closeButton: true,
      // timeOut: 10000,
      positionClass: 'toast-bottom-right',
      // preventDuplicates: true,
    }),
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    AppRoutingModule,
    FullCalendarModule,
    LoadingBarHttpClientModule,
    // SegurancaModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: environment.tokenWhitelistedDomains,
        blacklistedRoutes: environment.tokenBlacklistedRoutes
      }
    }),
  ],
  entryComponents: [
    VisualizarAuditoriaComponent
  ],
  providers: [
    DatePipe,
    { provide: MatPaginatorIntl, useValue: getPortuguesePaginatorIntl() }
    // {
    //   provide: LocationStrategy,
    //   useClass: PathLocationStrategy
    // }
    // { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    // { provide: LOCALE_ID, useValue: 'pt-BR' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
