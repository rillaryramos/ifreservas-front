import { LogoutService } from './../seguranca/logout.service';
import { Title } from '@angular/platform-browser';
import { RelatoriosService } from './services/relatorios.service';
import { RecuperacaoSenhaModule } from './../recuperacao-senha/recuperacao-senha.module';
import { MoneyHttp } from './../seguranca/money-http';
import { MAT_DATE_LOCALE } from '@angular/material';
import { RouterModule } from '@angular/router';
import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClientModule } from '@angular/common/http';

import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada.component';
import { ErrorHandlerService } from './services/error-handler.service';
import { MotoristaService } from './services/motorista.service';
import { ReservaVeiculoService } from './services/reserva-veiculo.service';
import { AuthService } from '../seguranca/auth.service';
import { NaoAutorizadoComponent } from './nao-autorizado.components';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    RecuperacaoSenhaModule
  ],
  declarations: [
    PaginaNaoEncontradaComponent,
    NaoAutorizadoComponent
  ],
  exports: [
  ],
  providers: [
    MotoristaService,
    ErrorHandlerService,
    ReservaVeiculoService,
    AuthService,
    MoneyHttp,
    RelatoriosService,
    Title,
    LogoutService,
    // JwtHelperService,
    // { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ]
})
export class CoreModule { }
