import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pagina-nao-encontrada',
  // template: `
  //   <p>
  //     Página não encontrada!
  //   </p>
  // `,
  template: `
    <div style="position: absolute; top: 50%; left: 50%; margin-right: -50%;transform: translate(-50%, -50%)">
      <img class="img-fluid" src="assets/images/404.png">
    </div>
  `,
  styles: []
})
export class PaginaNaoEncontradaComponent implements OnInit {

  constructor(private title: Title) { }

  ngOnInit() {
    this.title.setTitle('Página não encontrada');
  }

}
