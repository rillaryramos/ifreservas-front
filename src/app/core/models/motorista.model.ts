export class Motorista {
  constructor(
    public id: number,
    public nome: string,
    public sobrenome: string,
    public cpf: string,
    public ativo: boolean
  ) { }
}
