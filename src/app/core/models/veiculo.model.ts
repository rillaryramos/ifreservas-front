export class Veiculo {
    public id: number;
    public modelo: string;
    public marca: string;
    public placa: string;
    public observacao: string;
    public ativo: boolean;
}
