import { Usuario } from './usuario.model';
export class TrilhaAuditoria {
  public id: number;
  public descricao: string;
  public categoria: string;
  public tipo: string;
  public data: Date;
  public idEntidade: number;
  public usuario = new Usuario();
}
