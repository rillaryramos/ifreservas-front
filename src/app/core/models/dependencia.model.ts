import { Bloco } from './bloco.model';
export class Dependencia {
  public id: number;
  public nome: string;
  public ativo: boolean;
  public bloco = new Bloco();
}
