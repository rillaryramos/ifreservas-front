
export class Veiculo {
  public id: number;
  public modelo: string;
  public marca: string;
  public placa: string;
  public ativo: boolean;
}

export class Usuario {
  public id: number;
  public nome: string;
  public sobrenome: string;
  public email: string;
  public role: string;
  public ativo: boolean;
}

export class Status {
  public id: number;
  public nome: string;
}

export class Motorista {
  public id: number;
  public nome: string;
  public sobrenome: string;
  public cpf: string;
  public ativo: boolean;
}


export class ReservaVeiculo {
  // constructor(
  public id: number;
  public title: string;
  public start: Date;
  public end: Date;
  public horaInicial: string;
  public horaFinal: string;
  public saida: string;
  public destino: string;
  public motivo: string;
  public nivelAprovacao: number;
  public veiculo = new Veiculo();
  public usuario = new Usuario();
  public status = new Status();
  public motorista = new Motorista();
  public backgroundColor: string;
  public borderColor: string;
  public description: string;
  public dataEnvioAprovacao: Date;
  // ) {}
}

export class ReservasVeiculoParaReprovar {
  public reservaParaAprovar: ReservaVeiculo;
  public reservasParaReprovar: ReservaVeiculo[];
}
