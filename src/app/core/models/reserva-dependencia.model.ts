import { Status } from './reserva-veiculo.model';
import { Usuario } from './usuario.model';
import { Dependencia } from './dependencia.model';
import { Turma } from './turma.model';
export class HorarioReserva {
  public id: number;
  public diaSemana: string;
  public horarioInicio: Date;
  public horarioTermino: Date;
}

export class ReservaDependencia {
  public id: number;
  public title: string;
  public start: Date;
  public end: Date;
  public horarioInicio: string;
  public horarioTermino: string;
  public observacao: string;
  public usoContinuo: boolean;
  public aula: boolean;
  public dependencia = new Dependencia();
  public usuarioSolicitante = new Usuario();
  public usuarioUtilizador = new Usuario();
  public turma = new Turma();
  public status = new Status();
  // public horarioReserva = new HorarioReserva();
  public horarioReserva: HorarioReserva[];
  public backgroundColor: string;
  public borderColor: string;
}

export class ListaReservasDependencia {
  public reservas: ReservaDependencia[];
}
