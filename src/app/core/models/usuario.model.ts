export class Usuario {
  public id: number;
  public nome: string;
  public sobrenome: string;
  public email: string;
  public login: string;
  public role: string;
  public ativo: boolean;
}
