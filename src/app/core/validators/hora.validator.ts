import { ValidatorFn, FormGroup, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class HoraValidator {
  static horaFinalMaior: ValidatorFn = (formGroup: FormGroup) => {
    if (formGroup.get('start').value.toLocaleDateString() === formGroup.get('end').value.toLocaleDateString()) {
      const controlNames = Object.keys(formGroup.controls || {});
      const isValid = controlNames.every(controlName => formGroup.get('horaFinal').value.id > formGroup.get('horaInicial').value.id);
      return isValid ? null : { horaFinalNaoEMaior: true };
    }
    return null;
  }
}

export class HorarioValidator {
  static horaFinalMaior: ValidatorFn = (formGroup: FormGroup) => {
    const controlNames = Object.keys(formGroup.controls || {});
    const isValid = controlNames.every(controlName => formGroup.get('horarioTermino').value > formGroup.get('horarioInicio').value);
    return isValid ? null : { horaFinalNaoEMaior: true };
  }
}

/**
 * Custom ErrorStateMatcher which returns true (error exists) when the parent form group is invalid and the control has been touched
 */
export class ConfirmValidParentMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return control.parent.invalid && !form.control.controls.dataFormGroup.get('horaInicial').invalid && (control.touched || form.submitted);
  }
}

export class ConfirmValidMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return control.parent.invalid && !form.control.controls.horaInicial.invalid && !form.control.controls.end.invalid && (control.touched || form.submitted);
  }
}

export class ConfirmValidHorarioMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return control.parent.invalid && (control.touched || form.submitted);
  }
}

