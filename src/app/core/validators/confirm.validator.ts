import { FormControl, FormGroup, FormGroupDirective, NgForm, ValidatorFn } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class ConfirmValidator {
	/**
	 * Validates that child controls in the form group are equal
	 */
  static childrenEqual: ValidatorFn = (formGroup: FormGroup) => {
    const controlNames = Object.keys(formGroup.controls || {});
    const isValid = controlNames.every(controlName => formGroup.get('novaSenha').value === formGroup.get('confirmacaoSenha').value);
    return isValid ? null : { childrenNotEqual: true };
  }
}

/**
 * Custom ErrorStateMatcher which returns true (error exists) when the parent form group is invalid and the control has been touched
 */
export class ConfirmValidParentMatcherSenha implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return control.parent.invalid && (control.touched || form.submitted);
  }
}


