import { MoneyHttp } from './../../seguranca/money-http';
import { Turma } from './../models/turma.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class TurmaService {

  turmasUrl = 'http://localhost:8080/api/turma/';

  constructor(
    private http: MoneyHttp,
    private errorHandler: ErrorHandlerService
  ) { }

  getTurmasAtivas(): Observable<Turma[]> {
    return this.http.get<Turma[]>(this.turmasUrl + 'ativa')
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
      );
  }

  getTurmas(): Observable<Turma[]> {
    return this.http.get<Turma[]>(this.turmasUrl)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
      );
  }

  cadastrarTurma(turma: Turma): Observable<Turma> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<Turma>(this.turmasUrl, JSON.stringify(turma), { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  atualizarTurma(turma: Turma) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<Turma>(this.turmasUrl + turma.id, turma, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  alterarStatus(id: number, ativo: boolean) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put(this.turmasUrl + id + '/ativa', ativo, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  getTurmaById(id: number): Observable<Turma> {
    return this.http.get<Turma>(this.turmasUrl + id)
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  excluir(id: number): Observable<any> {
    return this.http.delete(this.turmasUrl + id)
      .pipe(
        catchError((err) => {
          throw this.errorHandler.handle(err);
        })
      );
  }
}
