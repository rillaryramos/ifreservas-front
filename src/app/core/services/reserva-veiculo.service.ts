import { MoneyHttp } from './../../seguranca/money-http';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import { ReservaVeiculo, ReservasVeiculoParaReprovar } from '../models/reserva-veiculo.model';

@Injectable({
  providedIn: 'root'
})
export class ReservaVeiculoService {

  reservasUrl = 'http://localhost:8080/api/reserva_veiculo/';

  constructor(private http: MoneyHttp, private errorHandler: ErrorHandlerService) { }

  getReservasVeiculo(): Observable<ReservaVeiculo[]> {
    return this.http.get<ReservaVeiculo[]>(this.reservasUrl)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getReservasEmAprovacao(idStatus: number, idVeiculo: number, page: number): Observable<any> {
    return this.http.get<any>(this.reservasUrl + 'em_aprovacao?idStatus=' + idStatus + '&idVeiculo=' + idVeiculo + '&size=5&page=' + page)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getReservasAprovadas(): Observable<ReservaVeiculo[]> {
    return this.http.get<ReservaVeiculo[]>(this.reservasUrl + 'aprovadas')
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getReservasByVeiculo(idVeiculo: number, tipo: string): Observable<ReservaVeiculo[]> {
    return this.http.get<ReservaVeiculo[]>(this.reservasUrl + 'veiculo/' + idVeiculo + '?tipo=' + tipo)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getReservaById(id: number): Observable<ReservaVeiculo> {
    return this.http.get<ReservaVeiculo>(this.reservasUrl + id)
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  createReserva(reserva: any): Observable<ReservaVeiculo> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<ReservaVeiculo>(this.reservasUrl, JSON.stringify(reserva), { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  atualizarReserva(reserva: any) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<ReservaVeiculo>(this.reservasUrl + reserva.id, reserva, { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  cancelarReserva(id: number) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<ReservaVeiculo>(this.reservasUrl + 'cancelar/' + id, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  reprovarReserva(id: number, motivo: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<ReservaVeiculo>(this.reservasUrl + 'reprovar/' + id, motivo, { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  cancelarReservaAdministrador(id: number, motivo: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<ReservaVeiculo>(this.reservasUrl + 'cancelar/' + id + '/administrador', motivo, { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  aprovarReserva(id: number, reservasVeiculoParaReprovar: ReservasVeiculoParaReprovar) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<ReservaVeiculo>(this.reservasUrl + 'aprovar/' + id, reservasVeiculoParaReprovar, { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  alterarStatus(idReserva: number, idStatus: number) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<ReservaVeiculo>(this.reservasUrl + idReserva + '/status/' + idStatus, { headers });
  }

  verificarCompatibilidadeEmAprovacao(idReserva: number, start: any, end: any): Observable<ReservaVeiculo[]> {
    return this.http.get<ReservaVeiculo[]>(this.reservasUrl + 'em_aprovacao/verificar_compatibilidade/' + idReserva + '?start=' + start + '&end=' + end);
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  alterarMotorista(idReserva: number, idMotorista: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<ReservaVeiculo>(this.reservasUrl + idReserva + '/motorista', idMotorista , { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }
}
