import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {
  constructor(private snackbar: MatSnackBar) {}

  sendMessage(message: string) {
    this.snackbar.open(message, 'OK', {
      duration: 5000
    });
  }

  destroyMessage() {
    this.snackbar.ngOnDestroy();
  }
}