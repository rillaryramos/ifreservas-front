import { MoneyHttp } from './../../seguranca/money-http';
import { Usuario } from './../models/usuario.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuariosUrl = 'http://localhost:8080/api/usuario/';

  constructor(
    private http: MoneyHttp,
    private httpClient: HttpClient,
    private errorHandler: ErrorHandlerService
  ) { }

  getUsuarios(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.usuariosUrl)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getTodosUsuarios(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.usuariosUrl + 'todos')
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getUsuariosAtivos(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.usuariosUrl + 'ativo')
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getUsuarioByEmail(email: string): Observable<Usuario> {
    return this.http.get<Usuario>(this.usuariosUrl + 'email?email=' + email)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getUsuarioByLogin(login: string): Observable<Usuario> {
    return this.http.get<Usuario>(this.usuariosUrl + 'login?login=' + login)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getUsuarioById(id: number): Observable<Usuario> {
    return this.http.get<Usuario>(this.usuariosUrl + id)
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  cadastrarUsuario(usuario: Usuario): Observable<Usuario> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<Usuario>(this.usuariosUrl, JSON.stringify(usuario), { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  updateUsuario(usuario: any) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<Usuario>(this.usuariosUrl + usuario.id, usuario, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  alterarSenha(idUsuario: number, senhaAtual: string, novaSenha: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<Usuario>(this.usuariosUrl + idUsuario + '/alterarSenha?senhaAtual=' + senhaAtual + '&novaSenha=' + novaSenha, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  alterarStatus(id: number, ativo: boolean) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put(this.usuariosUrl + id + '/ativo', ativo, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  alterarRoleAtivo(id: number, role: string, ativo: boolean) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put(this.usuariosUrl + id + '/roleAtivo?role=' + role + '&ativo=' + ativo, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  excluir(id: number): Observable<any> {
    return this.http.delete(this.usuariosUrl + id)
      .pipe(
        catchError((err) => {
          throw this.errorHandler.handle(err);
        })
      );
  }

  enviarLinkParaAlteracaoDeSenha(email: string) {
    return this.httpClient.get(this.usuariosUrl + 'enviarLinkParaAlteracaoDeSenha?email=' + email);
  }

  validarToken(token: string): Observable<any> {
    return this.httpClient.get<any>(this.usuariosUrl + 'validarToken?token=' + token);
  }

  redefinirSenha(token: string, email: string, novaSenha: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.httpClient.put(this.usuariosUrl + '/recuperarSenha/alterar?token=' + token + '&email=' + email + '&novaSenha=' + novaSenha, { headers });
  }
}
