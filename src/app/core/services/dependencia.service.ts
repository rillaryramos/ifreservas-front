import { MoneyHttp } from './../../seguranca/money-http';
import { Dependencia } from './../models/dependencia.model';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class DependenciaService {

  dependenciasUrl = 'http://localhost:8080/api/dependencia/';

  constructor(
    private http: MoneyHttp,
    private errorHandler: ErrorHandlerService
  ) { }

  getDependencias(): Observable<Dependencia[]> {
    return this.http.get<Dependencia[]>(this.dependenciasUrl)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
      );
  }

  cadastrarDependencia(dependencia: Dependencia): Observable<Dependencia> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<Dependencia>(this.dependenciasUrl, JSON.stringify(dependencia), { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  atualizarDependencia(dependencia: Dependencia) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<Dependencia>(this.dependenciasUrl + dependencia.id, dependencia, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  alterarStatus(id: number, ativo: boolean) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put(this.dependenciasUrl + id + '/ativa', ativo, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  getDependenciaById(id: number): Observable<Dependencia> {
    return this.http.get<Dependencia>(this.dependenciasUrl + id)
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  excluir(id: number): Observable<any> {
    return this.http.delete(this.dependenciasUrl + id)
      .pipe(
        catchError((err) => {
          throw this.errorHandler.handle(err);
        })
      );
  }
}
