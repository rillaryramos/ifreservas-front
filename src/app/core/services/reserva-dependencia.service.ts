import { ListaReservasDependencia } from './../models/reserva-dependencia.model';
import { MoneyHttp } from './../../seguranca/money-http';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import { ReservaDependencia } from '../models/reserva-dependencia.model';

@Injectable({
  providedIn: 'root'
})
export class ReservaDependenciaService {

  reservasUrl: string = 'http://localhost:8080/api/reserva_dependencia/';

  constructor(private http: MoneyHttp, private errorHandler: ErrorHandlerService) { }

  getReservasDependencia(): Observable<ReservaDependencia[]> {
    return this.http.get<ReservaDependencia[]>(this.reservasUrl)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getReservasByDependencia(idDependencia: number, tipo: string): Observable<ReservaDependencia[]> {
    return this.http.get<ReservaDependencia[]>(this.reservasUrl + 'dependencia/' + idDependencia + '?tipo=' + tipo)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getReservaById(id: number): Observable<ReservaDependencia> {
    return this.http.get<ReservaDependencia>(this.reservasUrl + id)
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  createReservaUsoBreve(reserva: any): Observable<ReservaDependencia> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<ReservaDependencia>(this.reservasUrl + 'uso_breve', JSON.stringify(reserva), { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  createReservaUsoBreveLista(reservasUsoBreve: ListaReservasDependencia) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<ReservaDependencia>(this.reservasUrl + 'uso_breve/more_one', JSON.stringify(reservasUsoBreve), { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  atualizarReservaUsoBreve(reserva: any) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<ReservaDependencia>(this.reservasUrl + 'uso_breve/' + reserva.id, reserva, { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  createReservaUsoContinuo(reserva: any): Observable<ReservaDependencia> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<ReservaDependencia>(this.reservasUrl + 'uso_continuo', JSON.stringify(reserva), { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  atualizarReservaUsoContinuo(reserva: any) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<ReservaDependencia>(this.reservasUrl + 'uso_continuo/' + reserva.id, reserva, { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }

  cancelarReserva(id: number, inicio, fim) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<ReservaDependencia>(this.reservasUrl + 'cancelar/' + id + '?inicio=' + inicio + '&fim=' + fim, { headers });
      // .pipe(
      //   catchError(error => {
      //     throw this.errorHandler.handle(error);
      //   })
      // );
  }
}
