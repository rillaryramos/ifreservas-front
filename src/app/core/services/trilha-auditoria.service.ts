import { TrilhaAuditoria } from './../models/trilha-auditoria.model';
import { MoneyHttp } from './../../seguranca/money-http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class TrilhaAuditoriaService {

  auditoriaUrl: string;

  constructor(
    private http: MoneyHttp,
    private errorHandler: ErrorHandlerService
  ) { this.auditoriaUrl = `${environment.apiUrl}/trilha_auditoria`; }

  getTrilhas(): Observable<any> {
    return this.http.get<any>(this.auditoriaUrl)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

}
