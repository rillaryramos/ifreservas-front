import { MoneyHttp } from './../../seguranca/money-http';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';
import { Veiculo } from '../models/veiculo.model';

@Injectable({
  providedIn: 'root'
})
export class VeiculoService {

  veiculosUrl = 'http://localhost:8080/api/veiculo/';

  constructor(private http: MoneyHttp, private errorHandler: ErrorHandlerService) { }

  getVeiculos(): Observable<Veiculo[]> {
    return this.http.get<Veiculo[]>(this.veiculosUrl)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  cadastrarVeiculo(veiculo: Veiculo): Observable<Veiculo> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<Veiculo>(this.veiculosUrl, JSON.stringify(veiculo), { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  atualizarVeiculo(veiculo: Veiculo) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<Veiculo>(this.veiculosUrl + veiculo.id, veiculo, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  alterarStatus(id: number, ativo: boolean) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put(this.veiculosUrl + id + '/ativo', ativo, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  getVeiculoById(id: number): Observable<Veiculo> {
    return this.http.get<Veiculo>(this.veiculosUrl + id)
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  excluir(id: number): Observable<any> {
    return this.http.delete(this.veiculosUrl + id)
      .pipe(
        catchError((err) => {
          throw this.errorHandler.handle(err);
        })
      );
  }
}
