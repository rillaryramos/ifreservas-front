import { Bloco } from './../models/bloco.model';
import { MoneyHttp } from './../../seguranca/money-http';
import { Dependencia } from './../models/dependencia.model';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class BlocoService {

  blocosUrl = 'http://localhost:8080/api/bloco/';

  constructor(
    private http: MoneyHttp,
    private errorHandler: ErrorHandlerService
  ) { }

  getBlocos(): Observable<Bloco[]> {
    return this.http.get<Bloco[]>(this.blocosUrl)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
      );
  }

  cadastrarBloco(bloco: Bloco): Observable<Bloco> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<Bloco>(this.blocosUrl, JSON.stringify(bloco), { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  atualizarBloco(bloco: Bloco) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<Bloco>(this.blocosUrl + bloco.id, bloco, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  getBlocoById(id: number): Observable<Bloco> {
    return this.http.get<Bloco>(this.blocosUrl + id)
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  excluir(id: number): Observable<any> {
    return this.http.delete(this.blocosUrl + id)
      .pipe(
        catchError((err) => {
          throw this.errorHandler.handle(err);
        })
      );
  }
}
