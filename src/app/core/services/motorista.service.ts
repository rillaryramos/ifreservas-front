import { MoneyHttp } from './../../seguranca/money-http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Motorista } from '../models/motorista.model';
import { catchError } from 'rxjs/operators';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class MotoristaService {

  motoristasUrl: string;

  constructor(
    private http: MoneyHttp,
    private errorHandler: ErrorHandlerService
  ) { this.motoristasUrl = `${environment.apiUrl}/motorista/`; }

  getMotoristas(): Observable<Motorista[]> {
    return this.http.get<Motorista[]>(this.motoristasUrl)
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  getMotoristasAtivos(): Observable<Motorista[]> {
    return this.http.get<Motorista[]>(this.motoristasUrl + 'ativo')
      .pipe(catchError((err) => {
        throw this.errorHandler.handle(err);
      })
    );
  }

  createMotorista(motorista: Motorista): Observable<Motorista> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post<Motorista>(this.motoristasUrl, JSON.stringify(motorista), { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  updateMotorista(motorista: Motorista) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put<Motorista>(this.motoristasUrl + motorista.id, motorista, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  changeStatus(id: number, ativo: boolean) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.put(this.motoristasUrl + id + '/ativo', ativo, { headers })
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  getMotoristaById(id: number): Observable<Motorista> {
    return this.http.get<Motorista>(this.motoristasUrl + id)
      .pipe(
        catchError(error => {
          throw this.errorHandler.handle(error);
        })
      );
  }

  excluir(id: number): Observable<any> {
    return this.http.delete(this.motoristasUrl + id)
      .pipe(
        catchError((err) => {
          throw this.errorHandler.handle(err);
        })
      );
  }
}
