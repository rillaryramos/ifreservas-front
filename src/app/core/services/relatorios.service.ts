import { MoneyHttp } from './../../seguranca/money-http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RelatoriosService {

  relatoriosUrl = 'http://localhost:8080/api/';

  constructor(
    private http: MoneyHttp
  ) { }

  relatorioDependencia(inicio: string, fim: string, idDependencia: number, idServidor: number, idTurma: number, tipo: string) {
    return this.http.get(
      this.relatoriosUrl + 'reserva_dependencia/relatorio?inicio=' + inicio + '&fim=' + fim + '&idDependencia=' + idDependencia
      + '&idUsuario=' + idServidor + '&idTurma=' + idTurma + '&tipo=' + tipo,
      { responseType: 'blob' })
      .toPromise()
      .catch(response => {
        if (response.status === 404) {
          return Promise.reject('Nenhum registro de reserva encontrado para os dados informados!');
        }
        return Promise.reject('Ocorreu um erro ao processar a sua solicitação');
      });
  }

  minhasReservasVeiculo(inicio: string, fim: string, idVeiculo: number, idStatus: number) {
    return this.http.get(
      this.relatoriosUrl + 'reserva_veiculo/relatorio/minhas_reservas?inicio=' + inicio + '&fim=' + fim + '&idVeiculo=' + idVeiculo
      + '&idStatus=' + idStatus,
      { responseType: 'blob' })
      .toPromise()
      .catch(response => {
        if (response.status === 404) {
          return Promise.reject('Nenhum registro de reserva encontrado para os dados informados!');
        }
        return Promise.reject('Ocorreu um erro ao processar a sua solicitação');
      });
  }

  relatorioReservasVeiculoAprovadas(inicio: string, fim: string, idVeiculo: number, idUsuario: number, idMotorista: number) {
    return this.http.get(
      this.relatoriosUrl + 'reserva_veiculo/relatorio/aprovadas?inicio=' + inicio + '&fim=' + fim + '&idVeiculo=' + idVeiculo
      + '&idUsuario=' + idUsuario + '&idMotorista=' + idMotorista,
      { responseType: 'blob' })
      .toPromise()
      .catch(response => {
        if (response.status === 404) {
          return Promise.reject('Nenhum registro de reserva encontrado para os dados informados!');
        }
        return Promise.reject('Ocorreu um erro ao processar a sua solicitação');
      });
  }

}
