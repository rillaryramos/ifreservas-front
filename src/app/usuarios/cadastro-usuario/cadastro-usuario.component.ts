import { CarregamentoComponent } from './../../shared/carregamento/carregamento.component';
import { LogoutService } from './../../seguranca/logout.service';
import { ErrorHandlerService } from './../../core/services/error-handler.service';
import { AuthService } from './../../seguranca/auth.service';
import { ConfirmacaoAlteracaoComponent } from './../confirmacao-alteracao/confirmacao-alteracao.component';
import { MatDialog } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.scss']
})
export class CadastroUsuarioComponent implements OnInit {
  formGroupUsuario: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private usuarioService: UsuarioService,
    private title: Title,
    public dialog: MatDialog,
    private auth: AuthService,
    private logoutService: LogoutService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.title.setTitle('Cadastro de usuário');
    this.configurarForm();
    const idUsuario = this.activatedRoute.snapshot.params['id'];
    if (idUsuario) {
      this.formGroupUsuario.setControl('nome', new FormControl({ value: null, disabled: true }));
      this.formGroupUsuario.setControl('sobrenome', new FormControl({ value: null, disabled: true }));
      this.formGroupUsuario.setControl('email', new FormControl({ value: null, disabled: true }));
      this.buscarUsuario(idUsuario);
    }
  }

  configurarForm() {
    this.formGroupUsuario = new FormGroup({
      id: new FormControl(null),
      nome: new FormControl(null, Validators.required),
      sobrenome: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      role: new FormControl(null, Validators.required),
      login: new FormControl({ value: '', disabled: true }),
      ativo: new FormControl(null),
      senha: new FormControl(null),
      dataCadastro: new FormControl(null)
    });
  }

  get id(): AbstractControl {
    return this.formGroupUsuario.controls.id;
  }

  get nome(): AbstractControl {
    return this.formGroupUsuario.controls.nome;
  }

  get sobrenome(): AbstractControl {
    return this.formGroupUsuario.controls.sobrenome;
  }

  get role(): AbstractControl {
    return this.formGroupUsuario.controls.role;
  }

  get login(): AbstractControl {
    return this.formGroupUsuario.controls.login;
  }

  get email(): AbstractControl {
    return this.formGroupUsuario.controls.email;
  }

  get ativo(): AbstractControl {
    return this.formGroupUsuario.controls.ativo;
  }

  get editando() {
    return Boolean(this.id.value);
  }

  buscarUsuario(id: number) {
    this.usuarioService.getUsuarioById(id)
      .subscribe(usuario => {
        this.formGroupUsuario.setValue(usuario);
        console.log(usuario);
        this.atualizarTituloEdicao();
      });
  }

  alteracaoStatus() {
    if (this.editando) {
      if (!this.ativo.value && this.role.value === 'ROLE_MASTER') {
        this.role.setValue(null);
      }
    }
  }

  exibirOpcaoMaster() {
    return Boolean(this.editando && this.ativo.value);
  }

  atualizarTituloEdicao() {
    this.title.setTitle(`Edição de usuário: ${this.formGroupUsuario.get('nome').value}`);
  }

  salvar() {
    if (this.editando) {
      this.atualizarUsuario();
    } else {
      this.adicionarUsuario();
    }
  }

  adicionarUsuario() {
    if (this.formGroupUsuario.valid) {
      const dialogRef = this.dialog.open(CarregamentoComponent, {
        disableClose: true,
        data: 'Enviando a senha gerada para o e-mail informado...',
        width: '350px'
      });
      return this.usuarioService.cadastrarUsuario({
        id: this.id.value,
        nome: this.nome.value,
        sobrenome: this.sobrenome.value,
        email: this.email.value + '@ifnmg.edu.br',
        // email: this.email.value + '@gmail.com',
        role: this.role.value,
        login: this.login.value,
        ativo: this.ativo.value
      })
        .pipe(
          catchError(error => {
            dialogRef.close();
            throw this.errorHandler.handle(error);
          })
        )
        .subscribe(usuario => {
          dialogRef.close();
          this.router.navigate(['/usuarios']);
          this.toastr.success('O usuário foi cadastrado');
        });
    }
  }

  atualizarUsuario() {
    if (this.role.valid) {
      if (this.role.value === 'ROLE_MASTER') {
        const dialogRef = this.dialog.open(ConfirmacaoAlteracaoComponent, {
          data: this.nome.value,
          width: '350px'
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.usuarioService.alterarRoleAtivo(this.id.value, this.role.value, this.ativo.value)
              .subscribe(usuario => {
                this.logoutService.logout()
                  .then(() => {
                    this.router.navigate(['/login']);
                  })
                  .catch(erro => this.errorHandler.handle(erro));
              });
          }
        });
      } else {
        this.usuarioService.alterarRoleAtivo(this.id.value, this.role.value, this.ativo.value)
          .subscribe(usuario => {
            this.router.navigate(['/usuarios']);
            this.toastr.success('O usuário foi atualizado');
          });
      }
    }
  }

  inserirLogin() {
    this.login.setValue(this.email.value);
  }
}
