import { CarregamentoComponent } from './../shared/carregamento/carregamento.component';
import { SharedModule } from './../shared/shared.module';
import { ConfirmacaoAlteracaoComponent } from './confirmacao-alteracao/confirmacao-alteracao.component';
import { RemoverUsuarioComponent } from './remover-usuario/remover-usuario.component';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { DemoMaterialModule } from '../demo-material-module';
import { UsuariosRoutingModule } from './usuarios-routing.module';

import { UsuariosComponent } from './usuarios.component';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';
import { StatusComponent } from './status-usuario/status-usuario.component';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    UsuariosRoutingModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    NgxMaskModule,
    SharedModule
  ],
  providers: [],
  entryComponents: [
    RemoverUsuarioComponent,
    StatusComponent,
    ConfirmacaoAlteracaoComponent,
    CarregamentoComponent
  ],
  declarations: [
    UsuariosComponent,
    CadastroUsuarioComponent,
    RemoverUsuarioComponent,
    StatusComponent,
    ConfirmacaoAlteracaoComponent
  ]
})
export class UsuariosModule {}
