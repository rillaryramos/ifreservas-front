import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-confirmacao-alteracao',
  templateUrl: 'confirmacao-alteracao.component.html',
})
export class ConfirmacaoAlteracaoComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmacaoAlteracaoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
