import { Usuario } from './../../core/models/usuario.model';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-remover-usuario',
  templateUrl: 'remover-usuario.component.html',
})
export class RemoverUsuarioComponent {

  constructor(
    public dialogRef: MatDialogRef<RemoverUsuarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Usuario) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
