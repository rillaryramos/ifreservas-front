import { AuthGuard } from './../seguranca/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsuariosComponent } from './usuarios.component';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';

const routes: Routes = [
  {
    path: '',
    component: UsuariosComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  },
  {
    path: 'novo',
    component: CadastroUsuarioComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  },
  {
    path: ':id',
    component: CadastroUsuarioComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
