import { Title } from '@angular/platform-browser';
import { Usuario } from './../core/models/usuario.model';
import { FiltroComponent } from './../shared/filtro/filtro.component';
import { Filtro } from './../core/models/filtro.model';
import { RemoverUsuarioComponent } from './remover-usuario/remover-usuario.component';
import { UsuarioService } from './../core/services/usuario.service';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { StatusComponent } from './status-usuario/status-usuario.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  selected = 'all';
  selectedTipo = 'all';
  filtro: Filtro;
  displayedColumns: string[] = ['id', 'nome', 'sobrenome', 'login', 'tipo', 'ativo', 'actions'];
  usuarios = new MatTableDataSource<Usuario>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('busca') busca: ElementRef;

  constructor(
    private usuarioService: UsuarioService,
    public dialog: MatDialog,
    private title: Title,
    private toastr: ToastrService
  ) {
    this.filtro = { filtros: ['Nome', 'Sobrenome', 'Login'], filtroSelecionado: 'Nome' };
  }

  ngOnInit() {
    this.title.setTitle('Usuários');
    this.listarUsuarios();
  }

  ngAfterViewInit() {
    this.usuarios.paginator = this.paginator;
  }

  listarUsuarios() {
    this.usuarioService.getUsuarios()
      .subscribe((usuarios: Usuario[]) => {
        this.usuarios.data = usuarios;
      });
  }

  changeStatus(usuario: Usuario) {
    const novoStatus = !usuario.ativo;
    this.usuarioService.alterarStatus(usuario.id, novoStatus).subscribe(() => {
      const acao = novoStatus ? 'ativado' : 'inativado';
      usuario.ativo = novoStatus;
      this.toastr.success(`Usuário ${acao} com sucesso`);
    });

  }

  openFiltroDialog() {
    const dialogRef = this.dialog.open(FiltroComponent, {
      data: this.filtro,
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.filtro.filtroSelecionado = result ? result : this.filtro.filtroSelecionado;
      this.filtroStatus();
    });
  }

  filtroStatus() {
    const busca = this.busca.nativeElement.value.toLowerCase();
    this.usuarios.filterPredicate = (usuario: Usuario, filter: string) => {
      return ((
        usuario.ativo ? 'true' : 'false'
      )
        .includes(this.selected) || this.selected === 'all') &&
        ((
          usuario.role
        )
          .includes(this.selectedTipo) || this.selectedTipo === 'all') && ((
            this.filtro.filtroSelecionado === 'Nome' ? usuario.nome :
              this.filtro.filtroSelecionado === 'Sobrenome' ? usuario.sobrenome :
                usuario.login
          )
            .toLowerCase()
            .includes(busca));
    };
    this.usuarios.filter = this.selected + busca;
    if (this.usuarios.paginator) {
      this.usuarios.paginator.firstPage();
    }
  }

  openAtivacaoDialog(usuario: Usuario): void {
    const dialogRef = this.dialog.open(StatusComponent, {
      data: usuario,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
        this.changeStatus(result);
      }
    });
  }

  openExclusaoDialog(usuario: Usuario): void {
    const dialogRef = this.dialog.open(RemoverUsuarioComponent, {
      data: usuario,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(result);
      }
    });
  }

  excluir(usuario: Usuario) {
    this.usuarioService.excluir(usuario.id).subscribe(() => {
      this.listarUsuarios();
      this.toastr.success('Usuário excluído com sucesso');
    });
  }
}
