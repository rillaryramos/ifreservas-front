import { Title } from '@angular/platform-browser';
import { ConfirmValidator, ConfirmValidParentMatcherSenha } from './../core/validators/confirm.validator';
import { Usuario } from './../core/models/usuario.model';
import { UsuarioService } from './../core/services/usuario.service';
import { AuthService } from './../seguranca/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  usuarioLogado: Usuario;
  formGroupPerfil: FormGroup;
  hideSenhaAtual = true;
  hideNovaSenha = true;
  hideConfirmacaoSenha = true;
  formGroupSenha: FormGroup;
  confirmValidParentMatcher: ConfirmValidParentMatcherSenha;

  constructor(
    private auth: AuthService,
    private usuarioService: UsuarioService,
    private toastr: ToastrService,
    private title: Title
  ) {
    this.confirmValidParentMatcher = new ConfirmValidParentMatcherSenha();
  }

  ngOnInit(): void {
    this.title.setTitle('Meu perfil');
    this.configurarFormGroupPerfil();
    this.configurarFormGroupSenha();
    if (this.auth.jwtPayLoad) {
      this.buscarUsuario(this.auth.jwtPayLoad.user_name);
    }
  }

  configurarFormGroupPerfil() {
    this.formGroupPerfil = new FormGroup({
      id: new FormControl(null),
      nome: new FormControl(null, Validators.required),
      sobrenome: new FormControl(null, Validators.required),
      email: new FormControl({ value: null, disabled: true }),
      login: new FormControl({ value: null, disabled: true }),
      role: new FormControl({ value: null, disabled: true }),
      ativo: new FormControl({ value: null, disabled: true }),
      senha: new FormControl({ value: null, disabled: true }),
      dataCadastro: new FormControl({ value: null, disabled: true })
    });
  }

  configurarFormGroupSenha() {
    this.formGroupSenha = new FormGroup({
      senhaAtual: new FormControl('', Validators.required),
      formGroupConfirmacao: new FormGroup({
        novaSenha: new FormControl('', [Validators.required, Validators.minLength(6)]),
        confirmacaoSenha: new FormControl('', Validators.required)
      },
        { validators: ConfirmValidator.childrenEqual })
    });
  }

  get id(): AbstractControl {
    return this.formGroupPerfil.controls.id;
  }

  get nome(): AbstractControl {
    return this.formGroupPerfil.controls.nome;
  }

  get sobrenome(): AbstractControl {
    return this.formGroupPerfil.controls.sobrenome;
  }

  get email(): AbstractControl {
    return this.formGroupPerfil.controls.email;
  }

  get login(): AbstractControl {
    return this.formGroupPerfil.controls.login;
  }

  get senhaAtual(): AbstractControl {
    return this.formGroupSenha.controls.senhaAtual;
  }

  get role(): AbstractControl {
    return this.formGroupPerfil.controls.role;
  }

  get novaSenha(): AbstractControl {
    return (<FormGroup>this.formGroupSenha.controls.formGroupConfirmacao)
      .controls.novaSenha;
  }

  get confirmacaoSenha(): AbstractControl {
    return (<FormGroup>this.formGroupSenha.controls.formGroupConfirmacao)
      .controls.confirmacaoSenha;
  }

  buscarUsuario(login: string) {
    this.usuarioService.getUsuarioByLogin(login)
      .subscribe((usuario: Usuario) => {
        this.usuarioLogado = usuario;
        this.formGroupPerfil.setValue(usuario);
      });
  }

  resetarDados(event) {
    event.preventDefault();
    this.buscarUsuario(this.auth.jwtPayLoad.user_name);
  }

  resetarAlteracaoSenha(event) {
    event.preventDefault();
    this.configurarFormGroupSenha();
  }

  alteracaoTab(event) {
    if (event.index === 0) {
      this.configurarFormGroupSenha();
    }
  }

  alterarDados() {
    if (this.formGroupPerfil.valid) {
      this.usuarioService.updateUsuario({
        id: this.id.value,
        nome: this.nome.value,
        sobrenome: this.sobrenome.value,
        email: this.email.value,
        login: this.login.value,
        role: this.role.value
      })
        .subscribe(usuario => {
          this.auth.jwtPayLoad.nome = usuario.nome;
          this.toastr.success('Seus dados foram salvos');
        });
    }
  }

  alterarSenha() {
    if (this.formGroupSenha.valid) {
      this.usuarioService.alterarSenha(this.usuarioLogado.id, this.senhaAtual.value, this.confirmacaoSenha.value)
        .subscribe(usuario => {
          this.toastr.success('Sua senha foi alterada');
        });
    }
  }
}
