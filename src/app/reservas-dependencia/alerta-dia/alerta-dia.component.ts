import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-alerta-dia',
  templateUrl: 'alerta-dia.component.html',
})
export class AlertaDiaComponent {

  constructor(
    public dialogRef: MatDialogRef<AlertaDiaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

}
