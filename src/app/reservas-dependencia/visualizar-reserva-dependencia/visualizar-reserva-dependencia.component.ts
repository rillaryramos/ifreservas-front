import { CarregamentoComponent } from './../../shared/carregamento/carregamento.component';
import { ErrorHandlerService } from './../../core/services/error-handler.service';
import { catchError } from 'rxjs/operators';
import { CancelarReservaUsoContinuoComponent } from './../cancelar-reserva-uso-continuo/cancelar-reserva-uso-continuo.component';
import { AuthService } from './../../seguranca/auth.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { DatePipe } from '@angular/common';
import { ReservaDependencia } from 'src/app/core/models/reserva-dependencia.model';
import { ReservaDependenciaService } from 'src/app/core/services/reserva-dependencia.service';
import { CadastroReservaDependenciaComponent } from '../cadastro-reserva-dependencia/cadastro-reserva-dependencia.component';
import { CancelarReservaDependenciaComponent } from '../cancelar-reserva-dependencia/cancelar-reserva-dependencia.component';
import { CadastroUsoContinuoComponent } from '../cadastro-uso-continuo/cadastro-uso-continuo.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-visualizar-reserva-dependencia',
  templateUrl: './visualizar-reserva-dependencia.component.html'
})
export class VisualizarReservaDependenciaComponent implements OnInit {

  reservaSelecionada: ReservaDependencia;

  constructor(
    public dialogRef: MatDialogRef<VisualizarReservaDependenciaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private reservaDependenciaService: ReservaDependenciaService,
    private datePipe: DatePipe,
    public dialog: MatDialog,
    private auth: AuthService,
    private toastr: ToastrService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.buscarReserva(this.data.id);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  buscarReserva(id: number) {
    this.reservaDependenciaService.getReservaById(id)
      .subscribe(reserva => {
        this.reservaSelecionada = reserva;
      });
  }

  maiorQueDataAtual() {
    let data;
    if (this.reservaSelecionada.usoContinuo) {
      data = this.data.start;
    } else {
      data = this.reservaSelecionada.start;
    }
    if (new Date(data) < new Date()) {
      return false;
    } else {
      return true;
    }
  }

  isUsuarioLogado() {
    return Boolean((this.auth.jwtPayLoad.id === this.reservaSelecionada.usuarioSolicitante.id && !this.reservaSelecionada.aula) || (this.auth.jwtPayLoad.authorities[0] === 'ROLE_ADMIN' && this.reservaSelecionada.aula));
  }

  openAlterarDialog(reserva: ReservaDependencia): void {
    if (!reserva.usoContinuo) {
      const dialogRef = this.dialog.open(CadastroReservaDependenciaComponent, {
        width: '450px',
        data: {
          id: reserva.id,
          title: reserva.title,
          start: new Date(reserva.start),
          end: new Date(reserva.end),
          horarioInicio: new Date(reserva.start).toTimeString().substring(0, 8),
          horarioTermino: new Date(reserva.end).toTimeString().substring(0, 8),
          observacao: reserva.observacao,
          dependencia: {
            id: reserva.dependencia.id
          }
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          // this.listarReservasPorVeiculo(this.veiculoSelecionado.id);
          this.dialogRef.close(true);
        }
      });
    } else {
      const dialogRef = this.dialog.open(CadastroUsoContinuoComponent, {
        width: '700px',
        maxHeight: '500px',
        data: {
          id: reserva.id,
          title: reserva.title,
          start: new Date(reserva.start),
          end: new Date(reserva.end),
          observacao: reserva.observacao,
          aula: reserva.aula,
          dependencia: {
            id: reserva.dependencia.id
          },
          usuarioUtilizador: reserva.usuarioUtilizador ? { id: reserva.usuarioUtilizador.id } : null,
          turma: reserva.turma ? { id: reserva.turma.id } : null,
          horarioReserva: reserva.horarioReserva
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.dialogRef.close(true);
          // this.listarReservasPorDependencia(this.dependenciaSelecionada.id);
        }
      });
    }
  }

  openCancelarDialog(reserva: ReservaDependencia): void {
    if (!reserva.usoContinuo) {
      const dialogRef = this.dialog.open(CancelarReservaDependenciaComponent, {
        data: reserva,
        width: '350px'
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.cancelar(null, null);
        }
      });
    } else {
      const dialogRef = this.dialog.open(CancelarReservaUsoContinuoComponent, {
        data: reserva,
        width: '350px'
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.cancelar(
            this.datePipe.transform(this.data.start, 'yyyy-MM-dd HH:mm:ss'),
            result === 'all' ? null : this.datePipe.transform(this.data.end, 'yyyy-MM-dd HH:mm:ss')
          );
        }
      });
    }
  }

  cancelar(inicio, fim) {
    const dialogRef1 = this.dialog.open(CarregamentoComponent, {
      disableClose: true,
      data: 'Cancelando reserva...',
      width: '350px'
    });
    return this.reservaDependenciaService.cancelarReserva(this.reservaSelecionada.id, inicio, fim)
      .pipe(
        catchError(error => {
          dialogRef1.close();
          throw this.errorHandler.handle(error);
        })
      )
      .subscribe(reserva => {
        dialogRef1.close();
        this.dialogRef.close(true);
        this.toastr.success('A reserva foi cancelada');
      });
  }
}
