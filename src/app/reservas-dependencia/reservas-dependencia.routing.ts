import { Routes } from '@angular/router';

import { ReservasDependenciaComponent } from './reservas-dependencia.component';

export const ReservasDependenciaRoutes: Routes = [{
  path: '',
  component: ReservasDependenciaComponent
}];
