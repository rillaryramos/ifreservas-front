import { CarregamentoComponent } from './../../shared/carregamento/carregamento.component';
import { AlertaCompatibilidadeReservaComponent } from './../alerta-compatibilildade-reserva/alerta-compatibilidade-reserva.component';
import { ErrorHandlerService } from './../../core/services/error-handler.service';
import { catchError } from 'rxjs/operators';
import { arrayHorariosDependencia } from './../../core/models/arrayHorariosDependencia';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ConfirmValidHorarioMatcher, HorarioValidator } from 'src/app/core/validators/hora.validator';
import { ReservaDependencia } from 'src/app/core/models/reserva-dependencia.model';
import { ReservaDependenciaService } from 'src/app/core/services/reserva-dependencia.service';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-cadastro-reserva-dependencia',
  templateUrl: './cadastro-reserva-dependencia.component.html',
  styleUrls: ['./cadastro-reserva-dependencia.component.scss']
})
export class CadastroReservaDependenciaComponent {

  newReservaDependenciaFormGroup: FormGroup;
  horarios: any[] = arrayHorariosDependencia;
  confirmValidMatcher: ConfirmValidHorarioMatcher;
  minDateInicial: Date;

  constructor(
    public dialogRef: MatDialogRef<CadastroReservaDependenciaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ReservaDependencia,
    private reservaDependenciaService: ReservaDependenciaService,
    private datePipe: DatePipe,
    private toastr: ToastrService,
    private errorHandler: ErrorHandlerService,
    public dialog: MatDialog
  ) {
    this.newReservaDependenciaFormGroup = new FormGroup(
      {
        title: new FormControl(data.title, Validators.required),
        start: new FormControl(data.start),
        horaFormGroup: new FormGroup(
          {
            horarioInicio: new FormControl(data.horarioInicio, Validators.required),
            horarioTermino: new FormControl(data.horarioTermino, Validators.required),
          },
          { validators: HorarioValidator.horaFinalMaior }
        ),
        observacao: new FormControl(data.observacao),
        idDependencia: new FormControl(data.dependencia.id)
      }
    );
    this.confirmValidMatcher = new ConfirmValidHorarioMatcher();
    this.minDateInicial = new Date();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get title() {
    return this.newReservaDependenciaFormGroup.controls.title;
  }

  get start() {
    return this.newReservaDependenciaFormGroup.controls.start;
  }

  get horarioInicio(): AbstractControl {
    return (<FormGroup>this.newReservaDependenciaFormGroup.controls.horaFormGroup).controls.horarioInicio;
  }

  get horarioTermino(): AbstractControl {
    return (<FormGroup>this.newReservaDependenciaFormGroup.controls.horaFormGroup).controls.horarioTermino;
  }

  get observacao() {
    return this.newReservaDependenciaFormGroup.controls.observacao;
  }

  get idDependencia(): AbstractControl {
    return this.newReservaDependenciaFormGroup.controls.idDependencia;
  }

  get editando() {
    return Boolean(this.data.id);
  }

  salvar() {
    if (this.editando) {
      this.atualizarReserva();
    } else {
      this.adicionarReserva();
    }
  }

  adicionarReserva() {
    if (this.newReservaDependenciaFormGroup.valid) {
      const dialogRef1 = this.dialog.open(CarregamentoComponent, {
        disableClose: true,
        data: 'Salvando reserva...',
        width: '350px'
      });
      this.reservaDependenciaService.createReservaUsoBreve({
        title: this.title.value,
        start: this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + 'T' + this.horarioInicio.value,
        end: this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + 'T' + this.horarioTermino.value,
        observacao: this.observacao.value,
        dependencia: {
          id: this.idDependencia.value
        },
        usoContinuo: false,
        aula: false
      })
        .pipe(
          catchError(error => {
            dialogRef1.close();
            if (error.status === 409) {
              const dialogRef = this.dialog.open(AlertaCompatibilidadeReservaComponent, {
                data: {
                  message: 'Sua solicitação de reserva tem compatibilidade de horário com outra(s) reserva(s).',
                  reservas: this.reservasCompatibilidade(error)
                },
                width: '420px',
                maxHeight: '600px'
              });
              return throwError(error);
            } else {
              throw this.errorHandler.handle(error);
            }
          })
        )
        .subscribe(reserva => {
          dialogRef1.close();
          this.dialogRef.close(this.newReservaDependenciaFormGroup);
          this.toastr.success('Reserva realizada');
        });
    }
  }

  atualizarReserva() {
    if (this.newReservaDependenciaFormGroup.valid) {
      const dialogRef1 = this.dialog.open(CarregamentoComponent, {
        disableClose: true,
        data: 'Salvando reserva...',
        width: '350px'
      });
      this.reservaDependenciaService.atualizarReservaUsoBreve({
        id: this.data.id,
        title: this.title.value,
        start: this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + 'T' + this.horarioInicio.value,
        end: this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + 'T' + this.horarioTermino.value,
        observacao: this.observacao.value,
        dependencia: {
          id: this.idDependencia.value
        },
        usoContinuo: false,
        aula: false
      })
        .pipe(
          catchError(error => {
            dialogRef1.close();
            if (error.status === 409) {
              const dialogRef = this.dialog.open(AlertaCompatibilidadeReservaComponent, {
                data: {
                  message: 'Sua solicitação de reserva tem compatibilidade de horário com outra(s) reserva(s).',
                  reservas: this.reservasCompatibilidade(error)
                },
                width: '420px',
                maxHeight: '600px'
              });
              return throwError(error);
            } else {
              throw this.errorHandler.handle(error);
            }
          })
        )
        .subscribe(reserva => {
          dialogRef1.close();
          this.dialogRef.close(this.newReservaDependenciaFormGroup);
          this.toastr.success('A reserva foi atualizada');
        });
    }
  }

  reservasCompatibilidade(error) {
    let arrayReservas = [];
    const data = new Date(this.start.value).toDateString().substring(0, 3);
    error.error.forEach(element => {
      if (!element.usoContinuo) {
        arrayReservas.push({
          title: element.title,
          start: element.start,
          end: element.end
        });
      } else {
        element.horarioReserva.map((fl) => {
          if (fl.diaSemana === data
            && (((this.horarioInicio.value >= fl.horarioInicio && this.horarioInicio.value < fl.horarioTermino)
              || (this.horarioTermino.value > fl.horarioInicio && this.horarioTermino.value <= fl.horarioTermino))
              || (fl.horarioInicio >= this.horarioInicio.value && fl.horarioInicio < this.horarioTermino.value))) {
            arrayReservas.push({
              title: element.title,
              start: new Date(this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + ' ' + fl.horarioInicio),
              end: new Date(this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + ' ' + fl.horarioTermino)
            });
          }
        });
      }
    });
    return arrayReservas;
  }

}
