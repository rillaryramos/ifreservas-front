import { CancelarReservaUsoContinuoComponent } from './cancelar-reserva-uso-continuo/cancelar-reserva-uso-continuo.component';
import { CarregamentoComponent } from './../shared/carregamento/carregamento.component';
import { AlertaCompatibilidadeReservaComponent } from './alerta-compatibilildade-reserva/alerta-compatibilidade-reserva.component';
import { AlertaDiaComponent } from './alerta-dia/alerta-dia.component';
import { AlertaCompatibilidadeHorarioComponent } from './alerta-compatibilidade-horario/alerta-compatibilidade-horario.component';
import { AlertaAlteracaoComponent } from './alerta-alteracao/alerta-alteracao.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, registerLocaleData, DatePipe } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { ReservasDependenciaComponent } from './reservas-dependencia.component';
import { ReservasDependenciaRoutes } from './reservas-dependencia.routing';
import { FullCalendarModule } from '@fullcalendar/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import ptBr from '@angular/common/locales/pt';
import { CadastroUsoContinuoComponent } from './cadastro-uso-continuo/cadastro-uso-continuo.component';
import { CadastroReservaDependenciaComponent } from './cadastro-reserva-dependencia/cadastro-reserva-dependencia.component';
import { VisualizarReservaDependenciaComponent } from './visualizar-reserva-dependencia/visualizar-reserva-dependencia.component';
import { CancelarReservaDependenciaComponent } from './cancelar-reserva-dependencia/cancelar-reserva-dependencia.component';
registerLocaleData(ptBr);

@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(ReservasDependenciaRoutes),
    FullCalendarModule,
    SharedModule
  ],
  declarations: [
    ReservasDependenciaComponent,
    CadastroReservaDependenciaComponent,
    CadastroUsoContinuoComponent,
    VisualizarReservaDependenciaComponent,
    CancelarReservaDependenciaComponent,
    AlertaAlteracaoComponent,
    AlertaCompatibilidadeHorarioComponent,
    AlertaDiaComponent,
    AlertaCompatibilidadeReservaComponent,
    CancelarReservaUsoContinuoComponent
  ],
  entryComponents: [
    CadastroReservaDependenciaComponent,
    VisualizarReservaDependenciaComponent,
    CancelarReservaDependenciaComponent,
    CadastroUsoContinuoComponent,
    AlertaAlteracaoComponent,
    AlertaCompatibilidadeHorarioComponent,
    AlertaDiaComponent,
    AlertaCompatibilidadeReservaComponent,
    CarregamentoComponent,
    CancelarReservaUsoContinuoComponent
  ],
  providers: [
    DatePipe
  ]
})
export class ReservasDependenciaModule {}
