import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-cancelar-reserva-dependencia',
    templateUrl: 'cancelar-reserva-dependencia.component.html',
})
export class CancelarReservaDependenciaComponent {

    constructor(
        public dialogRef: MatDialogRef<CancelarReservaDependenciaComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {}

    onNoClick(): void {
        this.dialogRef.close();
    }

}
