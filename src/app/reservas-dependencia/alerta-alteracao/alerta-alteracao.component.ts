import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-alerta-alteracao',
  templateUrl: 'alerta-alteracao.component.html',
})
export class AlertaAlteracaoComponent {

  constructor(
    public dialogRef: MatDialogRef<AlertaAlteracaoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

}
