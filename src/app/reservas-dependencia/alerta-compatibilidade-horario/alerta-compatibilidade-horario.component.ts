import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-alerta-compatibilidade-horario',
  templateUrl: 'alerta-compatibilidade-horario.component.html',
})
export class AlertaCompatibilidadeHorarioComponent {

  constructor(
    public dialogRef: MatDialogRef<AlertaCompatibilidadeHorarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

}
