import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-cancelar-reserva-uso-continuo',
  templateUrl: 'cancelar-reserva-uso-continuo.component.html',
  styleUrls: ['./cancelar-reserva-uso-continuo.component.scss']
})
export class CancelarReservaUsoContinuoComponent {
  cancelamentoFormGroup: FormGroup;
  acionou: boolean;

  constructor(
    public dialogRef: MatDialogRef<CancelarReservaUsoContinuoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.acionou = false;
    this.cancelamentoFormGroup = new FormGroup(
      {
        tipo: new FormControl(null, Validators.required)
      }
    );
    console.log(this.cancelamentoFormGroup);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  cancelar() {
    this.acionou = true;
    if (!this.cancelamentoFormGroup.invalid) {
      this.dialogRef.close(this.cancelamentoFormGroup.get('tipo').value);
    }
  }

}
