import { CarregamentoComponent } from './../../shared/carregamento/carregamento.component';
import { ErrorHandlerService } from './../../core/services/error-handler.service';
import { AlertaCompatibilidadeReservaComponent } from './../alerta-compatibilildade-reserva/alerta-compatibilidade-reserva.component';
import { AlertaDiaComponent } from './../alerta-dia/alerta-dia.component';
import { Dependencia } from './../../core/models/dependencia.model';
import { AlertaCompatibilidadeHorarioComponent } from './../alerta-compatibilidade-horario/alerta-compatibilidade-horario.component';
import { AuthService } from './../../seguranca/auth.service';
import { AlertaAlteracaoComponent } from './../alerta-alteracao/alerta-alteracao.component';
import { Turma } from './../../core/models/turma.model';
import { Usuario } from './../../core/models/reserva-veiculo.model';
import { ReservaDependencia } from './../../core/models/reserva-dependencia.model';
import { Component, ViewChild, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { TurmaService } from 'src/app/core/services/turma.service';
import { ReservaDependenciaService } from 'src/app/core/services/reserva-dependencia.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-cadastro-uso-continuo',
  templateUrl: './cadastro-uso-continuo.component.html',
  styleUrls: ['./cadastro-uso-continuo.component.scss']
})
export class CadastroUsoContinuoComponent implements OnInit {
  domingo = false;
  segunda = false;
  terca = false;
  quarta = false;
  quinta = false;
  sexta = false;
  sabado = false;
  horariosDomingo = [];
  horariosSegunda = [];
  horariosTerca = [];
  horariosQuarta = [];
  horariosQuinta = [];
  horariosSexta = [];
  horariosSabado = [];
  idObjetoSelecionado: number;
  nenhumDiaSelecionado = false;
  novaReservaFormGroup: FormGroup;
  minDateInicial: Date;
  diaHorarioReserva = [];
  usuarios: Usuario[];
  turmas: Turma[];
  horariosComCompatibilidade = [];
  salvarComoUsoContinuo = true;
  reservasUsoBreve = [];
  reservaDependencia: any;
  domingoHabilitado = false;
  segundaHabilitado = false;
  tercaHabilitado = false;
  quartaHabilitado = false;
  quintaHabilitado = false;
  sextaHabilitado = false;
  sabadoHabilitado = false;
  diasInvalidos = [];

  @ViewChild('componentDomingo') componentDomingoElement;
  @ViewChild('componentSegunda') componentSegundaElement;
  @ViewChild('componentTerca') componentTercaElement;
  @ViewChild('componentQuarta') componentQuartaElement;
  @ViewChild('componentQuinta') componentQuintaElement;
  @ViewChild('componentSexta') componentSextaElement;
  @ViewChild('componentSabado') componentSabadoElement;

  constructor(
    public dialogRef: MatDialogRef<CadastroUsoContinuoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ReservaDependencia,
    private usuarioService: UsuarioService,
    private turmaService: TurmaService,
    private reservaDependenciaService: ReservaDependenciaService,
    private datePipe: DatePipe,
    public dialog: MatDialog,
    private auth: AuthService,
    private toastr: ToastrService,
    private errorHandler: ErrorHandlerService
  ) {
    this.novaReservaFormGroup = new FormGroup({
      title: new FormControl(data.title, Validators.required),
      start: new FormControl(data.start, Validators.required),
      end: new FormControl(data.end, Validators.required),
      observacao: new FormControl(data.observacao),
      aula: new FormControl(false, Validators.required),
      utilizador: new FormControl(''),
      idUsuario: new FormControl(''),
      idTurma: new FormControl(''),
      idDependencia: new FormControl(data.dependencia.id)
    });
    this.minDateInicial = new Date();
  }

  ngOnInit() {
    if (this.editando) {
      this.alteracaoDatas();
      this.novaReservaFormGroup.setControl('aula', new FormControl({ value: this.data.aula, disabled: true }, Validators.required));
      this.novaReservaFormGroup.setControl('utilizador', new FormControl({ value: '', disabled: true }, Validators.required));
      if (this.data.aula) {
        if (this.data.usuarioUtilizador) {
          this.utilizador.setValue('servidor');
          this.idObjetoSelecionado = this.data.usuarioUtilizador.id;
          this.idUsuario.setValue(this.data.usuarioUtilizador.id);
        } else {
          this.utilizador.setValue('turma');
          this.idObjetoSelecionado = this.data.turma.id;
          this.idTurma.setValue(this.data.turma.id);
        }
      }
      this.data.horarioReserva.forEach(element => {
        if (element.diaSemana === 'Sun') {
          this.domingo = true;
          this.horariosDomingo.push(element);
        }
        if (element.diaSemana === 'Mon') {
          this.segunda = true;
          this.horariosSegunda.push(element);
        }
        if (element.diaSemana === 'Tue') {
          this.terca = true;
          this.horariosTerca.push(element);
        }
        if (element.diaSemana === 'Wed') {
          this.quarta = true;
          this.horariosQuarta.push(element);
        }
        if (element.diaSemana === 'Thu') {
          this.quinta = true;
          this.horariosQuinta.push(element);
        }
        if (element.diaSemana === 'Fri') {
          this.sexta = true;
          this.horariosSexta.push(element);
        }
        if (element.diaSemana === 'Sat') {
          this.sabado = true;
          this.horariosSabado.push(element);
        }
      });
    }
    this.listarUsuarios();
    this.listarTurmas();
  }

  alteracaoDatas() {
    // if (!this.editando) {
    if (this.start.value && this.end.value) {
      const start = new Date(this.start.value);
      const diaSemanaStart = start.toDateString().substring(0, 3);
      const end = new Date(this.end.value);
      const diaSemanaEnd = end.toDateString().substring(0, 3);
      const diferenca = moment(this.datePipe.transform(end, 'yyyy-MM-dd')).
        diff(moment(this.datePipe.transform(start, 'yyyy-MM-dd')), 'days');
      console.log(this.datePipe.transform(start, 'yyyy-MM-dd'));
      console.log(this.datePipe.transform(end, 'yyyy-MM-dd'));
      console.log(diferenca);
      if (diferenca === 0) {
        this.selecionarDiaSemana(diaSemanaStart);
        this.salvarComoUsoContinuo = false;
      } else if (diferenca > 0 && diferenca <= 6) {
        console.log('ENTROU!!!');
        this.salvarComoUsoContinuo = false;
        let entrouDomingo = 0;
        let entrouSegunda = 0;
        let entrouTerca = 0;
        let entrouQuarta = 0;
        let entrouQuinta = 0;
        let entrouSexta = 0;
        let entrouSabado = 0;
        const inicio = new Date(this.datePipe.transform(start, 'yyyy-MM-dd') + ' 00:00:00');
        const fim = new Date(this.datePipe.transform(end, 'yyyy-MM-dd') + ' 00:00:00');
        // console.log('DIA 1: ', new Date(this.datePipe.transform(start, 'yyyy-MM-dd') + ' 00:00:00'));
        // console.log('DIA 2: ', new Date(this.datePipe.transform(end, 'yyyy-MM-dd') + ' 00:00:00'));
        for (let dia = inicio; dia <= fim; dia.setDate(dia.getDate() + 1)) {
          if (dia.toDateString().substring(0, 3) === 'Sun') {
            this.domingoHabilitado = true;
            entrouDomingo += 1;
          } else if (dia.toDateString().substring(0, 3) === 'Mon') {
            this.segundaHabilitado = true;
            entrouSegunda += 1;
          } else if (dia.toDateString().substring(0, 3) === 'Tue') {
            this.tercaHabilitado = true;
            entrouTerca += 1;
          } else if (dia.toDateString().substring(0, 3) === 'Wed') {
            this.quartaHabilitado = true;
            entrouQuarta += 1;
          } else if (dia.toDateString().substring(0, 3) === 'Thu') {
            this.quintaHabilitado = true;
            entrouQuinta += 1;
          } else if (dia.toDateString().substring(0, 3) === 'Fri') {
            this.sextaHabilitado = true;
            entrouSexta += 1;
          } else {
            this.sabadoHabilitado = true;
            entrouSabado += 1;
          }
        }
        if (entrouDomingo === 0) {
          this.domingo = false;
          this.domingoHabilitado = false;
        }
        if (entrouSegunda === 0) {
          this.segunda = false;
          this.segundaHabilitado = false;
        }
        if (entrouTerca === 0) {
          this.terca = false;
          this.tercaHabilitado = false;
        }
        if (entrouQuarta === 0) {
          this.quarta = false;
          this.quartaHabilitado = false;
        }
        if (entrouQuinta === 0) {
          this.quinta = false;
          this.quintaHabilitado = false;
        }
        if (entrouSexta === 0) {
          this.sexta = false;
          this.sextaHabilitado = false;
        }
        if (entrouSabado === 0) {
          this.sabado = false;
          this.sabadoHabilitado = false;
        }
      } else if (diferenca < 0) {
        this.domingoHabilitado = false;
        this.segundaHabilitado = false;
        this.tercaHabilitado = false;
        this.quartaHabilitado = false;
        this.quintaHabilitado = false;
        this.sextaHabilitado = false;
        this.sabadoHabilitado = false;
      } else {
        this.domingoHabilitado = true;
        this.segundaHabilitado = true;
        this.tercaHabilitado = true;
        this.quartaHabilitado = true;
        this.quintaHabilitado = true;
        this.sextaHabilitado = true;
        this.sabadoHabilitado = true;
      }
    }
    // }
  }

  selecionarDiaSemana(dia: string) {
    if (dia === 'Sun') {
      this.domingoHabilitado = true;
      this.segunda = this.terca = this.quarta = this.quinta = this.sexta = this.sabado = false;
      this.segundaHabilitado = this.tercaHabilitado =
        this.quartaHabilitado = this.quintaHabilitado = this.sextaHabilitado = this.sabadoHabilitado = false;
    } else if (dia === 'Mon') {
      this.segundaHabilitado = true;
      this.domingo = this.terca = this.quarta = this.quinta = this.sexta = this.sabado = false;
      this.domingoHabilitado = this.tercaHabilitado =
        this.quartaHabilitado = this.quintaHabilitado = this.sextaHabilitado = this.sabadoHabilitado = false;
    } else if (dia === 'Tue') {
      this.tercaHabilitado = true;
      this.domingo = this.segunda = this.quarta = this.quinta = this.sexta = this.sabado = false;
      this.domingoHabilitado = this.segundaHabilitado =
        this.quartaHabilitado = this.quintaHabilitado = this.sextaHabilitado = this.sabadoHabilitado = false;
    } else if (dia === 'Wed') {
      this.quartaHabilitado = true;
      this.domingo = this.segunda = this.terca = this.quinta = this.sexta = this.sabado = false;
      this.domingoHabilitado = this.segundaHabilitado =
        this.tercaHabilitado = this.quintaHabilitado = this.sextaHabilitado = this.sabadoHabilitado = false;
    } else if (dia === 'Thu') {
      this.quintaHabilitado = true;
      this.domingo = this.segunda = this.terca = this.quarta = this.sexta = this.sabado = false;
      this.domingoHabilitado = this.segundaHabilitado =
        this.tercaHabilitado = this.quartaHabilitado = this.sextaHabilitado = this.sabadoHabilitado = false;
    } else if (dia === 'Fri') {
      this.sextaHabilitado = true;
      this.domingo = this.segunda = this.terca = this.quarta = this.quinta = this.sabado = false;
      this.domingoHabilitado = this.segundaHabilitado =
        this.tercaHabilitado = this.quartaHabilitado = this.quintaHabilitado = this.sabadoHabilitado = false;
    } else {
      this.sabadoHabilitado = true;
      this.domingo = this.segunda = this.terca = this.quarta = this.quinta = this.sexta = false;
      this.domingoHabilitado = this.segundaHabilitado =
        this.tercaHabilitado = this.quartaHabilitado = this.quintaHabilitado = this.sextaHabilitado = false;
    }
  }

  isCoordenador() {
    return Boolean(this.auth.jwtPayLoad.authorities[0] === 'ROLE_ADMIN');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get title(): AbstractControl {
    return this.novaReservaFormGroup.controls.title;
  }

  get start(): AbstractControl {
    return this.novaReservaFormGroup.controls.start;
  }

  get end(): AbstractControl {
    return this.novaReservaFormGroup.controls.end;
  }

  get aula(): AbstractControl {
    return this.novaReservaFormGroup.controls.aula;
  }

  get utilizador(): AbstractControl {
    return this.novaReservaFormGroup.controls.utilizador;
  }

  get idUsuario(): AbstractControl {
    return this.novaReservaFormGroup.controls.idUsuario;
  }

  get idTurma(): AbstractControl {
    return this.novaReservaFormGroup.controls.idTurma;
  }

  get idDependencia(): AbstractControl {
    return this.novaReservaFormGroup.controls.idDependencia;
  }

  get observacao(): AbstractControl {
    return this.novaReservaFormGroup.controls.observacao;
  }

  get editando() {
    return Boolean(this.data.id);
  }

  listarUsuarios() {
    this.usuarioService.getUsuariosAtivos()
      .subscribe((usuarios: Usuario[]) => {
        this.usuarios = usuarios;
      });
  }

  listarTurmas() {
    this.turmaService.getTurmasAtivas()
      .subscribe((turmas: Turma[]) => {
        this.turmas = turmas;
      });
  }

  changeAula() {
    if (!this.aula.value) {
      this.utilizador.setValue('');
      this.novaReservaFormGroup.setControl('utilizador', new FormControl(''));
      this.novaReservaFormGroup.setControl('idUsuario', new FormControl(''));
      this.novaReservaFormGroup.setControl('idTurma', new FormControl(''));
    }
  }

  changeUtilizador() {
    if (this.utilizador.value === 'servidor') {
      this.novaReservaFormGroup.setControl('idUsuario', new FormControl('', Validators.required));
      this.novaReservaFormGroup.setControl('idTurma', new FormControl(''));
    } else {
      this.novaReservaFormGroup.setControl('idTurma', new FormControl('', Validators.required));
      this.novaReservaFormGroup.setControl('idUsuario', new FormControl(''));
    }
  }

  aoSelecionar(evento) {
    if (this.utilizador.value === 'servidor') {
      this.idUsuario.setValue(evento.id);
      this.idTurma.setValue('');
    } else {
      this.idUsuario.setValue('');
      this.idTurma.setValue(evento.id);
    }
    // this.dependenciaSelecionada = evento;
  }

  verificarSelecao() {
    if (!this.domingo && !this.segunda && !this.terca && !this.quarta && !this.quinta && !this.sexta && !this.sabado) {
      this.nenhumDiaSelecionado = true;
      return true;
    } else {
      this.nenhumDiaSelecionado = false;
      return false;
    }
  }

  salvar() {
    if (!this.aula.value) {
      this.utilizador.setValue('');
      this.novaReservaFormGroup.setControl('utilizador', new FormControl(''));
      this.novaReservaFormGroup.setControl('idUsuario', new FormControl(''));
      this.novaReservaFormGroup.setControl('idTurma', new FormControl(''));
    }
    this.diaHorarioReserva = [];
    if (this.editando) {
      this.atualizarReserva();
    } else {
      this.adicionarReserva();
    }
  }

  adicionarReserva() {
    const dataAtual = new Date();
    let horario = 'T';
    if (this.datePipe.transform(this.start.value, 'yyyy-MM-dd') === this.datePipe.transform(dataAtual, 'yyyy-MM-dd')) {
      horario = horario + this.datePipe.transform(dataAtual, 'HH:mm');
    } else {
      horario = horario + '00:00';
    }
    this.verificarSelecao();
    const invalido = this.validarHorariosReserva();
    if (this.diasInvalidos.length > 0) {
      this.openAlertaDia();
    }
    if (this.horariosComCompatibilidade.length > 0) {
      this.openAlertaCompatibilidade();
    }
    let usoContinuo = true;
    if (this.novaReservaFormGroup.valid && !this.verificarSelecao() && !invalido) {
      if (this.diaHorarioReserva.length === 1 &&
        ((this.domingo && !this.segunda && !this.terca && !this.quarta && !this.quinta && !this.sexta && !this.sabado)
          || (!this.domingo && this.segunda && !this.terca && !this.quarta && !this.quinta && !this.sexta && !this.sabado)
          || (!this.domingo && !this.segunda && this.terca && !this.quarta && !this.quinta && !this.sexta && !this.sabado)
          || (!this.domingo && !this.segunda && !this.terca && this.quarta && !this.quinta && !this.sexta && !this.sabado)
          || (!this.domingo && !this.segunda && !this.terca && !this.quarta && this.quinta && !this.sexta && !this.sabado)
          || (!this.domingo && !this.segunda && !this.terca && !this.quarta && !this.quinta && this.sexta && !this.sabado)
          || (!this.domingo && !this.segunda && !this.terca && !this.quarta && !this.quinta && !this.sexta && this.sabado))) {
        let diaSelecionado;
        if (this.domingo) {
          diaSelecionado = 'Sun';
        } else if (this.segunda) {
          diaSelecionado = 'Mon';
        } else if (this.terca) {
          diaSelecionado = 'Tue';
        } else if (this.quarta) {
          diaSelecionado = 'Wed';
        } else if (this.quinta) {
          diaSelecionado = 'Thu';
        } else if (this.sexta) {
          diaSelecionado = 'Fri';
        } else {
          diaSelecionado = 'Sat';
        }
        const start = new Date(this.start.value);
        const end = new Date(this.end.value);
        let i = 0;
        for (let dia = new Date(this.start.value); dia <= new Date(this.end.value); dia.setDate(dia.getDate() + 1)) {
          if (dia.toDateString().substring(0, 3) === diaSelecionado) {
            i++;
          }
          if (i > 1) {
            usoContinuo = true;
            break;
          } else {
            usoContinuo = false;
          }
        }
        if (!usoContinuo) {
          for (let dia2 = new Date(this.start.value); dia2 <= new Date(this.end.value); dia2.setDate(dia2.getDate() + 1)) {
            if (dia2.toDateString().substring(0, 3) === diaSelecionado) {
              console.log('1', dia2);
              const diaReserva = this.datePipe.transform(dia2, 'yyyy-MM-dd');
              const dialogRef1 = this.dialog.open(CarregamentoComponent, {
                disableClose: true,
                data: 'Salvando reserva...',
                width: '350px'
              });
              this.reservaDependenciaService.createReservaUsoBreve({
                title: this.title.value,
                start: this.datePipe.transform(dia2, 'yyyy-MM-dd') + 'T' + this.diaHorarioReserva[0].horarioInicio,
                end: this.datePipe.transform(dia2, 'yyyy-MM-dd') + 'T' + this.diaHorarioReserva[0].horarioTermino,
                observacao: this.observacao.value,
                dependencia: {
                  id: this.idDependencia.value
                },
                usuarioUtilizador: this.idUsuario.value ? { id: this.idUsuario.value } : null,
                turma: this.idTurma.value ? { id: this.idTurma.value } : null,
                usoContinuo: false,
                aula: this.aula.value,
              })
                .pipe(
                  catchError(error => {
                    dialogRef1.close();
                    if (error.status === 409) {
                      const data = new Date(diaReserva + ' 00:00:00');
                      const dialogRef = this.dialog.open(AlertaCompatibilidadeReservaComponent, {
                        data: {
                          message: 'Sua solicitação de reserva tem compatibilidade de horário com outra(s) reserva(s).',
                          reservas: this.reservasCompatibilidade(error, data)
                        },
                        width: '420px',
                        maxHeight: '600px'
                      });
                      return throwError(error);
                    } else {
                      throw this.errorHandler.handle(error);
                    }
                  })
                )
                .subscribe(reserva => {
                  dialogRef1.close();
                  this.dialogRef.close(this.novaReservaFormGroup);
                  this.toastr.success('Reserva realizada');
                });
            }
          }

        } else {
          this.salvarUsoContinuo(horario);
        }
      } else {
        this.salvarUsoContinuo(horario);
      }
    } else if (!invalido) {
      this.toastr.error('Preencha os campos obrigatórios');
    }
  }

  salvarUsoContinuo(horario) {
    const dialogRef1 = this.dialog.open(CarregamentoComponent, {
      disableClose: true,
      data: 'Salvando reserva...',
      width: '350px'
    });
    this.reservaDependenciaService.createReservaUsoContinuo({
      title: this.title.value,
      start: this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + horario,
      end: this.datePipe.transform(this.end.value, 'yyyy-MM-dd') + 'T00:00',
      observacao: this.observacao.value,
      dependencia: {
        id: this.idDependencia.value
      },
      usuarioUtilizador: this.idUsuario.value ? { id: this.idUsuario.value } : null,
      turma: this.idTurma.value ? { id: this.idTurma.value } : null,
      usoContinuo: true,
      aula: this.aula.value,
      horarioReserva: this.diaHorarioReserva
    })
      .pipe(
        catchError(error => {
          dialogRef1.close();
          if (error.status === 409) {
            const dialogRef = this.dialog.open(AlertaCompatibilidadeReservaComponent, {
              data: {
                message: 'Sua solicitação de reserva tem compatibilidade de horário com outra(s) reserva(s).',
                reservas: this.reservasCompatibilidadeParaUsoContinuo(error)
              },
              width: '420px',
              maxHeight: '600px'
            });
            return throwError(error);
          } else {
            throw this.errorHandler.handle(error);
          }
        })
      )
      .subscribe(reserva => {
        dialogRef1.close();
        this.dialogRef.close(this.novaReservaFormGroup);
        this.toastr.success('Reserva realizada');
      });
  }

  reservasCompatibilidade(error, data) {
    let arrayReservas = [];
    const diaData = data.toDateString().substring(0, 3);
    error.error.forEach(element => {
      if (!element.usoContinuo) {
        arrayReservas.push({
          title: element.title,
          start: element.start,
          end: element.end
        });
      } else {
        element.horarioReserva.map((fl) => {
          if (fl.diaSemana === diaData
            && (((this.diaHorarioReserva[0].horarioInicio >= fl.horarioInicio && this.diaHorarioReserva[0].horarioInicio < fl.horarioTermino)
              || (this.diaHorarioReserva[0].horarioTermino > fl.horarioInicio && this.diaHorarioReserva[0].horarioTermino <= fl.horarioTermino))
              || (fl.horarioInicio >= this.diaHorarioReserva[0].horarioInicio && fl.horarioInicio < this.diaHorarioReserva[0].horarioTermino))) {
            arrayReservas.push({
              title: element.title,
              start: new Date(this.datePipe.transform(data, 'yyyy-MM-dd') + ' ' + fl.horarioInicio),
              end: new Date(this.datePipe.transform(data, 'yyyy-MM-dd') + ' ' + fl.horarioTermino)
            });
          }
        });
      }
    });
    return arrayReservas;
  }

  reservasCompatibilidadeParaUsoContinuo(error) {
    let arrayReservas = [];
    let horarios = [];
    error.error.forEach(element => {
      if (!element.usoContinuo) {
        arrayReservas.push({
          title: element.title,
          start: element.start,
          end: element.end
        });
      } else {
        horarios = [];
        element.horarioReserva.map((fl) => {
          this.diaHorarioReserva.map((hr) => {
            if (fl.diaSemana === hr.diaSemana) {
              if (((hr.horarioInicio >= fl.horarioInicio && hr.horarioInicio < fl.horarioTermino)
                || (hr.horarioTermino > fl.horarioInicio && hr.horarioTermino <= fl.horarioTermino))
                || (fl.horarioInicio >= hr.horarioInicio && fl.horarioInicio < hr.horarioTermino)) {
                horarios.push(fl);
              }
            }
          });
        });
        if (horarios.length > 0) {
          arrayReservas.push({
            title: element.title,
            start: element.start,
            end: element.end,
            horarioReserva: horarios
          });
        }
      }
    });
    return arrayReservas;
  }

  atualizarReserva() {
    console.log('AQUI');
    if (!this.start.valid) {
      this.openAlertaAlteracao();
    }
    this.verificarSelecao();
    this.diaHorarioReserva = []; //VERIFICAR
    const invalido = this.validarHorariosReserva();
    if (this.diasInvalidos.length > 0) {
      this.openAlertaDia();
    }
    if (this.horariosComCompatibilidade.length > 0) {
      this.openAlertaCompatibilidade();
    }
    if (this.novaReservaFormGroup.valid && !this.verificarSelecao() && !invalido) {
      const dialogRef1 = this.dialog.open(CarregamentoComponent, {
        disableClose: true,
        data: 'Salvando reserva...',
        width: '350px'
      });
      this.reservaDependenciaService.atualizarReservaUsoContinuo({
        id: this.data.id,
        title: this.title.value,
        start: this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + 'T00:00',
        end: this.datePipe.transform(this.end.value, 'yyyy-MM-dd') + 'T00:00',
        observacao: this.observacao.value,
        dependencia: {
          id: this.idDependencia.value
        },
        usuarioUtilizador: this.idUsuario.value ? { id: this.idUsuario.value } : null,
        turma: this.idTurma.value ? { id: this.idTurma.value } : null,
        usoContinuo: true,
        aula: this.aula.value,
        horarioReserva: this.diaHorarioReserva
      })
        .pipe(
          catchError(error => {
            console.log(error);
            dialogRef1.close();
            if (error.status === 409) {
              const dialogRef = this.dialog.open(AlertaCompatibilidadeReservaComponent, {
                data: {
                  message: 'Sua solicitação de reserva tem compatibilidade de horário com outra(s) reserva(s).',
                  reservas: this.reservasCompatibilidadeParaUsoContinuo(error)
                },
                width: '420px',
                maxHeight: '600px'
              });
              return throwError(error);
            } else {
              throw this.errorHandler.handle(error);
            }
          })
        )
        .subscribe(reserva => {
          dialogRef1.close();
          this.dialogRef.close(this.novaReservaFormGroup);
          this.toastr.success('A reserva foi atualizada');
        });
    } else if (!invalido) {
      this.toastr.error('Preencha os campos obrigatórios');
    }
  }

  openAlertaCompatibilidade(): void {
    const dialogRef = this.dialog.open(AlertaCompatibilidadeHorarioComponent, {
      data: this.horariosComCompatibilidade,
      width: '400px'
    });
  }

  openAlertaDia(): void {
    const dialogRef = this.dialog.open(AlertaDiaComponent, {
      data: this.diasInvalidos,
      width: '400px'
    });
  }

  openAlertaAlteracao(): void {
    const dialogRef = this.dialog.open(AlertaAlteracaoComponent, {
      data: null,
      width: '350px'
    });
  }

  validarHorariosReserva() {
    let invalido = false;
    this.horariosComCompatibilidade = [];
    this.diasInvalidos = [];
    if (this.domingo && this.componentDomingoElement.teste()) {
      this.componentDomingoElement.teste().forEach(element => {
        if (this.componentDomingoElement.teste().filter((fl) => {
          return (((element.horarioInicio >= fl.horarioInicio && element.horarioInicio < fl.horarioTermino)
            || (element.horarioTermino > fl.horarioInicio && element.horarioTermino <= fl.horarioTermino))
            || (fl.horarioInicio >= element.horarioInicio && fl.horarioInicio < element.horarioTermino));
        }).length > 1) {
          this.horariosComCompatibilidade.push(element);
        }
      });
      if (this.horariosComCompatibilidade.length > 0) {
        return true;
      }
      this.componentDomingoElement.teste().forEach(re => this.diaHorarioReserva.push(re));
    } else if (this.domingo && !this.componentDomingoElement.teste()) {
      invalido = true;
      this.diasInvalidos.push('domingo');
    }

    if (this.segunda && this.componentSegundaElement.teste()) {
      this.componentSegundaElement.teste().forEach(element => {
        if (this.componentSegundaElement.teste().filter((fl) => {
          return (((element.horarioInicio >= fl.horarioInicio && element.horarioInicio < fl.horarioTermino)
            || (element.horarioTermino > fl.horarioInicio && element.horarioTermino <= fl.horarioTermino))
            || (fl.horarioInicio >= element.horarioInicio && fl.horarioInicio < element.horarioTermino));
        }).length > 1) {
          this.horariosComCompatibilidade.push(element);
        }
      });
      if (this.horariosComCompatibilidade.length > 0) {
        return true;
      }
      this.componentSegundaElement.teste().forEach(re => this.diaHorarioReserva.push(re));
    } else if (this.segunda && !this.componentSegundaElement.teste()) {
      invalido = true;
      this.diasInvalidos.push('segunda');
    }

    if (this.terca && this.componentTercaElement.teste()) {
      this.componentTercaElement.teste().forEach(element => {
        if (this.componentTercaElement.teste().filter((fl) => {
          return (((element.horarioInicio >= fl.horarioInicio && element.horarioInicio < fl.horarioTermino)
            || (element.horarioTermino > fl.horarioInicio && element.horarioTermino <= fl.horarioTermino))
            || (fl.horarioInicio >= element.horarioInicio && fl.horarioInicio < element.horarioTermino));
        }).length > 1) {
          this.horariosComCompatibilidade.push(element);
        }
      });
      if (this.horariosComCompatibilidade.length > 0) {
        return true;
      }
      this.componentTercaElement.teste().forEach(re => this.diaHorarioReserva.push(re));
    } else if (this.terca && !this.componentTercaElement.teste()) {
      invalido = true;
      this.diasInvalidos.push('terça');
    }

    if (this.quarta && this.componentQuartaElement.teste()) {
      this.componentQuartaElement.teste().forEach(element => {
        if (this.componentQuartaElement.teste().filter((fl) => {
          return (((element.horarioInicio >= fl.horarioInicio && element.horarioInicio < fl.horarioTermino)
            || (element.horarioTermino > fl.horarioInicio && element.horarioTermino <= fl.horarioTermino))
            || (fl.horarioInicio >= element.horarioInicio && fl.horarioInicio < element.horarioTermino));
        }).length > 1) {
          this.horariosComCompatibilidade.push(element);
        }
      });
      if (this.horariosComCompatibilidade.length > 0) {
        return true;
      }
      this.componentQuartaElement.teste().forEach(re => this.diaHorarioReserva.push(re));
    } else if (this.quarta && !this.componentQuartaElement.teste()) {
      invalido = true;
      this.diasInvalidos.push('quarta');
    }

    if (this.quinta && this.componentQuintaElement.teste()) {
      this.componentQuintaElement.teste().forEach(element => {
        if (this.componentQuintaElement.teste().filter((fl) => {
          return (((element.horarioInicio >= fl.horarioInicio && element.horarioInicio < fl.horarioTermino)
            || (element.horarioTermino > fl.horarioInicio && element.horarioTermino <= fl.horarioTermino))
            || (fl.horarioInicio >= element.horarioInicio && fl.horarioInicio < element.horarioTermino));
        }).length > 1) {
          this.horariosComCompatibilidade.push(element);
        }
      });
      if (this.horariosComCompatibilidade.length > 0) {
        return true;
      }
      this.componentQuintaElement.teste().forEach(re => this.diaHorarioReserva.push(re));
    } else if (this.quinta && !this.componentQuintaElement.teste()) {
      invalido = true;
      this.diasInvalidos.push('quinta');
    }

    if (this.sexta && this.componentSextaElement.teste()) {
      this.componentSextaElement.teste().forEach(element => {
        if (this.componentSextaElement.teste().filter((fl) => {
          return (((element.horarioInicio >= fl.horarioInicio && element.horarioInicio < fl.horarioTermino)
            || (element.horarioTermino > fl.horarioInicio && element.horarioTermino <= fl.horarioTermino))
            || (fl.horarioInicio >= element.horarioInicio && fl.horarioInicio < element.horarioTermino));
        }).length > 1) {
          this.horariosComCompatibilidade.push(element);
        }
      });
      if (this.horariosComCompatibilidade.length > 0) {
        return true;
      }
      this.componentSextaElement.teste().forEach(re => this.diaHorarioReserva.push(re));
    } else if (this.sexta && !this.componentSextaElement.teste()) {
      invalido = true;
      this.diasInvalidos.push('sexta');
    }

    if (this.sabado && this.componentSabadoElement.teste()) {
      this.componentSabadoElement.teste().forEach(element => {
        if (this.componentSabadoElement.teste().filter((fl) => {
          return (((element.horarioInicio >= fl.horarioInicio && element.horarioInicio < fl.horarioTermino)
            || (element.horarioTermino > fl.horarioInicio && element.horarioTermino <= fl.horarioTermino))
            || (fl.horarioInicio >= element.horarioInicio && fl.horarioInicio < element.horarioTermino));
        }).length > 1) {
          this.horariosComCompatibilidade.push(element);
        }
      });
      if (this.horariosComCompatibilidade.length > 0) {
        return true;
      }
      this.componentSabadoElement.teste().forEach(re => this.diaHorarioReserva.push(re));
    } else if (this.sabado && !this.componentSabadoElement.teste()) {
      invalido = true;
      this.diasInvalidos.push('sábado');
    }
    return invalido;
  }

}
