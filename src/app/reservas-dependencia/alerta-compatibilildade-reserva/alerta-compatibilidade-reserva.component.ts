import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-alerta-compatibilidade-reserva',
  templateUrl: 'alerta-compatibilidade-reserva.component.html',
})
export class AlertaCompatibilidadeReservaComponent {

  constructor(
    public dialogRef: MatDialogRef<AlertaCompatibilidadeReservaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

}
