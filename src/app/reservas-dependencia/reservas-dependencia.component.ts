import { Title } from '@angular/platform-browser';
import { Dependencia } from './../core/models/dependencia.model';
import { VisualizarReservaDependenciaComponent } from './visualizar-reserva-dependencia/visualizar-reserva-dependencia.component';
import { ReservaVeiculo } from 'src/app/core/models/reserva-veiculo.model';
import { Component, ViewChild, OnInit } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { ReservaDependenciaService } from '../core/services/reserva-dependencia.service';
import { ReservaDependencia } from '../core/models/reserva-dependencia.model';
import { CadastroReservaDependenciaComponent } from './cadastro-reserva-dependencia/cadastro-reserva-dependencia.component';
import { DependenciaService } from '../core/services/dependencia.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CadastroUsoContinuoComponent } from './cadastro-uso-continuo/cadastro-uso-continuo.component';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
// import * as moment from 'moment-timezone';

@Component({
  selector: 'app-reservas-dependencia',
  templateUrl: './reservas-dependencia.component.html',
  styleUrls: ['./reservas-dependencia.component.scss']
})
export class ReservasDependenciaComponent implements OnInit {

  @ViewChild('calendar') calendarComponent: FullCalendarComponent; // the #calendar in the template

  dependencias: Dependencia[];
  dependenciaSelecionada: Dependencia;
  // calendarVisible = true;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeekends = true;
  // reservas = new Array(ReservaDependencia);
  reservas = [];
  calendarEvents: EventInput[];
  firstFormGroup: FormGroup;
  exibirPor = 'minhas';

  constructor(
    public dialog: MatDialog,
    private datePipe: DatePipe,
    private reservaDependenciaService: ReservaDependenciaService,
    private dependenciaService: DependenciaService,
    private formBuilder: FormBuilder,
    private title: Title,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.title.setTitle('Reservas de dependência');
    this.listarDependencias();
    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
  }

  aoSelecionar(evento) {
    this.dependenciaSelecionada = evento;
    if (!this.dependenciaSelecionada.ativo) {
      this.toastr.error('Dependência selecionada está indisponível');
    } else {
      this.toastr.clear();
    }
    this.firstFormGroup.controls.firstCtrl.setValue('Selecionado');
  }

  changeStep(arg) {
    if (arg.selectedIndex === 1) {
      this.listarReservasPorDependencia();
    }
  }

  handleEventClick(arg) {
    const dialogRef = this.dialog.open(VisualizarReservaDependenciaComponent, {
      width: '420px',
      data: {
        id: arg.event.id,
        start: arg.event.start,
        end: arg.event.end
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listarReservasPorDependencia();
      }
    });
  }

  handleDateClick(arg) {
    this.toastr.clear();
    const horario = this.datePipe.transform(arg.date, 'HH:mm');
    let horaInicial;
    if (horario === '07:00' || horario === '07:30') {
      horaInicial = '07:15:00';
    } else if (horario === '08:00') {
      horaInicial = '08:05:00';
    } else if (horario === '08:30' || horario === '09:00') {
      horaInicial = '08:55:00';
    } else if (horario === '09:30') {
      horaInicial = '09:45:00';
    } else if (horario === '10:00') {
      horaInicial = '10:00:00';
    } else if (horario === '10:30' || horario === '11:00') {
      horaInicial = '10:50:00';
    } else if (horario === '11:30' || horario === '12:00' || horario === '12:30' || horario === '13:00') {
      horaInicial = '11:40:00';
    } else if (horario === '13:30') {
      horaInicial = '13:30:00';
    } else if (horario === '14:00' || horario === '14:30') {
      horaInicial = '14:20:00';
    } else if (horario === '15:00' || horario === '15:30') {
      horaInicial = '15:10:00';
    } else if (horario === '16:00') {
      horaInicial = '16:00:00';
    } else if (horario === '16:30') {
      horaInicial = '16:15:00';
    } else if (horario === '17:00') {
      horaInicial = '17:05:00';
    } else if (horario === '17:30' || horario === '18:00' || horario === '18:30') {
      horaInicial = '17:55:00';
    } else if (horario === '19:00') {
      horaInicial = '19:00:00';
    } else if (horario === '19:30' || horario === '20:00') {
      horaInicial = '19:50:00';
    } else if (horario === '20:30') {
      horaInicial = '20:40:00';
    } else if (horario === '21:00') {
      horaInicial = '20:50:00';
    } else if (horario === '21:30' || horario === '22:00') {
      horaInicial = '21:40:00';
    } else if (horario === '22:00') {
      horaInicial = '21:40:00';
    } else if (horario === '22:00') {
      horaInicial = '22:30:00';
    } else {
      horaInicial = null;
    }
    const dataAtual = new Date();
    if (this.dependenciaSelecionada.ativo) {
      if ((arg.date > dataAtual) || (this.datePipe.transform(arg.date, 'dd/MM/yyyy') === this.datePipe.transform(dataAtual, 'dd/MM/yyyy'))) {
        const dialogRef = this.dialog.open(CadastroReservaDependenciaComponent, {
          width: '450px',
          data: {
            start: arg.date,
            horarioInicio: horaInicial,
            dependencia: {
              id: this.dependenciaSelecionada.id
            }
          }
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.listarReservasPorDependencia();
          }
        });
      } else {
        this.toastr.error('Só é possível realizar uma reserva a partir do dia atual');
      }
    } else {
      this.toastr.error('Dependência selecionada está indisponível');
    }
  }

  addUsoContinuo() {
    const dialogRef = this.dialog.open(CadastroUsoContinuoComponent, {
      width: '700px',
      // maxHeight: '500px',
      maxHeight: '600px',
      data: {
        dependencia: {
          id: this.dependenciaSelecionada.id
        }
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listarReservasPorDependencia();
      }
    });
  }

  transformarReservas(reserva) {
    reserva.horarioReserva.forEach(element => {
      let start = null;
      let end = null;
      if (moment(reserva.end).isDST()) {
        start = moment.utc(reserva.start);
        end = moment.utc(reserva.end);
      } else {
        start = moment(reserva.start);
        end = moment(reserva.end);
      }
      let incrementar = 0;
      while (start <= end) {
        let continuar = true;
        if (incrementar === 0 && start.toLocaleString().substring(0, 3).toLocaleUpperCase() === element.diaSemana.toLocaleUpperCase()) {
          const horasInicial = element.horarioInicio.substring(0, 2);
          const minutosInicial = element.horarioInicio.substring(3, 5);
          if (moment(start) > moment(start).set('hour', horasInicial).set('minute', minutosInicial)) {
            continuar = false;
          }
        }
        if (continuar && start.toLocaleString().substring(0, 3).toLocaleUpperCase() === element.diaSemana.toLocaleUpperCase()) {
          let data = this.datePipe.transform(start, 'yyyy-MM-dd');
          if (moment(reserva.end).isDST()) {
            data = this.datePipe.transform(moment(start).add(1, 'day'), 'yyyy-MM-dd');
          }
          this.reservas.push({
            id: reserva.id,
            title: reserva.title,
            start: data + 'T' + element.horarioInicio,
            end: data + 'T' + element.horarioTermino,
            usoContinuo: true,
            aula: reserva.aula,
            horarioReserva: null,
            status: {
              id: reserva.status.id
            }
            // backgroundColor: '#34bb34',
            // borderColor: '#34bb34'
          });
        }
        incrementar++;
        start = start.add(1, 'day');
      }
    });
  }

  listarDependencias() {
    this.dependenciaService.getDependencias()
      .subscribe((dependencias: Dependencia[]) => {
        this.dependencias = dependencias;
      });
  }

  listarReservasPorDependencia() {
    this.reservaDependenciaService.getReservasByDependencia(this.dependenciaSelecionada.id, this.exibirPor)
      .subscribe((reservas: ReservaDependencia[]) => {
        this.reservas = [];
        reservas.forEach(re => re.usoContinuo ? this.transformarReservas(re) : this.reservas.push(re));
        console.log(this.reservas);
        this.reservas.map(re => {
          re.status.id === 5 ? (re.backgroundColor = '#9d37d8', re.borderColor = '#9d37d8') :
          re.aula === true ? (re.backgroundColor = '#34bb34', re.borderColor = '#34bb34') :
          (re.backgroundColor = '#3788d8', re.borderColor = '#3788d8');
        });
        this.calendarEvents = this.reservas;
      });
  }

}
