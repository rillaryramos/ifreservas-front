import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-compatibilidade-horario-reserva-aprovada',
  templateUrl: 'compatibilidade-horario-reserva-aprovada.component.html',
})
export class CompatibilidadeHorarioReservaAprovadaComponent {

  constructor(
    public dialogRef: MatDialogRef<CompatibilidadeHorarioReservaAprovadaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

}
