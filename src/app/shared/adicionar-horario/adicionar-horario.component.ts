import { arrayHorariosDependencia } from './../../core/models/arrayHorariosDependencia';
import { Component, Input, Output, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { HorarioValidator, ConfirmValidHorarioMatcher } from 'src/app/core/validators/hora.validator';

@Component({
  selector: 'app-adicionar-horario',
  templateUrl: './adicionar-horario.component.html',
  styleUrls: ['./adicionar-horario.component.scss']
})
export class AdicionarHorarioComponent implements OnInit {

  @Input() diaSemana: string;
  @Input() horariosEdicao: Array<any>;
  @Output() horarioSelecionado = new EventEmitter();
  @ViewChild('formRef') formRef: FormGroupDirective;
  horarios: any[] = arrayHorariosDependencia;
  confirmValidMatcher: ConfirmValidHorarioMatcher;
  confirmArray: ConfirmValidHorarioMatcher;
  horariosFormGroup: FormGroup;

  constructor() {
    this.confirmValidMatcher = new ConfirmValidHorarioMatcher();
  }

  ngOnInit() {
    this.horariosFormGroup = new FormGroup({
      horariosArray: new FormArray([])
    });

    if (this.horariosEdicao.length > 0) {
      for (let i = 0; i < this.horariosEdicao.length; ++i) {
        this.addBudgetItem({
          diaSemana: this.horariosEdicao[i].diaSemana,
          horarioInicio: this.horariosEdicao[i].horarioInicio,
          horarioTermino: this.horariosEdicao[i].horarioTermino
        });
      }
      console.log(this.horariosFormGroup.controls.horariosArray);
    } else {
      this.addBudgetItem(null);
    }
  }

  get horariosArray() {
    return this.horariosFormGroup.controls.horariosArray as FormArray;
  }

  addBudgetItem(budget) {
    if (budget) {
      this.horariosArray.push(
        new FormGroup({
          diaSemana: new FormControl(budget.diaSemana),
          horarioInicio: new FormControl(budget.horarioInicio, Validators.required),
          horarioTermino: new FormControl(budget.horarioTermino, Validators.required)
        },
        { validators: HorarioValidator.horaFinalMaior }
        )
      );
    } else {
      this.horariosArray.push(
        new FormGroup({
          diaSemana: new FormControl(this.diaSemana),
          horarioInicio: new FormControl('', Validators.required),
          horarioTermino: new FormControl('', Validators.required)
        },
        { validators: HorarioValidator.horaFinalMaior }
        )
      );
    }
  }

  deleteBudgetItem(index: number) {
    if (this.horariosArray.value.length > 1) {
      this.horariosArray.removeAt(index);
    }
  }

  public teste() {
    this.formRef.onSubmit(undefined);
    return this.formRef.invalid ? null : this.horariosArray.value;
  }
}

