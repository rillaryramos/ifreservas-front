import { ErrorHandlerService } from './../../core/services/error-handler.service';
import { CarregamentoComponent } from './../carregamento/carregamento.component';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Motorista } from 'src/app/core/models/motorista.model';
import { ReservaVeiculoService } from 'src/app/core/services/reserva-veiculo.service';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { ReservaVeiculo } from 'src/app/core/models/reserva-veiculo.model';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cancelar-rejeitar-reserva-veiculo',
  templateUrl: 'cancelar-rejeitar-reserva.component.html',
})
export class CancelarRejeitarReservaComponent {
  rejeicaoFormGroup: FormGroup;
  motoristas: Motorista[];
  botoesDesabilitados: boolean;

  constructor(
    public dialogRef: MatDialogRef<CancelarRejeitarReservaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private reservaVeiculoService: ReservaVeiculoService,
    private snackbarService: SnackbarService,
    public dialog: MatDialog,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService) {
    this.botoesDesabilitados = false;
    this.rejeicaoFormGroup = new FormGroup(
      {
        motivo: new FormControl('', Validators.required)
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get motivo(): AbstractControl {
    return this.rejeicaoFormGroup.controls.motivo;
  }

  reprovar() {
    if (this.rejeicaoFormGroup.valid) {
      this.botoesDesabilitados = true;
      if (this.data.tipo === 'Reprovar') {
        const dialogRef = this.dialog.open(CarregamentoComponent, {
          disableClose: true,
          data: 'Reprovando reserva...',
          width: '350px'
        });
        this.reservaVeiculoService.reprovarReserva(this.data.reserva.id, this.motivo.value)
          .pipe(
            catchError(error => {
              dialogRef.close();
              throw this.errorHandler.handle(error);
            })
          )
          .subscribe(reserva => {
            dialogRef.close();
            this.dialogRef.close(this.rejeicaoFormGroup);
            this.toastr.success('A reserva foi reprovada');
          });
      } else {
        const dialogRef = this.dialog.open(CarregamentoComponent, {
          disableClose: true,
          data: 'Cancelando reserva...',
          width: '350px'
        });
        this.reservaVeiculoService.cancelarReservaAdministrador(this.data.reserva.id, this.motivo.value)
          .pipe(
            catchError(error => {
              dialogRef.close();
              throw this.errorHandler.handle(error);
            })
          )
          .subscribe(reserva => {
            dialogRef.close();
            this.dialogRef.close(this.rejeicaoFormGroup);
            this.toastr.success('A reserva foi cancelada');
          });
      }
      this.botoesDesabilitados = false;
    }
  }

}
