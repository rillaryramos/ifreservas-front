import { CarregamentoComponent } from './carregamento/carregamento.component';
import { CompatibilidadeHorarioReservaAprovadaComponent } from './compatibilidade-horario-reserva-aprovada/compatibilidade-horario-reserva-aprovada.component';
import { CancelarRejeitarReservaComponent } from './cancelar-rejeitar-reserva/cancelar-rejeitar-reserva.component';
import { AuthNoticeComponent } from './auth-notice/auth-notice.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../demo-material-module';
import { MenuItems } from './menu-items/menu-items';
import { FiltroComponent } from './filtro/filtro.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { SelectionFilterComponent } from './selection-filter/selection-filter.component';
import { AdicionarHorarioComponent } from './adicionar-horario/adicionar-horario.component';

@NgModule({
  declarations: [
    FiltroComponent,
    SelectionFilterComponent,
    AdicionarHorarioComponent,
    AuthNoticeComponent,
    CancelarRejeitarReservaComponent,
    CompatibilidadeHorarioReservaAprovadaComponent,
    CarregamentoComponent
  ],
  imports: [
    CommonModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMatSelectSearchModule
  ],
  exports: [
    SelectionFilterComponent,
    AdicionarHorarioComponent,
    AuthNoticeComponent,
    CancelarRejeitarReservaComponent,
    CompatibilidadeHorarioReservaAprovadaComponent,
    CarregamentoComponent
  ],
  providers: [ MenuItems ],
  entryComponents: [
    FiltroComponent,
    CarregamentoComponent
  ],
})
export class SharedModule { }
