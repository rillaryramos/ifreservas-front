import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-carregamento',
  templateUrl: 'carregamento.component.html',
})
export class CarregamentoComponent {

  constructor(
    public dialogRef: MatDialogRef<CarregamentoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

}
