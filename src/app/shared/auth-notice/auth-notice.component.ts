import { AuthNoticeService } from './../../core/services/auth/auth-notice.service';
import { AuthNotice } from './../../core/services/auth/auth-notice.interface';
import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'm-auth-notice',
  templateUrl: './auth-notice.component.html',
  styleUrls: ['./auth-notice.component.scss']
})
export class AuthNoticeComponent implements OnInit {
  @Output() type: any;
  @Output() message: any = '';

  constructor(public authNoticeService: AuthNoticeService) { }

  ngOnInit() {
    this.authNoticeService.onNoticeChanged$.subscribe(
      (notice: AuthNotice) => {
        this.message = notice.message;
        this.type = notice.type;
      }
    );
  }
}
