import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  permissao: Array<string>;
}

const MENUITEMS = [
  { state: 'aprovacao', type: 'link', name: 'Aprovação', icon: 'departure_board', permissao: ['ROLE_APPROVER'] },
  { state: 'blocos', type: 'link', name: 'Blocos', icon: 'location_city', permissao: ['ROLE_MASTER'] },
  { state: 'dependencias', type: 'link', name: 'Dependências', icon: 'home', permissao: ['ROLE_MASTER'] },
  { state: 'motoristas', type: 'link', name: 'Motoristas', icon: 'airline_seat_recline_normal', permissao: ['ROLE_MASTER', 'ROLE_APPROVER'] },
  { state: 'turmas', type: 'link', name: 'Turmas', icon: 'school', permissao: ['ROLE_MASTER'] },
  { state: 'usuarios', type: 'link', name: 'Usuários', icon: 'people', permissao: ['ROLE_MASTER'] },
  { state: 'veiculos', type: 'link', name: 'Veículos', icon: 'directions_bus', permissao: ['ROLE_MASTER', 'ROLE_APPROVER'] },
  { state: 'relatorios', type: 'link', name: 'Relatórios', icon: 'file_copy', permissao: [] },
  { state: 'perfil', type: 'link', name: 'Meu perfil', icon: 'person', permissao: [] },
  { state: 'auditoria', type: 'link', name: 'Auditoria', icon: 'youtube_searched_for', permissao: ['ROLE_MASTER'] }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
