import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-selection-filter',
  templateUrl: './selection-filter.component.html',
  styleUrls: ['./selection-filter.component.scss']
})
export class SelectionFilterComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() lista: Array<any>;
  @Input() place: string;
  @Input() filtro: string;
  @Input() messageErro: string;
  @Input() estilo: string;
  @Input() idSelecionado: number;
  @Input() removerEspaco: boolean;
  @Input() placeholderLabel: string;
  @Input() noEntriesFoundLabel: string;
  // @Input() disabled: boolean;
  @Output() objetoSelecionado = new EventEmitter();
  public ctrl: FormControl;
  public filterCtrl: FormControl = new FormControl();
  public filteredArray: ReplaySubject<Objeto[]> = new ReplaySubject<Objeto[]>(1);
  protected _onDestroy = new Subject<void>();
  @ViewChild('singleSelect') singleSelect: MatSelect;

  constructor() { }

  ngOnInit() {
    this.ctrl = new FormControl(null);
    if (this.idSelecionado) {
      this.ctrl.setValue(this.lista[this.lista.indexOf(this.lista.filter(element => element.id === this.idSelecionado)[0])]);
    }
    this.filteredArray.next(this.lista.slice());

    this.filterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterLista();
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  limpar() {
    this.ctrl.setValue(null);
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  changeObjeto(arg) {
    this.objetoSelecionado.emit(arg.value);
  }

  protected setInitialValue() {
    this.filteredArray
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelect.compareWith = (a: Objeto, b: Objeto) => a && b && a.id === b.id;
      });
  }

  protected filterLista() {
    if (!this.lista) {
      return;
    }
    let search = this.filterCtrl.value;
    if (!search) {
      this.filteredArray.next(this.lista.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    if (this.filtro === 'dependencia') {
      this.filteredArray.next(
        // this.lista.filter(objeto => objeto.nome.toLowerCase().indexOf(search) > -1)
        this.lista.filter(objeto => (objeto.nome.toLowerCase().indexOf(search) > -1) || (objeto.bloco.nome.toLowerCase().indexOf(search) > -1))
      );
    } else if (this.filtro === 'veiculo') {
      this.filteredArray.next(
        this.lista.filter(objeto => (objeto.modelo.toLowerCase().indexOf(search) > -1) || (objeto.marca.toLowerCase().indexOf(search) > -1) || (objeto.placa.toLowerCase().indexOf(search) > -1))
      );
    } else if (this.filtro === 'servidor') {
      this.filteredArray.next(
        this.lista.filter(objeto => (objeto.login.toLowerCase().indexOf(search) > -1) || ((objeto.nome + ' ' + objeto.sobrenome).toLowerCase().indexOf(search) > -1))
      );
    } else if (this.filtro === 'turma') {
      this.filteredArray.next(
        this.lista.filter(objeto => objeto.nome.toLowerCase().indexOf(search) > -1)
      );
    } else if (this.filtro === 'motorista') {
      this.filteredArray.next(
        this.lista.filter(objeto => (objeto.nome + ' ' + objeto.sobrenome).toLowerCase().indexOf(search) > -1)
      );
    }
  }

}

export interface Objeto {
  id: string;
  modelo: string;
  nome: string;
}
