import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Filtro } from 'src/app/core/models/filtro.model';

@Component({
  selector: 'app-filtro',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.css']
})
export class FiltroComponent {

  filtroAtual: string;
  constructor(
    public dialogRef: MatDialogRef<FiltroComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Filtro) {
      this.filtroAtual = data.filtroSelecionado;
    }

  onNoClick(): void {
      this.dialogRef.close();
  }

}