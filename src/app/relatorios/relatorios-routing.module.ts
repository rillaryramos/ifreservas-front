import { RelatoriosComponent } from './relatorios.component';
import { RelatorioVeiculoComponent } from './relatorio-veiculo/relatorio-veiculo.component';
import { RelatorioDependenciaComponent } from './relatorio-dependencia/relatorio-dependencia.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: RelatoriosComponent
  },
  {
    path: 'reservas-dependencia',
    component: RelatorioDependenciaComponent
  },
  {
    path: 'reservas-veiculo',
    component: RelatorioVeiculoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatoriosRoutingModule { }
