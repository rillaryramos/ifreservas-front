import { RelatoriosComponent } from './relatorios.component';
import { RelatorioVeiculoComponent } from './relatorio-veiculo/relatorio-veiculo.component';
import { RelatorioDependenciaComponent } from './relatorio-dependencia/relatorio-dependencia.component';
import { SharedModule } from './../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoMaterialModule } from './../demo-material-module';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { RelatoriosRoutingModule } from './relatorios-routing.module';

@NgModule({
  declarations: [
    RelatoriosComponent,
    RelatorioDependenciaComponent,
    RelatorioVeiculoComponent
  ],
  imports: [
    CommonModule,
    RelatoriosRoutingModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    DatePipe
  ]

})
export class RelatoriosModule { }
