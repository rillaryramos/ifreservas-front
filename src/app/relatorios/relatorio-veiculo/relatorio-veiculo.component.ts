import { Title } from '@angular/platform-browser';
import { Motorista } from './../../core/models/motorista.model';
import { MotoristaService } from './../../core/services/motorista.service';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { RelatoriosService } from './../../core/services/relatorios.service';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { VeiculoService } from './../../core/services/veiculo.service';
import { Veiculo, Usuario } from './../../core/models/reserva-veiculo.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-relatorio-veiculo',
  templateUrl: './relatorio-veiculo.component.html',
  styleUrls: ['./relatorio-veiculo.component.css']
})
export class RelatorioVeiculoComponent implements OnInit {

  veiculos: Veiculo[];
  usuarios: Usuario[];
  motoristas: Motorista[];
  formGroupRelatorioPorVeiculo: FormGroup;
  formGroupRelatorioPorServidor: FormGroup;
  formGroupRelatorioPorMotorista: FormGroup;
  formGroupReservas: FormGroup;
  formGroupMinhasReservas: FormGroup;
  botaoReservaDesabilitados: boolean;
  @ViewChild('veiculo') selecVeiculo;
  @ViewChild('veiculoReservas') selecVeiculoReservas;
  @ViewChild('usuarioReservas') selecUsuarioReservas;
  @ViewChild('motoristaReservas') selecMotoristaReservas;

  constructor(
    private veiculoService: VeiculoService,
    private datePipe: DatePipe,
    private relatoriosService: RelatoriosService,
    private usuarioService: UsuarioService,
    private motoristaService: MotoristaService,
    private title: Title,
    private toastr: ToastrService
  ) {
    this.botaoReservaDesabilitados = false;
  }

  ngOnInit() {
    this.title.setTitle('Relatórios: reservas de veículo');
    this.configurarForm();
    this.listarVeiculos();
    this.listarUsuarios();
    this.listarMotoristas();
  }

  configurarForm() {
    this.formGroupRelatorioPorVeiculo = new FormGroup({
      inicio: new FormControl(null, Validators.required),
      fim: new FormControl(null, Validators.required),
      idVeiculo: new FormControl(null, Validators.required)
    });

    this.formGroupRelatorioPorServidor = new FormGroup({
      inicio: new FormControl(null, Validators.required),
      fim: new FormControl(null, Validators.required),
      idUsuario: new FormControl(null, Validators.required)
    });

    this.formGroupRelatorioPorMotorista = new FormGroup({
      inicio: new FormControl(null, Validators.required),
      fim: new FormControl(null, Validators.required),
      idMotorista: new FormControl(null, Validators.required)
    });

    this.formGroupReservas = new FormGroup({
      inicio: new FormControl(null, Validators.required),
      fim: new FormControl(null, Validators.required),
      idVeiculo: new FormControl(null),
      idUsuario: new FormControl(null),
      idMotorista: new FormControl(null)
    });

    this.formGroupMinhasReservas = new FormGroup({
      inicio: new FormControl(null, Validators.required),
      fim: new FormControl(null, Validators.required),
      idVeiculo: new FormControl(null),
      idStatus: new FormControl('null')
    });
  }

  get inicioRelatorioPorVeiculo(): AbstractControl {
    return this.formGroupRelatorioPorVeiculo.controls.inicio;
  }

  get inicioRelatorioPorServidor(): AbstractControl {
    return this.formGroupRelatorioPorServidor.controls.inicio;
  }

  get inicioRelatorioPorMotorista(): AbstractControl {
    return this.formGroupRelatorioPorMotorista.controls.inicio;
  }

  get inicioReservas(): AbstractControl {
    return this.formGroupReservas.controls.inicio;
  }

  get inicio(): AbstractControl {
    return this.formGroupMinhasReservas.controls.inicio;
  }

  removerVeiculoReservas() {
    this.formGroupReservas.get('idVeiculo').setValue(null);
    this.selecVeiculoReservas.limpar();
  }

  removerMotoristaReservas() {
    this.formGroupReservas.get('idMotorista').setValue(null);
    this.selecMotoristaReservas.limpar();
  }

  removerUsuarioReservas() {
    this.formGroupReservas.get('idUsuario').setValue(null);
    this.selecUsuarioReservas.limpar();
  }

  removerVeiculo() {
    this.formGroupMinhasReservas.get('idVeiculo').setValue(null);
    this.selecVeiculo.limpar();
  }

  listarVeiculos() {
    this.veiculoService.getVeiculos()
      .subscribe((veiculos: Veiculo[]) => {
        this.veiculos = veiculos;
      });
  }

  listarUsuarios() {
    this.usuarioService.getTodosUsuarios()
      .subscribe((usuarios: Usuario[]) => {
        this.usuarios = usuarios;
      });
  }

  listarMotoristas() {
    this.motoristaService.getMotoristas()
      .subscribe((motoristas: Motorista[]) => {
        this.motoristas = motoristas;
      });
  }

  aoSelecionarVeiculoRelatorioPorVeiculo(arg) {
    this.formGroupRelatorioPorVeiculo.get('idVeiculo').setValue(arg.id);
  }

  aoSelecionarVeiculo(arg) {
    this.formGroupMinhasReservas.get('idVeiculo').setValue(arg.id);
  }

  aoSelecionarVeiculoReservas(arg) {
    this.formGroupReservas.get('idVeiculo').setValue(arg.id);
  }

  aoSelecionarServidorRelatorioPorServidor(arg) {
    this.formGroupRelatorioPorServidor.get('idUsuario').setValue(arg.id);
  }

  aoSelecionarServidorReservas(arg) {
    this.formGroupReservas.get('idUsuario').setValue(arg.id);
  }

  aoSelecionarMotoristaRelatorioPorMotorista(arg) {
    this.formGroupRelatorioPorMotorista.get('idMotorista').setValue(arg.id);
  }

  aoSelecionarMotoristaReservas(arg) {
    this.formGroupReservas.get('idMotorista').setValue(arg.id);
  }

  gerarRelatorioMinhasReservas() {
    if (this.formGroupMinhasReservas.valid) {
      this.relatoriosService.minhasReservasVeiculo(
        this.datePipe.transform(this.formGroupMinhasReservas.get('inicio').value, 'yyyy-MM-dd'),
        this.datePipe.transform(this.formGroupMinhasReservas.get('fim').value, 'yyyy-MM-dd'),
        this.formGroupMinhasReservas.get('idVeiculo').value,
        this.formGroupMinhasReservas.get('idStatus').value
      )
        .then(relatorio => {
          const url = window.URL.createObjectURL(relatorio);
          window.open(url);
        })
        .catch(erro => {
          this.toastr.error(erro);
        });
    }
  }

  gerarRelatorioReservasAprovadas() {
    if (this.formGroupReservas.valid) {
      this.botaoReservaDesabilitados = true;
      this.relatoriosService.relatorioReservasVeiculoAprovadas(
        this.datePipe.transform(this.formGroupReservas.get('inicio').value, 'yyyy-MM-dd'),
        this.datePipe.transform(this.formGroupReservas.get('fim').value, 'yyyy-MM-dd'),
        this.formGroupReservas.get('idVeiculo').value,
        this.formGroupReservas.get('idUsuario').value,
        this.formGroupReservas.get('idMotorista').value
      )
        .then(relatorio => {
          const url = window.URL.createObjectURL(relatorio);
          window.open(url);
        })
        .catch(erro => {
          this.toastr.error(erro);
        });
      this.botaoReservaDesabilitados = false;
    }
  }

  gerarRelatorioPorVeiculo() {
    if (this.formGroupRelatorioPorVeiculo.valid) {
      this.relatoriosService.relatorioReservasVeiculoAprovadas(
        this.datePipe.transform(this.formGroupRelatorioPorVeiculo.get('inicio').value, 'yyyy-MM-dd'),
        this.datePipe.transform(this.formGroupRelatorioPorVeiculo.get('fim').value, 'yyyy-MM-dd'),
        this.formGroupRelatorioPorVeiculo.get('idVeiculo').value, null, null
      )
        .then(relatorio => {
          const url = window.URL.createObjectURL(relatorio);
          window.open(url);
        })
        .catch(erro => {
          this.toastr.error(erro);
        });
    }
  }

  gerarRelatorioPorServidor() {
    if (this.formGroupRelatorioPorServidor.valid) {
      this.relatoriosService.relatorioReservasVeiculoAprovadas(
        this.datePipe.transform(this.formGroupRelatorioPorServidor.get('inicio').value, 'yyyy-MM-dd'),
        this.datePipe.transform(this.formGroupRelatorioPorServidor.get('fim').value, 'yyyy-MM-dd'), null,
        this.formGroupRelatorioPorServidor.get('idUsuario').value, null
      )
        .then(relatorio => {
          const url = window.URL.createObjectURL(relatorio);
          window.open(url);
        })
        .catch(erro => {
          this.toastr.error(erro);
        });
    }
  }

  gerarRelatorioPorMotorista() {
    if (this.formGroupRelatorioPorMotorista.valid) {
      this.relatoriosService.relatorioReservasVeiculoAprovadas(
        this.datePipe.transform(this.formGroupRelatorioPorMotorista.get('inicio').value, 'yyyy-MM-dd'),
        this.datePipe.transform(this.formGroupRelatorioPorMotorista.get('fim').value, 'yyyy-MM-dd'), null,
        null, this.formGroupRelatorioPorMotorista.get('idMotorista').value
      )
        .then(relatorio => {
          const url = window.URL.createObjectURL(relatorio);
          window.open(url);
        })
        .catch(erro => {
          this.toastr.error(erro);
        });
    }
  }

}
