import { Title } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { TurmaService } from './../../core/services/turma.service';
import { UsuarioService } from './../../core/services/usuario.service';
import { Turma } from './../../core/models/turma.model';
import { Usuario } from './../../core/models/usuario.model';
import { DependenciaService } from './../../core/services/dependencia.service';
import { Dependencia } from './../../core/models/dependencia.model';
import { RelatoriosService } from './../../core/services/relatorios.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-relatorio-dependencia',
  templateUrl: './relatorio-dependencia.component.html',
  styleUrls: ['./relatorio-dependencia.component.css']
})
export class RelatorioDependenciaComponent implements OnInit {

  dependencias: Dependencia[];
  usuarios: Usuario[];
  turmas: Turma[];
  dependenciaSelecionada: Dependencia;
  usuarioSelecionado: Usuario;
  turmaSelecionada: Turma;
  formGroupRelatorioPorDependencia: FormGroup;
  formGroupRelatorioPorServidor: FormGroup;
  formGroupRelatorioPorTurma: FormGroup;
  formGroupRelatorioPorTipo: FormGroup;
  formGroupRelatorio: FormGroup;
  aulaSim = true;
  aulaNao = true;
  botoesDesabilitados: boolean;
  @ViewChild('dependencia') selecDependencia;

  constructor(
    private relatoriosService: RelatoriosService,
    private dependenciaService: DependenciaService,
    private usuarioService: UsuarioService,
    private turmaService: TurmaService,
    private datePipe: DatePipe,
    private title: Title,
    private toastr: ToastrService
  ) {
    this.botoesDesabilitados = false;
  }

  ngOnInit() {
    this.title.setTitle('Relatórios: reservas de dependência');
    this.configurarForm();
    this.listarDependencias();
    this.listarUsuarios();
    this.listarTurmas();
  }

  configurarForm() {
    this.formGroupRelatorioPorDependencia = new FormGroup({
      inicio: new FormControl(null, Validators.required),
      fim: new FormControl(null, Validators.required),
      idDependencia: new FormControl(null, Validators.required)
    });
    this.formGroupRelatorioPorServidor = new FormGroup({
      inicio: new FormControl(null, Validators.required),
      fim: new FormControl(null, Validators.required),
      idServidor: new FormControl(null, Validators.required)
    });
    this.formGroupRelatorioPorTurma = new FormGroup({
      inicio: new FormControl(null, Validators.required),
      fim: new FormControl(null, Validators.required),
      idTurma: new FormControl(null, Validators.required)
    });
    this.formGroupRelatorioPorTipo = new FormGroup({
      inicio: new FormControl(null, Validators.required),
      fim: new FormControl(null, Validators.required),
      tipo: new FormControl(null, Validators.required)
    });
    this.formGroupRelatorio = new FormGroup({
      inicio: new FormControl(null, Validators.required),
      fim: new FormControl(null, Validators.required),
      idDependencia: new FormControl(null),
      utilizador: new FormControl('todos'),
      tipo: new FormControl('null'),
      idServidor: new FormControl(null),
      idTurma: new FormControl(null)
    });
  }

  listarDependencias() {
    this.dependenciaService.getDependencias()
      .subscribe((dependencias: Dependencia[]) => {
        this.dependencias = dependencias;
      });
  }

  listarUsuarios() {
    this.usuarioService.getTodosUsuarios()
      .subscribe((usuarios: Usuario[]) => {
        this.usuarios = usuarios;
      });
  }

  listarTurmas() {
    this.turmaService.getTurmas()
      .subscribe((turmas: Turma[]) => {
        this.turmas = turmas;
      });
  }

  get inicioRelatorioPorDependencia(): AbstractControl {
    return this.formGroupRelatorioPorDependencia.controls.inicio;
  }

  get inicioRelatorioPorServidor(): AbstractControl {
    return this.formGroupRelatorioPorServidor.controls.inicio;
  }

  get inicioRelatorioPorTurma(): AbstractControl {
    return this.formGroupRelatorioPorTurma.controls.inicio;
  }

  get inicioRelatorioPorTipo(): AbstractControl {
    return this.formGroupRelatorioPorTipo.controls.inicio;
  }

  get inicio(): AbstractControl {
    return this.formGroupRelatorio.controls.inicio;
  }

  get idDependencia(): AbstractControl {
    return this.formGroupRelatorio.controls.idDependencia;
  }

  get utilizador(): AbstractControl {
    return this.formGroupRelatorio.controls.utilizador;
  }

  aoSelecionarDependencia(arg) {
    this.formGroupRelatorio.get('idDependencia').setValue(arg.id);
  }

  aoSelecionarDependenciaRelatorioPorDependencia(arg) {
    this.formGroupRelatorioPorDependencia.get('idDependencia').setValue(arg.id);
  }

  removerDependencia() {
    this.formGroupRelatorio.get('idDependencia').setValue(null);
    this.selecDependencia.limpar();
  }

  aoSelecionarServidorRelatorioPorServidor(arg) {
    this.formGroupRelatorioPorServidor.get('idServidor').setValue(arg.id);
  }

  aoSelecionarServidor(arg) {
    this.formGroupRelatorio.get('idServidor').setValue(arg.id);
  }

  aoSelecionarTurma(arg) {
    this.formGroupRelatorio.get('idTurma').setValue(arg.id);
  }

  aoSelecionarTurmaRelatorioPorTurma(arg) {
    this.formGroupRelatorioPorTurma.get('idTurma').setValue(arg.id);
  }

  changeUtilizador() {
    if (this.utilizador.value === 'servidor') {
      this.formGroupRelatorio.setControl('idServidor', new FormControl(null, Validators.required));
      this.formGroupRelatorio.setControl('idTurma', new FormControl(null));
    } else if (this.utilizador.value === 'turma') {
      this.formGroupRelatorio.setControl('idTurma', new FormControl(null, Validators.required));
      this.formGroupRelatorio.setControl('idServidor', new FormControl(null));
    } else {
      this.formGroupRelatorio.setControl('idTurma', new FormControl(null));
      this.formGroupRelatorio.setControl('idServidor', new FormControl(null));
    }
  }

  gerar() {
    if (this.formGroupRelatorio.valid) {
      this.botoesDesabilitados = true;
      this.relatoriosService.relatorioDependencia(
        this.datePipe.transform(this.formGroupRelatorio.get('inicio').value, 'yyyy-MM-dd'),
        this.datePipe.transform(this.formGroupRelatorio.get('fim').value, 'yyyy-MM-dd'),
        this.formGroupRelatorio.get('idDependencia').value,
        this.formGroupRelatorio.get('idServidor').value,
        this.formGroupRelatorio.get('idTurma').value,
        this.formGroupRelatorio.get('tipo').value
      )
        .then(relatorio => {
          const url = window.URL.createObjectURL(relatorio);
          window.open(url);
        });
      this.botoesDesabilitados = false;
    }
  }

  gerarRelatorioPorDependencia() {
    if (this.formGroupRelatorioPorDependencia.valid) {
      this.relatoriosService.relatorioDependencia(
        this.datePipe.transform(this.formGroupRelatorioPorDependencia.get('inicio').value, 'yyyy-MM-dd'),
        this.datePipe.transform(this.formGroupRelatorioPorDependencia.get('fim').value, 'yyyy-MM-dd'),
        this.formGroupRelatorioPorDependencia.get('idDependencia').value, null, null, null
      )
        .then(relatorio => {
          const url = window.URL.createObjectURL(relatorio);
          window.open(url);
        })
        .catch(erro => {
          this.toastr.error(erro);
        });
    }
  }

  gerarRelatorioPorServidor() {
    if (this.formGroupRelatorioPorServidor.valid) {
      this.relatoriosService.relatorioDependencia(
        this.datePipe.transform(this.formGroupRelatorioPorServidor.get('inicio').value, 'yyyy-MM-dd'),
        this.datePipe.transform(this.formGroupRelatorioPorServidor.get('fim').value, 'yyyy-MM-dd'), null,
        this.formGroupRelatorioPorServidor.get('idServidor').value, null, null
      )
        .then(relatorio => {
          const url = window.URL.createObjectURL(relatorio);
          window.open(url);
        })
        .catch(erro => {
          this.toastr.error(erro);
        });
    }
  }

  gerarRelatorioPorTurma() {
    if (this.formGroupRelatorioPorTurma.valid) {
      this.relatoriosService.relatorioDependencia(
        this.datePipe.transform(this.formGroupRelatorioPorTurma.get('inicio').value, 'yyyy-MM-dd'),
        this.datePipe.transform(this.formGroupRelatorioPorTurma.get('fim').value, 'yyyy-MM-dd'), null,
        null, this.formGroupRelatorioPorTurma.get('idTurma').value, null
      )
        .then(relatorio => {
          const url = window.URL.createObjectURL(relatorio);
          window.open(url);
        })
        .catch(erro => {
          this.toastr.error(erro);
        });
    }
  }

  gerarRelatorioPorTipo() {
    if (this.formGroupRelatorioPorTipo.valid) {
      this.relatoriosService.relatorioDependencia(
        this.datePipe.transform(this.formGroupRelatorioPorTipo.get('inicio').value, 'yyyy-MM-dd'),
        this.datePipe.transform(this.formGroupRelatorioPorTipo.get('fim').value, 'yyyy-MM-dd'), null,
        null, null, this.formGroupRelatorioPorTipo.get('tipo').value
      )
        .then(relatorio => {
          const url = window.URL.createObjectURL(relatorio);
          window.open(url);
        })
        .catch(erro => {
          this.toastr.error(erro);
        });
    }
  }
}
