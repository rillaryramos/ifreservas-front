import { VeiculoService } from './../../core/services/veiculo.service';
import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cadastro-veiculo',
  templateUrl: './cadastro-veiculo.component.html',
  styleUrls: ['./cadastro-veiculo.component.scss']
})
export class CadastroVeiculoComponent implements OnInit {
  formGroupVeiculo: FormGroup;
  // currentMask: string = '000.000.000-00';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private veiculoService: VeiculoService,
    private title: Title,
    private toastr: ToastrService
  ) {

  }

  ngOnInit() {
    this.title.setTitle('Cadastro de veículo');
    this.configurarForm();
    const idVeiculo = this.activatedRoute.snapshot.params['id'];
    if (idVeiculo) {
      this.buscarVeiculo(idVeiculo);
    }
  }

  configurarForm() {
    this.formGroupVeiculo = new FormGroup({
      id: new FormControl(null),
      modelo: new FormControl(null, Validators.required),
      marca: new FormControl(null, Validators.required),
      placa: new FormControl(null, [Validators.required]),
      observacao: new FormControl(null),
      ativo: new FormControl(null)
    });
  }

  get id(): AbstractControl {
    return this.formGroupVeiculo.controls.id;
  }

  get placa(): AbstractControl {
    return this.formGroupVeiculo.controls.placa;
  }

  get editando() {
    return Boolean(this.id.value);
  }

  buscarVeiculo(id: number) {
    this.veiculoService.getVeiculoById(id)
      .subscribe(veiculo => {
        this.formGroupVeiculo.setValue(veiculo);
        this.atualizarTituloEdicao();
      });
  }

  atualizarTituloEdicao() {
    this.title.setTitle(`Edição de veículo: ${this.formGroupVeiculo.get('modelo').value}`);
  }

  salvar() {
    if (this.editando) {
      this.atualizarVeiculo();
    } else {
      this.adicionarVeiculo();
    }
  }

  adicionarVeiculo() {
    if (this.formGroupVeiculo.valid) {
      return this.veiculoService.cadastrarVeiculo({
        id: null,
        modelo: this.formGroupVeiculo.get('modelo').value,
        marca: this.formGroupVeiculo.get('marca').value,
        placa: this.placa.value.substring(0, 3) + '-' + this.placa.value.substring(3, 7),
        observacao: this.formGroupVeiculo.get('observacao').value,
        ativo: this.formGroupVeiculo.get('ativo').value
      })
        .subscribe(motorista => {
          this.router.navigate(['/veiculos']);
          this.toastr.success('O veículo foi cadastrado');
        });
    }
  }

  atualizarVeiculo() {
    if (this.formGroupVeiculo.valid) {
      this.veiculoService.atualizarVeiculo({
        id: this.formGroupVeiculo.get('id').value,
        modelo: this.formGroupVeiculo.get('modelo').value,
        marca: this.formGroupVeiculo.get('marca').value,
        placa: this.placa.value.substring(0, 3) + '-' + this.placa.value.substring(3, 7),
        observacao: this.formGroupVeiculo.get('observacao').value,
        ativo: this.formGroupVeiculo.get('ativo').value
      })
        .subscribe(motorista => {
          this.router.navigate(['/veiculos']);
          this.toastr.success('O veículo foi atualizado');
        });
    }
  }
}
