import { AuthGuard } from './../seguranca/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VeiculosComponent } from './veiculos.component';
import { CadastroVeiculoComponent } from './cadastro-veiculo/cadastro-veiculo.component';

const routes: Routes = [
  {
    path: '',
    component: VeiculosComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER', 'ROLE_APPROVER'] }
  },
  {
    path: 'novo',
    component: CadastroVeiculoComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER', 'ROLE_APPROVER'] }
  },
  {
    path: ':id',
    component: CadastroVeiculoComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER', 'ROLE_APPROVER'] }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VeiculosRoutingModule { }
