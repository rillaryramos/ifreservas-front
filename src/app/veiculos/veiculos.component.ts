import { Title } from '@angular/platform-browser';
import { FiltroComponent } from './../shared/filtro/filtro.component';
import { Filtro } from './../core/models/filtro.model';
import { StatusComponent } from './status-veiculo/status-veiculo.component';
import { RemoverVeiculoComponent } from './remover-veiculo/remover-veiculo.component';
import { VeiculoService } from './../core/services/veiculo.service';
import { Veiculo } from './../core/models/veiculo.model';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-veiculos',
  templateUrl: './veiculos.component.html',
  styleUrls: ['./veiculos.component.scss']
})
export class VeiculosComponent implements OnInit, AfterViewInit {

  selected = 'all';
  filtro: Filtro;
  displayedColumns: string[] = ['id', 'modelo', 'marca', 'placa', 'ativo', 'actions'];
  veiculos = new MatTableDataSource<Veiculo>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('busca') busca: ElementRef;

  constructor(
    private veiculoService: VeiculoService,
    public dialog: MatDialog,
    private title: Title,
    private toastr: ToastrService
  ) {
    this.filtro = { filtros: ['Tipo', 'Marca', 'Placa'], filtroSelecionado: 'Tipo' };
  }

  ngOnInit() {
    this.title.setTitle('Veículos');
    this.listarVeiculos();
  }

  ngAfterViewInit() {
    this.veiculos.paginator = this.paginator;
  }

  listarVeiculos() {
    this.veiculoService.getVeiculos()
      .subscribe((veiculos: Veiculo[]) => {
        this.veiculos.data = veiculos;
      });
  }

  changeStatus(veiculo: Veiculo) {
    const novoStatus = !veiculo.ativo;
    this.veiculoService.alterarStatus(veiculo.id, novoStatus).subscribe(() => {
      const acao = novoStatus ? 'ativado' : 'inativado';
      veiculo.ativo = novoStatus;
      this.toastr.success(`Veículo ${acao}(o) com sucesso`);
    });

  }

  openFiltroDialog() {
    const dialogRef = this.dialog.open(FiltroComponent, {
      data: this.filtro,
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.filtro.filtroSelecionado = result ? result : this.filtro.filtroSelecionado;
      this.filtroStatus();
    });
  }

  filtroStatus() {
    const busca = this.busca.nativeElement.value.toLowerCase();
    this.veiculos.filterPredicate = (veiculo: Veiculo, filter: string) => {
      return ((
        veiculo.ativo ? 'true' : 'false'
      )
        .includes(this.selected) || this.selected === 'all') && ((
          this.filtro.filtroSelecionado === 'Tipo' ? veiculo.modelo :
            this.filtro.filtroSelecionado === 'Marca' ? veiculo.marca : veiculo.placa
        )
          .toLowerCase()
          .includes(busca));
    };
    this.veiculos.filter = this.selected + busca;
    if (this.veiculos.paginator) {
      this.veiculos.paginator.firstPage();
    }
  }

  openAtivacaoDialog(veiculo: Veiculo): void {
    const dialogRef = this.dialog.open(StatusComponent, {
      data: veiculo,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.changeStatus(result);
      }
    });
  }

  openExclusaoDialog(veiculo: Veiculo): void {
    const dialogRef = this.dialog.open(RemoverVeiculoComponent, {
      data: veiculo,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(result);
      }
    });
  }

  excluir(veiculo: Veiculo) {
    this.veiculoService.excluir(veiculo.id).subscribe(() => {
      this.listarVeiculos();
      this.toastr.success(`Veículo excluído com sucesso`);
    });
  }
}
