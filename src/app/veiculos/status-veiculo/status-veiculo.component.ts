import { Veiculo } from './../../core/models/veiculo.model';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-status-veiculo',
  templateUrl: 'status-veiculo.component.html',
})
export class StatusComponent {

  constructor(
    public dialogRef: MatDialogRef<StatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Veiculo) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
