import { StatusComponent } from './status-veiculo/status-veiculo.component';
import { RemoverVeiculoComponent } from './remover-veiculo/remover-veiculo.component';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { DemoMaterialModule } from '../demo-material-module';
import { VeiculosRoutingModule } from './veiculos-routing.module';

import { VeiculosComponent } from './veiculos.component';
import { CadastroVeiculoComponent } from './cadastro-veiculo/cadastro-veiculo.component';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    VeiculosRoutingModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    NgxMaskModule
  ],
  providers: [],
  entryComponents: [
    RemoverVeiculoComponent,
    StatusComponent
  ],
  declarations: [
    VeiculosComponent,
    CadastroVeiculoComponent,
    RemoverVeiculoComponent,
    StatusComponent
  ]
})
export class VeiculosModule {}
