import { Veiculo } from './../../core/models/reserva-veiculo.model';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-remover-veiculo',
  templateUrl: 'remover-veiculo.component.html',
})
export class RemoverVeiculoComponent {

  constructor(
    public dialogRef: MatDialogRef<RemoverVeiculoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Veiculo) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
