import { CadastroBlocoComponent } from './cadastro-bloco/cadastro-bloco.component';
import { BlocosComponent } from './blocos.component';
import { AuthGuard } from './../seguranca/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: BlocosComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  },
  {
    path: 'novo',
    component: CadastroBlocoComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  },
  {
    path: ':id',
    component: CadastroBlocoComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlocosRoutingModule { }
