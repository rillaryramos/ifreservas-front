import { Bloco } from './../../core/models/bloco.model';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-remover-bloco',
  templateUrl: 'remover-bloco.component.html',
})
export class RemoverBlocoComponent {

  constructor(
    public dialogRef: MatDialogRef<RemoverBlocoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Bloco) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
