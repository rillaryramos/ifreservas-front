import { CadastroBlocoComponent } from './cadastro-bloco/cadastro-bloco.component';
import { RemoverBlocoComponent } from './remover-bloco/remover-bloco.component';
import { BlocosRoutingModule } from './blocos-routing.module';
import { BlocosComponent } from './blocos.component';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DemoMaterialModule } from '../demo-material-module';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    BlocosRoutingModule
  ],
  providers: [],
  entryComponents: [
    RemoverBlocoComponent
  ],
  declarations: [
    BlocosComponent,
    RemoverBlocoComponent,
    CadastroBlocoComponent
  ]
})
export class BlocosModule {}
