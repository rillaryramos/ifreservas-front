import { BlocoService } from './../../core/services/bloco.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cadastro-bloco',
  templateUrl: './cadastro-bloco.component.html',
  styleUrls: ['./cadastro-bloco.component.scss']
})
export class CadastroBlocoComponent implements OnInit {
  formGroupBloco: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private blocoService: BlocoService,
    private title: Title,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.title.setTitle('Cadastro de bloco');
    this.configurarForm();
    const idBloco = this.activatedRoute.snapshot.params['id'];
    if (idBloco) {
      this.buscarBloco(idBloco);
    }
  }

  configurarForm() {
    this.formGroupBloco = new FormGroup({
      id: new FormControl(null),
      nome: new FormControl(null, Validators.required)
    });
  }

  get id(): AbstractControl {
    return this.formGroupBloco.controls.id;
  }

  get editando() {
    return Boolean(this.id.value);
  }

  buscarBloco(id: number) {
    this.blocoService.getBlocoById(id)
      .subscribe(bloco => {
        this.formGroupBloco.setValue(bloco);
        this.atualizarTituloEdicao();
      });
  }

  atualizarTituloEdicao() {
    this.title.setTitle(`Edição de bloco: ${this.formGroupBloco.get('nome').value}`);
  }

  salvar() {
    if (this.editando) {
      this.atualizarBloco();
    } else {
      this.adicionarBloco();
    }
  }

  adicionarBloco() {
    if (this.formGroupBloco.valid) {
      return this.blocoService.cadastrarBloco(this.formGroupBloco.value)
      .subscribe(bloco => {
        this.router.navigate(['/blocos']);
        this.toastr.success('O bloco foi cadastrado');
      });
    }
  }

  atualizarBloco() {
    if (this.formGroupBloco.valid) {
      this.blocoService.atualizarBloco(this.formGroupBloco.value)
      .subscribe(bloco => {
        this.router.navigate(['/blocos']);
        this.toastr.success('O bloco foi atualizado');
      });
    }
  }
}
