import { Title } from '@angular/platform-browser';
import { RemoverBlocoComponent } from './remover-bloco/remover-bloco.component';
import { BlocoService } from './../core/services/bloco.service';
import { Bloco } from './../core/models/bloco.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';

import { SnackbarService } from '../core/services/snackbar.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-blocos',
  templateUrl: './blocos.component.html',
  styleUrls: ['./blocos.component.scss']
})
export class BlocosComponent implements OnInit {

  displayedColumns: string[] = ['codigo', 'nome', 'actions'];
  blocos = new MatTableDataSource<Bloco>();

  constructor(
    private blocoService: BlocoService,
    public dialog: MatDialog,
    private title: Title,
    private toastr: ToastrService
  ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.title.setTitle('Blocos');
    this.listarBlocos();
  }

  ngAfterViewInit() {
    this.blocos.paginator = this.paginator;
  }

  listarBlocos() {
    this.blocoService.getBlocos()
      .subscribe((blocos: Bloco[]) => {
        this.blocos.data = blocos;
      });
  }

  openExclusaoDialog(bloco: Bloco): void {
    const dialogRef = this.dialog.open(RemoverBlocoComponent, {
      data: bloco,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(result);
      }
    });
  }

  public doFilter = (value: string) => {
    this.blocos.filterPredicate = (bloco: Bloco, filter: string) => {
      return (
        bloco.nome
      )
        .toLowerCase()
        .includes(filter);
    };
    this.blocos.filter = value.trim().toLocaleLowerCase();
    if (this.blocos.paginator) {
      this.blocos.paginator.firstPage();
    }
  }

  excluir(bloco: Bloco) {
    this.blocoService.excluir(bloco.id).subscribe(() => {
      this.listarBlocos();
      this.toastr.success('Bloco excluído com sucesso');
    });
  }

}
