import { LogoutService } from './../../../seguranca/logout.service';
import { Router } from '@angular/router';
import { ErrorHandlerService } from './../../../core/services/error-handler.service';
import { Component } from '@angular/core';
import { AuthService } from 'src/app/seguranca/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class AppHeaderComponent {

  constructor(
    private auth: AuthService,
    private logoutService: LogoutService,
    private errorHandler: ErrorHandlerService,
    private router: Router
  ) { }

  logout() {
    this.logoutService.logout()
      .then(() => {
        this.router.navigate(['/login']);
      })
      .catch(erro => {
        console.log(erro);
        this.errorHandler.handle(erro);
      });
  }

  get nomeUsuario() {
    return this.auth.jwtPayLoad ? this.auth.jwtPayLoad.nome : null;
  }
}
