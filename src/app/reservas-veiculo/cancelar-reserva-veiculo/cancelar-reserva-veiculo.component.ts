import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-cancelar-reserva-veiculo',
    templateUrl: 'cancelar-reserva-veiculo.component.html',
})
export class CancelarReservaVeiculoComponent {

    constructor(
        public dialogRef: MatDialogRef<CancelarReservaVeiculoComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {}

    onNoClick(): void {
        this.dialogRef.close();
    }

}
