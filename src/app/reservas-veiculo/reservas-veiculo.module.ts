import { CarregamentoComponent } from './../shared/carregamento/carregamento.component';
import { AlertaReservaAbertaComponent } from './alerta-reserva-aberta/alerta-reserva-aberta.component';
import { CancelarRejeitarReservaComponent } from './../shared/cancelar-rejeitar-reserva/cancelar-rejeitar-reserva.component';
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe, registerLocaleData } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { ReservasVeiculoComponent } from './reservas-veiculo.component';
import { ReservasVeiculoRoutes } from './reservas-veiculo.routing';
import { FullCalendarModule } from '@fullcalendar/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CadastroReservaVeiculoComponent } from './cadastro-reserva-veiculo/cadastro-reserva-veiculo.component';
import { VisualizarReservaVeiculoComponent } from './visualizar-reserva-veiculo/visualizar-reserva-veiculo.component';
import { MAT_DATE_LOCALE } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import ptBr from '@angular/common/locales/pt';
import { CancelarReservaVeiculoComponent } from './cancelar-reserva-veiculo/cancelar-reserva-veiculo.component';
import { EnviarAprovacaoReservaVeiculoComponent } from './enviar-aprovacao-reserva-veiculo/enviar-aprovacao-reserva-veiculo.component';
import { CompatibilidadeHorarioReservaAprovadaComponent } from '../shared/compatibilidade-horario-reserva-aprovada/compatibilidade-horario-reserva-aprovada.component';
registerLocaleData(ptBr);

@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(ReservasVeiculoRoutes),
    FullCalendarModule,
    SharedModule,
  ],
  declarations: [
    ReservasVeiculoComponent,
    CadastroReservaVeiculoComponent,
    VisualizarReservaVeiculoComponent,
    CancelarReservaVeiculoComponent,
    EnviarAprovacaoReservaVeiculoComponent,
    AlertaReservaAbertaComponent
    // CompatibilidadeHorarioComponent
  ],
  entryComponents: [
    CadastroReservaVeiculoComponent,
    VisualizarReservaVeiculoComponent,
    CancelarReservaVeiculoComponent,
    EnviarAprovacaoReservaVeiculoComponent,
    CancelarRejeitarReservaComponent,
    // CompatibilidadeHorarioComponent,
    CompatibilidadeHorarioReservaAprovadaComponent,
    AlertaReservaAbertaComponent,
    CarregamentoComponent
  ],
  providers: [
    DatePipe,
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ]
})
export class ReservasVeiculoModule {}
