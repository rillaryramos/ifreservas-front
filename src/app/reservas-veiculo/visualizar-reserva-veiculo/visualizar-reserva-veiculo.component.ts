import { ErrorHandlerService } from './../../core/services/error-handler.service';
import { CarregamentoComponent } from './../../shared/carregamento/carregamento.component';
import { CancelarRejeitarReservaComponent } from './../../shared/cancelar-rejeitar-reserva/cancelar-rejeitar-reserva.component';
import { AuthService } from 'src/app/seguranca/auth.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ReservaVeiculo } from 'src/app/core/models/reserva-veiculo.model';
import { ReservaVeiculoService } from 'src/app/core/services/reserva-veiculo.service';
import { DatePipe } from '@angular/common';
import { CancelarReservaVeiculoComponent } from '../cancelar-reserva-veiculo/cancelar-reserva-veiculo.component';
import { CadastroReservaVeiculoComponent } from '../cadastro-reserva-veiculo/cadastro-reserva-veiculo.component';
import { EnviarAprovacaoReservaVeiculoComponent } from '../enviar-aprovacao-reserva-veiculo/enviar-aprovacao-reserva-veiculo.component';
import { ToastrService } from 'ngx-toastr';
import { catchError } from 'rxjs/operators';
import { CompatibilidadeHorarioReservaAprovadaComponent } from 'src/app/shared/compatibilidade-horario-reserva-aprovada/compatibilidade-horario-reserva-aprovada.component';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-visualizar-reserva-veiculo',
  templateUrl: './visualizar-reserva-veiculo.component.html',
  styleUrls: ['./visualizar-reserva-veiculo.component.scss']
})
export class VisualizarReservaVeiculoComponent implements OnInit {

  reservaSelecionada: ReservaVeiculo;

  constructor(
    public dialogRef: MatDialogRef<VisualizarReservaVeiculoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: number,
    private reservaVeiculoService: ReservaVeiculoService,
    private datePipe: DatePipe,
    public dialog: MatDialog,
    private auth: AuthService,
    private toastr: ToastrService,
    private errorHandler: ErrorHandlerService
  ) {
  }

  ngOnInit() {
    this.buscarReserva(this.data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  buscarReserva(id: number) {
    this.reservaVeiculoService.getReservaById(id)
      .subscribe(reserva => {
        this.reservaSelecionada = reserva;
        this.maiorQueDataAtual();
        this.isUsuarioLogado();
      });
  }

  maiorQueDataAtual() {
    if (new Date(this.reservaSelecionada.start) < new Date()) {
      return false;
    } else {
      return true;
    }
  }

  openAlterarDialog(reserva: ReservaVeiculo): void {
    const dialogRef = this.dialog.open(CadastroReservaVeiculoComponent, {
      width: '420px',
      data: {
        id: reserva.id,
        title: reserva.title,
        start: new Date(reserva.start),
        end: new Date(reserva.end),
        horaInicial: new Date(reserva.start).toTimeString().substring(0, 5),
        horaFinal: new Date(reserva.end).toTimeString().substring(0, 5),
        saida: reserva.saida,
        destino: reserva.destino,
        motivo: reserva.motivo,
        veiculo: {
          id: reserva.veiculo.id
        }
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.listarReservasPorVeiculo(this.veiculoSelecionado.id);
        this.dialogRef.close(true);
      }
    });
  }

  openCancelarDialog(reserva: ReservaVeiculo, ): void {
    if (reserva.status.id === 3) {
      const tipo = 'Cancelar';
      const dialogRef = this.dialog.open(CancelarRejeitarReservaComponent, {
        data: { reserva, tipo },
        width: '400px'
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.dialogRef.close(true);
        }
      });
    } else {
      const dialogRef = this.dialog.open(CancelarReservaVeiculoComponent, {
        data: reserva,
        width: '350px'
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.alterarStatus(5);
        }
      });
    }
  }

  openEnviarAprovacaDialog(reserva: ReservaVeiculo, enviar: boolean): void {
    const dialogRef = this.dialog.open(EnviarAprovacaoReservaVeiculoComponent, {
      data: { reserva, enviar },
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.alterarStatus(result.enviar ? 2 : 1);
      }
    });
  }

  isUsuarioLogado() {
    return Boolean(this.auth.jwtPayLoad.id === this.reservaSelecionada.usuario.id);
  }

  alterarStatus(idStatus: number) {
    let dialogRef;
    if (idStatus === 2) {
      dialogRef = this.dialog.open(CarregamentoComponent, {
        disableClose: true,
        data: 'Enviando a reserva para aprovação...',
        width: '350px'
      });
    }
    return this.reservaVeiculoService.alterarStatus(this.reservaSelecionada.id, idStatus)
      .pipe(
        catchError(error => {
          if (idStatus === 2) {
            dialogRef.close();
          }
          if (error.status === 409) {
            const dialogRef2 = this.dialog.open(CompatibilidadeHorarioReservaAprovadaComponent, {
              data: {
                message: 'Não é possível enviar para aprovação, pois sua solicitação de reserva tem compatibilidade de horário com outra(s) reserva(s) aprovada(s).',
                reservas: error.error
              },
              width: '400px',
              maxHeight: '600px'
            });
            return throwError(error);
          } else {
            throw this.errorHandler.handle(error);
          }
        })
      )
      .subscribe(reserva => {
        this.dialogRef.close(true);
        if (idStatus === 1) {
          this.toastr.success('A reserva foi retirada da aprovação');
        } else if (idStatus === 2) {
          dialogRef.close();
          this.toastr.success('A reserva foi enviada para aprovação');
        } else {
          this.toastr.success('A reserva foi cancelada');
        }
      });
  }
}
