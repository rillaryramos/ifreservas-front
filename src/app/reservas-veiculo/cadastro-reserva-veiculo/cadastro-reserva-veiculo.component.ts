import { CarregamentoComponent } from './../../shared/carregamento/carregamento.component';
import { CompatibilidadeHorarioReservaAprovadaComponent } from './../../shared/compatibilidade-horario-reserva-aprovada/compatibilidade-horario-reserva-aprovada.component';
import { ErrorHandlerService } from './../../core/services/error-handler.service';
import { catchError } from 'rxjs/operators';
import { arrayHorarios } from './../../core/models/arrayHorarios';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ReservaVeiculo, Veiculo } from 'src/app/core/models/reserva-veiculo.model';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { HoraValidator, ConfirmValidParentMatcher } from 'src/app/core/validators/hora.validator';
import { ReservaVeiculoService } from 'src/app/core/services/reserva-veiculo.service';
import { ToastrService } from 'ngx-toastr';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-cadastro-reserva-veiculo',
  templateUrl: './cadastro-reserva-veiculo.component.html',
  styleUrls: ['./cadastro-reserva-veiculo.component.scss']
})
export class CadastroReservaVeiculoComponent {
  newReservaVeiculoFormGroup: FormGroup;
  confirmValidParentMatcher: ConfirmValidParentMatcher;
  minDateInicial: Date;
  minDate: Date;
  dataFinalMaiorInicial: boolean;
  horarios: any[] = arrayHorarios;

  constructor(
    public dialogRef: MatDialogRef<CadastroReservaVeiculoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ReservaVeiculo, private datePipe: DatePipe,
    private reservaVeiculoService: ReservaVeiculoService,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService,
    public dialog: MatDialog
  ) {
    this.newReservaVeiculoFormGroup = new FormGroup(
      {
        title: new FormControl(data.title, Validators.required),
        dataFormGroup: new FormGroup(
          {
            horaInicial: new FormControl(this.editando ? this.horarios.filter(re => re.valor === data.horaInicial)[0] : '', Validators.required),
            horaFinal: new FormControl(this.editando ? this.horarios.filter(re => re.valor === data.horaFinal)[0] : '', Validators.required),
            start: new FormControl(data.start),
            end: new FormControl(data.end, Validators.required),
          },
          { validators: HoraValidator.horaFinalMaior }
        ),
        saida: new FormControl(data.saida, Validators.required),
        destino: new FormControl(data.destino, Validators.required),
        motivo: new FormControl(data.motivo, Validators.required),
        idVeiculo: new FormControl(data.veiculo.id)
      }
    );
    this.confirmValidParentMatcher = new ConfirmValidParentMatcher();
    this.minDateInicial = new Date();
    this.minDate = data.start;
    this.dataFinalMaiorInicial = false;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get title(): AbstractControl {
    return this.newReservaVeiculoFormGroup.controls.title;
  }

  get start(): AbstractControl {
    return (<FormGroup>this.newReservaVeiculoFormGroup.controls.dataFormGroup).controls.start;
  }

  get end(): AbstractControl {
    return (<FormGroup>this.newReservaVeiculoFormGroup.controls.dataFormGroup).controls.end;
  }

  get horaInicial(): AbstractControl {
    return (<FormGroup>this.newReservaVeiculoFormGroup.controls.dataFormGroup).controls.horaInicial;
  }

  get horaFinal(): AbstractControl {
    return (<FormGroup>this.newReservaVeiculoFormGroup.controls.dataFormGroup).controls.horaFinal;
  }

  get saida(): AbstractControl {
    return this.newReservaVeiculoFormGroup.controls.saida;
  }

  get destino(): AbstractControl {
    return this.newReservaVeiculoFormGroup.controls.destino;
  }

  get motivo(): AbstractControl {
    return this.newReservaVeiculoFormGroup.controls.motivo;
  }

  get idVeiculo(): AbstractControl {
    return this.newReservaVeiculoFormGroup.controls.idVeiculo;
  }

  get editando() {
    return Boolean(this.data.id);
  }

  // teste(horario: string) {
  //   let val = '2019-10-09 '+horario;
  //   if(this.datePipe.transform(this.start.value, 'dd/MM/yyyy') === this.datePipe.transform(new Date(), 'dd/MM/yyyy')) {
  //     const ww = new Date(val);
  //     if(ww < new Date) {
  //       return true;
  //     }
  //   }
  // }

  salvar() {
    if (this.editando) {
      this.atualizarReserva();
    } else {
      this.adicionarReserva();
    }
  }

  adicionarReserva() {
    if (this.newReservaVeiculoFormGroup.valid) {
      const dialogRef1 = this.dialog.open(CarregamentoComponent, {
        disableClose: true,
        data: 'Salvando solicitação de reserva...',
        width: '350px'
      });
      this.reservaVeiculoService.createReserva({
        title: this.title.value,
        start: this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + 'T' + this.horaInicial.value.valor,
        end: this.datePipe.transform(this.end.value, 'yyyy-MM-dd') + 'T' + this.horaFinal.value.valor,
        saida: this.saida.value,
        destino: this.destino.value,
        motivo: this.motivo.value,
        veiculo: {
          id: this.idVeiculo.value
        }
      })
        .pipe(
          catchError(error => {
            dialogRef1.close();
            if (error.status === 409) {
              const dialogRef = this.dialog.open(CompatibilidadeHorarioReservaAprovadaComponent, {
                data: {
                  message: 'Sua solicitação de reserva tem compatibilidade de horário com outra(s) reserva(s) aprovada(s).',
                  reservas: error.error
                },
                width: '400px',
                maxHeight: '600px'
              });
              return throwError(error);
            } else {
              throw this.errorHandler.handle(error);
            }
          })
        )
        .subscribe(reserva => {
          dialogRef1.close();
          this.dialogRef.close(this.newReservaVeiculoFormGroup);
          this.toastr.success('Solicitação de reserva criada');
        });
    }
  }

  atualizarReserva() {
    if (this.newReservaVeiculoFormGroup.valid) {
      const dialogRef1 = this.dialog.open(CarregamentoComponent, {
        disableClose: true,
        data: 'Salvando solicitação de reserva...',
        width: '350px'
      });
      this.reservaVeiculoService.atualizarReserva({
        id: this.data.id,
        title: this.title.value,
        start: this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + 'T' + this.horaInicial.value.valor,
        end: this.datePipe.transform(this.end.value, 'yyyy-MM-dd') + 'T' + this.horaFinal.value.valor,
        saida: this.saida.value,
        destino: this.destino.value,
        motivo: this.motivo.value,
        veiculo: {
          id: this.idVeiculo.value
        }
      })
        .pipe(
          catchError(error => {
            dialogRef1.close();
            if (error.status === 409) {
              const dialogRef = this.dialog.open(CompatibilidadeHorarioReservaAprovadaComponent, {
                data: {
                  message: 'Sua solicitação de reserva tem compatibilidade de horário com outra(s) reserva(s) aprovada(s).',
                  reservas: error.error
                },
                width: '400px',
                maxHeight: '600px'
              });
              return throwError(error);
            } else {
              throw this.errorHandler.handle(error);
            }
          })
        )
        .subscribe(reserva => {
          dialogRef1.close();
          this.dialogRef.close(this.newReservaVeiculoFormGroup);
          this.toastr.success('A reserva foi atualizada');
        });
    }
  }

}
