import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-alerta-reserva-aberta',
  templateUrl: 'alerta-reserva-aberta.component.html',
})
export class AlertaReservaAbertaComponent {

  constructor(
    public dialogRef: MatDialogRef<AlertaReservaAbertaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

}
