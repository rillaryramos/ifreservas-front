import { Routes } from '@angular/router';

import { ReservasVeiculoComponent } from './reservas-veiculo.component';

export const ReservasVeiculoRoutes: Routes = [{
  path: '',
  component: ReservasVeiculoComponent
}];
