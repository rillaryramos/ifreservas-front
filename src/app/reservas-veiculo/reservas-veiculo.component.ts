import { AlertaReservaAbertaComponent } from './alerta-reserva-aberta/alerta-reserva-aberta.component';
import { Title } from '@angular/platform-browser';
import { Component, OnInit, AfterViewInit, ViewChild, Inject } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { MatDialog } from '@angular/material';
import { ReservaVeiculo } from '../core/models/reserva-veiculo.model';
import { CadastroReservaVeiculoComponent } from './cadastro-reserva-veiculo/cadastro-reserva-veiculo.component';
import { VisualizarReservaVeiculoComponent } from './visualizar-reserva-veiculo/visualizar-reserva-veiculo.component';
import { ReservaVeiculoService } from '../core/services/reserva-veiculo.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VeiculoService } from '../core/services/veiculo.service';
import { Veiculo } from '../core/models/veiculo.model';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

export interface DialogData {
  title: string;
  start: Date;
  end: Date;
  horaInicial: string;
  horaFinal: string;
  destino: string;
  motivo: string;
}

@Component({
  selector: 'app-reservas-veiculo',
  templateUrl: './reservas-veiculo.component.html',
  styleUrls: ['./reservas-veiculo.component.css']
})
export class ReservasVeiculoComponent implements OnInit {

  veiculos: Veiculo[];
  veiculoSelecionado: Veiculo;
  reservasVeiculo: ReservaVeiculo[];
  reservaSelecionada: ReservaVeiculo;
  firstFormGroup: FormGroup;
  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];
  calendarWeekends = true;
  calendarEvents: EventInput[];
  exibirPor = 'minhas';

  constructor(
    public dialog: MatDialog,
    private reservaVeiculoService: ReservaVeiculoService,
    private veiculoService: VeiculoService,
    private _formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private title: Title,
    private toastr: ToastrService
  ) { }

  @ViewChild('calendar') calendarComponent: FullCalendarComponent; // the #calendar in the template

  ngOnInit() {
    // this.calendarComponent.defaultDate = '2018-11-11';
    this.title.setTitle('Reservas de veículo');
    this.listarVeiculos();
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
  }

  aoSelecionar(evento) {
    this.veiculoSelecionado = evento;
    if (!this.veiculoSelecionado.ativo) {
      this.toastr.error('Veículo selecionado está indisponível');
    } else {
      this.toastr.clear();
    }
    this.firstFormGroup.controls.firstCtrl.setValue('Selecionado');
  }

  changeStep(arg) {
    if (arg.selectedIndex === 1) {
      this.listarReservasPorVeiculo();
    }
  }

  handleEventClick(arg) {
    const dialogRef = this.dialog.open(VisualizarReservaVeiculoComponent, {
      width: '420px',
      data: arg.event.id
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listarReservasPorVeiculo();
      }
    });
  }

  handleDateClick(arg) {
    this.toastr.clear();
    const dataAtual = new Date();
    if (this.veiculoSelecionado.ativo) {
      if ((arg.date > dataAtual) || (this.datePipe.transform(arg.date, 'dd/MM/yyyy') === this.datePipe.transform(dataAtual, 'dd/MM/yyyy'))) {
        const dialogRef = this.dialog.open(CadastroReservaVeiculoComponent, {
          width: '420px',
          data: {
            title: '',
            start: arg.date,
            end: arg.date,
            horaInicial: '',
            horaFinal: '',
            veiculo: {
              id: this.veiculoSelecionado.id
            }
          }
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            if (this.exibirPor === 'all') {
              const dialogRef = this.dialog.open(AlertaReservaAbertaComponent, {
                data: null,
                width: '400px'
              });
            }
            this.listarReservasPorVeiculo();
          }
        });
      } else {
        this.toastr.error('Só é possível solicitar uma reserva a partir do dia atual');
      }
    } else {
      this.toastr.error('Veículo selecionado está indisponível');
    }
  }

  listarVeiculos() {
    this.veiculoService.getVeiculos()
      .subscribe((veiculos: Veiculo[]) => {
        this.veiculos = veiculos;
      });
  }

  listarReservasPorVeiculo() {
    console.log('AQUI');
    this.reservaVeiculoService.getReservasByVeiculo(this.veiculoSelecionado.id, this.exibirPor)
      .subscribe((reservas: ReservaVeiculo[]) => {
        // reservas.map(re => re.status.id === 1 ? (re.backgroundColor = '#ec3244', re.borderColor = '#ec3244') : re.status.id === 2 ? (re.backgroundColor = 'orange', re.borderColor = 'orange') : (re.backgroundColor = '#3788d8', re.borderColor = '#3788d8'));
        reservas.map(re =>
          re.status.id === 2 ? (re.backgroundColor = 'orange', re.borderColor = 'orange') :
            re.status.id === 3 ? (re.backgroundColor = '#34bb34', re.borderColor = '#34bb34') :
              re.status.id === 1 ? (re.backgroundColor = '#3788d8', re.borderColor = '#3788d8') :
                re.status.id === 4 ? (re.backgroundColor = '#ec3244', re.borderColor = '#ec3244') :
                  re.status.id === 5 ? (re.backgroundColor = '#9d37d8', re.borderColor = '#9d37d8') :
                    (re.backgroundColor = '#ff4081', re.borderColor = '#ff4081')
        );
        this.calendarEvents = reservas;
      });
  }

  buscarReserva(id: number) {
    this.reservaVeiculoService.getReservaById(id)
      .subscribe(reserva => {
        this.reservaSelecionada = reserva;
      });
  }
}
