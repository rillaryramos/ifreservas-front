import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
    selector: 'enviar-aprovacao-reserva-veiculo',
    templateUrl: 'enviar-aprovacao-reserva-veiculo.component.html',
})
export class EnviarAprovacaoReservaVeiculoComponent {

    constructor(
        public dialogRef: MatDialogRef<EnviarAprovacaoReservaVeiculoComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {}

    onNoClick(): void {
        this.dialogRef.close();
    }

}