import { Title } from '@angular/platform-browser';
import { TurmaService } from 'src/app/core/services/turma.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cadastro-turma',
  templateUrl: './cadastro-turma.component.html',
  styleUrls: ['./cadastro-turma.component.scss']
})
export class CadastroTurmaComponent implements OnInit {
  formGroupTurma: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private turmaService: TurmaService,
    private title: Title,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.title.setTitle('Cadastro de turma');
    this.configurarForm();
    const idTurma = this.activatedRoute.snapshot.params['id'];
    if (idTurma) {
      this.buscarTurma(idTurma);
    }
  }

  configurarForm() {
    this.formGroupTurma = new FormGroup({
      id: new FormControl(null),
      nome: new FormControl(null, Validators.required),
      ativo: new FormControl(null)
    });
  }

  get id(): AbstractControl {
    return this.formGroupTurma.controls.id;
  }

  get editando() {
    return Boolean(this.id.value);
  }

  buscarTurma(id: number) {
    this.turmaService.getTurmaById(id)
      .subscribe(turma => {
        this.formGroupTurma.setValue(turma);
        this.atualizarTituloEdicao();
      });
  }

  atualizarTituloEdicao() {
    this.title.setTitle(`Edição de turma: ${this.formGroupTurma.get('nome').value}`);
  }

  salvar() {
    if (this.editando) {
      this.atualizarTurma();
    } else {
      this.adicionarTurma();
    }
  }

  adicionarTurma() {
    if (this.formGroupTurma.valid) {
      return this.turmaService.cadastrarTurma(this.formGroupTurma.value)
        .subscribe(motorista => {
          this.router.navigate(['/turmas']);
          this.toastr.success('A turma foi cadastrada');
        });
    }
  }

  atualizarTurma() {
    if (this.formGroupTurma.valid) {
      this.turmaService.atualizarTurma(this.formGroupTurma.value)
        .subscribe(motorista => {
          this.router.navigate(['/turmas']);
          this.toastr.success('A turma foi atualizada');
        });
    }
  }
}
