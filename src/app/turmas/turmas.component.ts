import { Title } from '@angular/platform-browser';
import { StatusComponent } from './status-turma/status-turma.component';
import { RemoverTurmaComponent } from './remover-turma/remover-turma.component';
import { Turma } from './../core/models/turma.model';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { TurmaService } from './../core/services/turma.service';
// import { Veiculo } from './../core/models/reserva-veiculo.model';
import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-turmas',
  templateUrl: './turmas.component.html',
  styleUrls: ['./turmas.component.scss']
})
export class TurmasComponent implements OnInit {

  selected = 'all';
  displayedColumns: string[] = ['id', 'nome', 'ativo', 'actions'];
  turmas = new MatTableDataSource<Turma>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('busca') busca: ElementRef;

  constructor(
    private turmaService: TurmaService,
    public dialog: MatDialog,
    private title: Title,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.title.setTitle('Turmas');
    this.listarTurmas();
  }

  ngAfterViewInit() {
    this.turmas.paginator = this.paginator;
  }

  listarTurmas() {
    this.turmaService.getTurmas()
      .subscribe((turmas: Turma[]) => {
        this.turmas.data = turmas;
      });
  }

  changeStatus(turma: Turma) {
    const novoStatus = !turma.ativo;
    this.turmaService.alterarStatus(turma.id, novoStatus).subscribe(() => {
      const acao = novoStatus ? 'ativada' : 'inativada';
      turma.ativo = novoStatus;
      this.toastr.success(`Turma ${acao} com sucesso`);
    });
  }

  filtroStatus() {
    const busca = this.busca.nativeElement.value.toLowerCase();
    this.turmas.filterPredicate = (turma: Turma, filter: string) => {
      return ((
        turma.ativo ? 'true' : 'false'
      )
        .includes(this.selected) || this.selected === 'all') && ((
          turma.nome
        )
          .toLowerCase()
          .includes(busca));
    };
    this.turmas.filter = this.selected + busca;
    if (this.turmas.paginator) {
      this.turmas.paginator.firstPage();
    }
  }

  openAtivacaoDialog(veiculo: Turma): void {
    const dialogRef = this.dialog.open(StatusComponent, {
      data: veiculo,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.changeStatus(result);
      }
    });
  }

  openExclusaoDialog(turma: Turma): void {
    const dialogRef = this.dialog.open(RemoverTurmaComponent, {
      data: turma,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(result);
      }
    });
  }

  excluir(turma: Turma) {
    this.turmaService.excluir(turma.id).subscribe(() => {
      this.listarTurmas();
      this.toastr.success('Turma excluída com sucesso');
    });
  }
}
