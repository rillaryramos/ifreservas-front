import { Turma } from './../../core/models/turma.model';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-remover-turma',
  templateUrl: 'remover-turma.component.html',
})
export class RemoverTurmaComponent {

  constructor(
    public dialogRef: MatDialogRef<RemoverTurmaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Turma) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
