import { Turma } from './../../core/models/turma.model';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-status-turma',
  templateUrl: 'status-turma.component.html',
})
export class StatusComponent {

  constructor(
    public dialogRef: MatDialogRef<StatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Turma) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
