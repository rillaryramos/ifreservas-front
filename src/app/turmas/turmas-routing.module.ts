import { AuthGuard } from './../seguranca/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TurmasComponent } from './turmas.component';
import { CadastroTurmaComponent } from './cadastro-turma/cadastro-turma.component';

const routes: Routes = [
  {
    path: '',
    component: TurmasComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  },
  {
    path: 'nova',
    component: CadastroTurmaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  },
  {
    path: ':id',
    component: CadastroTurmaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TurmasRoutingModule { }
