import { StatusComponent } from './status-turma/status-turma.component';
import { RemoverTurmaComponent } from './remover-turma/remover-turma.component';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { DemoMaterialModule } from '../demo-material-module';
import { TurmasRoutingModule } from './turmas-routing.module';

import { TurmasComponent } from './turmas.component';
import { CadastroTurmaComponent } from './cadastro-turma/cadastro-turma.component';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TurmasRoutingModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    NgxMaskModule
  ],
  providers: [],
  entryComponents: [
    RemoverTurmaComponent,
    StatusComponent
  ],
  declarations: [
    TurmasComponent,
    CadastroTurmaComponent,
    RemoverTurmaComponent,
    StatusComponent
  ]
})
export class TurmasModule {}
