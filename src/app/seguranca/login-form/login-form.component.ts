import { Title } from '@angular/platform-browser';
import { AuthNoticeService } from './../../core/services/auth/auth-notice.service';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import * as objectPath from 'object-path';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit, OnDestroy {
  formGroupLogin: FormGroup;
  errors: any = [];
  botaoHabilitado: boolean;

  constructor(
    private auth: AuthService,
    private router: Router,
    public authNoticeService: AuthNoticeService,
    private title: Title
  ) {
    this.botaoHabilitado = true;
   }

  ngOnInit(): void {
    this.title.setTitle('IFReservas | Login');
    this.configurarForm();
    if (!this.authNoticeService.onNoticeChanged$.getValue()) {
      this.authNoticeService.setNotice(null, 'success');
    }
  }

  configurarForm() {
    this.formGroupLogin = new FormGroup({
      login: new FormControl(null, Validators.required),
      senha: new FormControl(null, Validators.required)
    });
  }

  ngOnDestroy(): void {
    this.authNoticeService.setNotice(null);
  }

  login() {
    if (this.formGroupLogin.valid) {
      this.botaoHabilitado = false;
      this.authNoticeService.setNotice(null, 'success');
      this.auth.login(this.formGroupLogin.controls.login.value, this.formGroupLogin.controls.senha.value)
        .then(() => {
          this.botaoHabilitado = true;
          this.router.navigate(['/reservas-dependencias']);
        })
        .catch(erro => {
          this.botaoHabilitado = true;
          this.authNoticeService.setNotice(erro, 'error');
        });
    }
  }

}
