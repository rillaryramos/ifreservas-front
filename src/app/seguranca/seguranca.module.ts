import { SharedModule } from './../shared/shared.module';
import { AuthGuard } from './auth.guard';
import { environment } from './../../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SegurancaRoutingModule } from './seguranca-routing.module';
import { LoginFormComponent } from './login-form/login-form.component';
import { DemoMaterialModule } from '../demo-material-module';
import { JwtModule } from '@auth0/angular-jwt';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    SegurancaRoutingModule,
    SharedModule
  ],
  declarations: [
    LoginFormComponent,
    // AuthNoticeComponent
  ],
  providers: [
    AuthGuard
  ]
})
export class SegurancaModule { }
