import { Injectable } from '@angular/core';

import { AuthService } from './auth.service';
import { MoneyHttp } from './money-http';

@Injectable()
export class LogoutService {

  tokensRevokeUrl = 'http://localhost:8080/api/token/revoke/';

  constructor(
    private http: MoneyHttp,
    private auth: AuthService
  ) { }

  logout() {
    return this.http.delete(this.tokensRevokeUrl, { withCredentials: true })
      .toPromise()
      .then(() => {
        this.auth.limparAccessToken();
      });
  }

}
