import { CancelarRejeitarReservaComponent } from './../shared/cancelar-rejeitar-reserva/cancelar-rejeitar-reserva.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ReservaVeiculoService } from '../core/services/reserva-veiculo.service';
import { ReservaVeiculo } from '../core/models/reserva-veiculo.model';
import { MatDialog } from '@angular/material';
import { AprovarComponent } from './aprovar/aprovar.component';
import { Veiculo } from '../core/models/veiculo.model';
import { VeiculoService } from '../core/services/veiculo.service';

@Component({
  selector: 'app-aprovacao',
  templateUrl: './aprovacao.component.html',
  styleUrls: ['./aprovacao.component.scss']
})
export class AprovacaoComponent implements OnInit {

  veiculos: Veiculo[];
  veiculoSelecionado: Veiculo;
  reservas: ReservaVeiculo[];
  status = 2;
  currentPage: number = 0;
  lastPage = true;
  @ViewChild('filter') componentFilter;

  constructor(
    private reservaVeiculoService: ReservaVeiculoService,
    public dialog: MatDialog,
    private veiculoService: VeiculoService) { }

  ngOnInit() {
    this.listarVeiculos();
    this.listarReservasAprovacao();
  }

  alterarFiltro() {
    this.currentPage = 0;
    this.listarReservasAprovacao();
  }

  listarReservasAprovacao() {
    this.reservaVeiculoService.getReservasEmAprovacao(
      this.status, this.veiculoSelecionado ? this.veiculoSelecionado.id : null, this.currentPage
    )
      .subscribe((reservas: any) => {
        this.reservas = reservas.content;
        this.lastPage = reservas.last;
      });
  }

  listarVeiculos() {
    this.veiculoService.getVeiculos()
      .subscribe((veiculos: Veiculo[]) => {
        this.veiculos = veiculos;
      });
  }

  aoSelecionar(arg) {
    this.veiculoSelecionado = arg;
    this.alterarFiltro();
  }

  removerMotorista() {
    this.veiculoSelecionado = null;
    this.componentFilter.limpar();
    this.alterarFiltro();
  }

  openAprovacaoDialog(reserva: ReservaVeiculo): void {
    const dialogRef = this.dialog.open(AprovarComponent, {
      data: reserva,
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listarReservasAprovacao();
      }
    });
  }

  openReprovarDialog(reserva: ReservaVeiculo, tipo: string): void {
    const dialogRef = this.dialog.open(CancelarRejeitarReservaComponent, {
      data: { reserva, tipo },
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.listarReservasAprovacao();
      }
    });
  }

  maiorQueDataAtual(start) {
    if (new Date(start) < new Date()) {
      return false;
    } else {
      return true;
    }
  }

  previous() {
    this.currentPage -= 1;
    this.listarReservasAprovacao();
    // this.getReservations();
  }

  next() {
    this.currentPage += 1;
    this.listarReservasAprovacao();
    // this.getReservations();
  }

}
