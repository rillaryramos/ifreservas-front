import { CarregamentoComponent } from './../shared/carregamento/carregamento.component';
import { CancelarRejeitarReservaComponent } from './../shared/cancelar-rejeitar-reserva/cancelar-rejeitar-reserva.component';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule, DatePipe } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DemoMaterialModule } from '../demo-material-module';
import { AprovacaoRoutingModule } from './aprovacao-routing.module';
import { AprovacaoComponent } from './aprovacao.component';
import { AprovarComponent } from './aprovar/aprovar.component';
import { SharedModule } from '../shared/shared.module';
import { CompatibilidadeHorarioReservaVeiculoComponent } from './compatibilidade-horario-reserva-veiculo/compatibilidade-horario-reserva-veiculo.component';
import { CompatibilidadeHorarioReservaAprovadaComponent } from '../shared/compatibilidade-horario-reserva-aprovada/compatibilidade-horario-reserva-aprovada.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    AprovacaoRoutingModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    DatePipe
  ],
  entryComponents: [
    AprovarComponent,
    CancelarRejeitarReservaComponent,
    CompatibilidadeHorarioReservaVeiculoComponent,
    CompatibilidadeHorarioReservaAprovadaComponent,
    CarregamentoComponent
  ],
  declarations: [
    AprovacaoComponent,
    AprovarComponent,
    CompatibilidadeHorarioReservaVeiculoComponent
  ]
})
export class AprovacaoModule {}
