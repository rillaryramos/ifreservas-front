import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ReservaVeiculo } from 'src/app/core/models/reserva-veiculo.model';

@Component({
    selector: 'app-compatibilidade-horario-reserva-veiculo',
    templateUrl: 'compatibilidade-horario-reserva-veiculo.component.html',
    styleUrls: ['./compatibilidade-horario-reserva-veiculo.component.scss']
})
export class CompatibilidadeHorarioReservaVeiculoComponent {

    constructor(
        public dialogRef: MatDialogRef<CompatibilidadeHorarioReservaVeiculoComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ReservaVeiculo[]) {
        }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
