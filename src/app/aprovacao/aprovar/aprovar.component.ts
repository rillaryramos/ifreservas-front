import { CarregamentoComponent } from './../../shared/carregamento/carregamento.component';
import { ErrorHandlerService } from './../../core/services/error-handler.service';
import { arrayHorarios } from './../../core/models/arrayHorarios';
import { ConfirmValidMatcher } from './../../core/validators/hora.validator';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { MotoristaService } from 'src/app/core/services/motorista.service';
import { Motorista } from 'src/app/core/models/motorista.model';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { HoraValidator, ConfirmValidParentMatcher } from 'src/app/core/validators/hora.validator';
import { ReservaVeiculoService } from 'src/app/core/services/reserva-veiculo.service';
import { ReservaVeiculo, ReservasVeiculoParaReprovar } from 'src/app/core/models/reserva-veiculo.model';
import { CompatibilidadeHorarioReservaVeiculoComponent } from '../compatibilidade-horario-reserva-veiculo/compatibilidade-horario-reserva-veiculo.component';
import { DatePipe } from '@angular/common';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { CompatibilidadeHorarioReservaAprovadaComponent } from 'src/app/shared/compatibilidade-horario-reserva-aprovada/compatibilidade-horario-reserva-aprovada.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-aprovar-reserva-veiculo',
  templateUrl: 'aprovar.component.html',
  styleUrls: ['./aprovar.component.scss']
})
export class AprovarComponent implements OnInit {
  motoristas: Motorista[];
  dataFormGroup: FormGroup;
  horarios: any[] = arrayHorarios;
  minDateInicial: Date;
  confirmValidMatcher: ConfirmValidMatcher;
  reservasEmAprovacao: ReservaVeiculo[];
  alterar = false;
  idObjetoSelecionado: number;
  motorista: any;
  botoesDesabilitados: boolean;
  @ViewChild('filter') componentFilter;

  constructor(
    public dialogRef: MatDialogRef<AprovarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private motoristaService: MotoristaService,
    private reservaVeiculoService: ReservaVeiculoService,
    public dialog: MatDialog,
    private datePipe: DatePipe,
    private errorHandler: ErrorHandlerService,
    private toastr: ToastrService) {
    this.botoesDesabilitados = false;
    this.dataFormGroup = new FormGroup(
      {
        horaInicial: new FormControl(this.horarios.filter(re => re.valor === new Date(data.start).toTimeString().substring(0, 5))[0], Validators.required),
        horaFinal: new FormControl(this.horarios.filter(re => re.valor === new Date(data.end).toTimeString().substring(0, 5))[0], Validators.required),
        start: new FormControl(new Date(data.start), Validators.required),
        end: new FormControl(new Date(data.end), Validators.required)
      },
      { validators: HoraValidator.horaFinalMaior }
    );
    this.minDateInicial = new Date();
    this.confirmValidMatcher = new ConfirmValidMatcher();
  }

  ngOnInit() {
    if (this.alterando && this.data.motorista) {
      this.idObjetoSelecionado = this.data.motorista.id;
    }
    this.listarMotoristasAtivos();
  }

  get horaInicial(): AbstractControl {
    return this.dataFormGroup.controls.horaInicial;
  }

  get horaFinal(): AbstractControl {
    return this.dataFormGroup.controls.horaFinal;
  }

  get start(): AbstractControl {
    return this.dataFormGroup.controls.start;
  }

  get end(): AbstractControl {
    return this.dataFormGroup.controls.end;
  }

  get alterando() {
    return Boolean(this.data.status.id === 3);
  }

  mudancaAlterar() {
    if (this.alterar) {
      this.dataFormGroup.setControl('start', new FormControl(new Date(this.data.start), Validators.required));
      this.dataFormGroup.setControl('end', new FormControl(new Date(this.data.end), Validators.required));
      this.dataFormGroup.setControl('horaInicial', new FormControl(this.horarios.filter(re => re.valor === new Date(this.data.start).toTimeString().substring(0, 5))[0], Validators.required));
      this.dataFormGroup.setControl('horaFinal', new FormControl(this.horarios.filter(re => re.valor === new Date(this.data.end).toTimeString().substring(0, 5))[0], Validators.required));
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  listarMotoristasAtivos() {
    this.motoristaService.getMotoristasAtivos()
      .subscribe((motoristas: Motorista[]) => {
        this.motoristas = motoristas;
      });
  }

  removerMotorista() {
    this.motorista = null;
    this.componentFilter.limpar();
  }

  aoSelecionar(arg) {
    this.motorista = arg;
  }

  salvar() {
    if (this.alterando) {
      this.alterarMotorista();
    } else {
      this.verificarCompatibilidade();
    }
  }

  alterarMotorista() {
    // this.botoesDesabilitados = true;
    const dialogRef = this.dialog.open(CarregamentoComponent, {
      disableClose: true,
      data: 'Alterando motorista...',
      width: '350px'
    });
    this.reservaVeiculoService.alterarMotorista(this.data.id, this.motorista ? this.motorista.id : 'null')
      .pipe(
        catchError(error => {
          dialogRef.close();
          throw this.errorHandler.handle(error);
        })
      )
      .subscribe(reserva => {
        dialogRef.close();
        this.dialogRef.close(this.data);
        this.toastr.success('O motorista foi alterado');
      });
    // this.botoesDesabilitados = false;
  }

  verificarCompatibilidade() {
    if (!this.alterar || this.dataFormGroup.valid) {
      let va1 = this.data.start;
      let va2 = this.data.end;
      if (this.alterar) {
        va1 = this.datePipe.transform(this.start.value, 'yyyy-MM-dd') + 'T' + this.horaInicial.value.valor + ':00';
        va2 = this.datePipe.transform(this.end.value, 'yyyy-MM-dd') + 'T' + this.horaFinal.value.valor + ':00';
      }
      // this.botoesDesabilitados = true;
      this.reservaVeiculoService.verificarCompatibilidadeEmAprovacao(this.data.id, va1, va2)
        .pipe(
          catchError(error => {
            if (error.status === 409) {
              const dialogRef = this.dialog.open(CompatibilidadeHorarioReservaAprovadaComponent, {
                data: {
                  message: 'Há reserva(s) aprovada(s) com compatibilidade de horário com a reserva em questão.',
                  reservas: error.error
                },
                width: '400px'
              });
              return throwError(error);
            } else {
              throw this.errorHandler.handle(error);
            }
          })
        )
        .subscribe(reservas => {
          this.data.start = va1;
          this.data.end = va2;
          this.reservasEmAprovacao = reservas;
          if (this.reservasEmAprovacao.length > 0) {
            this.openDialog(this.reservasEmAprovacao);
          } else {
            this.aprovar({ reservaParaAprovar: this.data, reservasParaReprovar: [] });
          }
        });
    }
  }

  openDialog(reservas: ReservaVeiculo[]): void {
    const dialogRef = this.dialog.open(CompatibilidadeHorarioReservaVeiculoComponent, {
      data: reservas,
      width: '700px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.aprovar({ reservaParaAprovar: this.data, reservasParaReprovar: this.reservasEmAprovacao });
        //   this.dialogRef.close();
      } else {
        // this.dialogRef.close();
        this.botoesDesabilitados = false;
      }
    });
  }

  aprovar(reservasVeiculoParaReprovar: ReservasVeiculoParaReprovar) {
    reservasVeiculoParaReprovar.reservaParaAprovar.motorista = this.motorista;
    const dialogRef = this.dialog.open(CarregamentoComponent, {
      disableClose: true,
      data: 'Aprovando reserva...',
      width: '350px'
    });
    this.reservaVeiculoService.aprovarReserva(this.data.id, reservasVeiculoParaReprovar)
      .pipe(
        catchError(error => {
          dialogRef.close();
          throw this.errorHandler.handle(error);
        })
      )
      .subscribe(reserva => {
        dialogRef.close();
        this.dialogRef.close(this.data);
        this.toastr.success('A reserva foi aprovada');
      });
    this.botoesDesabilitados = false;
  }

}
