import { AuthGuard } from './../seguranca/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MotoristasComponent } from './motoristas.component';
import { CadastroMotoristaComponent } from './cadastro-motorista/cadastro-motorista.component';

const routes: Routes = [
  {
    path: '',
    component: MotoristasComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER', 'ROLE_APPROVER'] }
  },
  {
    path: 'novo',
    component: CadastroMotoristaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER', 'ROLE_APPROVER'] }
  },
  {
    path: ':id',
    component: CadastroMotoristaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER', 'ROLE_APPROVER'] }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MotoristasRoutingModule { }
