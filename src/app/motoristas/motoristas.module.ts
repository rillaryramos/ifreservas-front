import { RemoverMotoristaComponent } from './remover-motorista/remover-motorista.component';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { DemoMaterialModule } from '../demo-material-module';
import { MotoristasRoutingModule } from './motoristas-routing.module';
import { MotoristasComponent } from './motoristas.component';
import { CadastroMotoristaComponent } from './cadastro-motorista/cadastro-motorista.component';
import { StatusComponent } from './status-motorista/status-motorista.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MotoristasRoutingModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    NgxMaskModule
  ],
  providers: [],
  entryComponents: [
    StatusComponent,
    RemoverMotoristaComponent
  ],
  declarations: [
    MotoristasComponent,
    CadastroMotoristaComponent,
    StatusComponent,
    RemoverMotoristaComponent
  ]
})
export class MotoristasModule {}
