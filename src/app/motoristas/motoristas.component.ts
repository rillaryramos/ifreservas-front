import { Title } from '@angular/platform-browser';
import { RemoverMotoristaComponent } from './remover-motorista/remover-motorista.component';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';

import { Motorista } from '../core/models/motorista.model';
import { StatusComponent } from './status-motorista/status-motorista.component';
import { FiltroComponent } from '../shared/filtro/filtro.component';
import { Filtro } from '../core/models/filtro.model';
import { MotoristaService } from '../core/services/motorista.service';
import { SnackbarService } from '../core/services/snackbar.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-motoristas',
  templateUrl: './motoristas.component.html',
  styleUrls: ['./motoristas.component.scss']
})
export class MotoristasComponent implements OnInit, AfterViewInit {

  selected = 'all';
  filtro: Filtro;
  displayedColumns: string[] = ['codigo', 'nome', 'sobrenome', 'cpf', 'ativo', 'actions'];
  motoristas = new MatTableDataSource<Motorista>();

  constructor(
    private motoristaService: MotoristaService,
    public dialog: MatDialog,
    private title: Title,
    private toastr: ToastrService
  ) {
    this.filtro = { filtros: ['Nome', 'Sobrenome', 'CPF'], filtroSelecionado: 'Nome' };
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('busca') busca: ElementRef;
  @ViewChild('buscaCpf') buscaCpf: ElementRef;

  ngOnInit() {
    this.title.setTitle('Motoristas');
    this.listarMotoristas();
  }

  ngAfterViewInit() {
    this.motoristas.paginator = this.paginator;
  }

  listarMotoristas() {
    this.motoristaService.getMotoristas()
      .subscribe((motoristas: Motorista[]) => {
        this.motoristas.data = motoristas;
      });
  }

  changeStatus(motorista: Motorista) {
    const novoStatus = !motorista.ativo;
    this.motoristaService.changeStatus(motorista.id, novoStatus).subscribe(() => {
      const acao = novoStatus ? 'ativada' : 'inativada';
      motorista.ativo = novoStatus;
      this.toastr.success(`Motorista ${acao}(o) com sucesso`);
    });

  }

  openAtivacaoDialog(motorista: Motorista): void {
    const dialogRef = this.dialog.open(StatusComponent, {
      data: motorista,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.changeStatus(result);
      }
    });
  }

  openExclusaoDialog(motorista: Motorista): void {
    const dialogRef = this.dialog.open(RemoverMotoristaComponent, {
      data: motorista,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(result);
      }
    });
  }

  openFiltroDialog() {
    const dialogRef = this.dialog.open(FiltroComponent, {
      data: this.filtro,
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.filtro.filtroSelecionado = result ? result : this.filtro.filtroSelecionado;
      if (this.filtro.filtroSelecionado !== 'CPF') {
        this.buscaCpf.nativeElement.value = '';
      } else {
        this.busca.nativeElement.value = '';
      }
      this.filtroStatus();
    });
  }

  filtroStatus() {
    const busca = this.filtro.filtroSelecionado !== 'CPF' ?
      this.busca.nativeElement.value.toLowerCase() : this.buscaCpf.nativeElement.value.replace(/[\-\.-]/g, '');
    this.motoristas.filterPredicate = (motorista: Motorista, filter: string) => {
      return ((
        motorista.ativo ? 'true' : 'false'
      )
        .includes(this.selected) || this.selected === 'all') && ((
          this.filtro.filtroSelecionado === 'Nome' ? motorista.nome :
            this.filtro.filtroSelecionado === 'Sobrenome' ? motorista.sobrenome : motorista.cpf
        )
          .toLowerCase()
          .includes(busca));
    };
    this.motoristas.filter = this.selected + busca;
    if (this.motoristas.paginator) {
      this.motoristas.paginator.firstPage();
    }
  }

  excluir(motorista: Motorista) {
    this.motoristaService.excluir(motorista.id).subscribe(() => {
      this.listarMotoristas();
      this.toastr.success('Motorista excluído com sucesso');
    });
  }

}
