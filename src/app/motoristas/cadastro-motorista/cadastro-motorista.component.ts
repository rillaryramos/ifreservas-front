import { GenericValidator } from './../../core/validators/cpf.validator';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MotoristaService } from 'src/app/core/services/motorista.service';
import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cadastro-motorista',
  templateUrl: './cadastro-motorista.component.html',
  styleUrls: ['./cadastro-motorista.component.scss']
})
export class CadastroMotoristaComponent implements OnInit {
  formGroupMotorista: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private motoristaService: MotoristaService,
    private title: Title,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.title.setTitle('Cadastro de motorista');
    this.configurarForm();
    const idMotorista = this.activatedRoute.snapshot.params['id'];
    if (idMotorista) {
      this.buscarMotorista(idMotorista);
    }
  }

  configurarForm() {
    this.formGroupMotorista = new FormGroup({
      id: new FormControl(null),
      nome: new FormControl(null, Validators.required),
      sobrenome: new FormControl(null, Validators.required),
      cpf: new FormControl(null, [ Validators.required, GenericValidator.isValidCpf() ]),
      ativo: new FormControl(null)
    });
  }

  get id(): AbstractControl {
    return this.formGroupMotorista.controls.id;
  }

  get cpf(): AbstractControl {
    return this.formGroupMotorista.controls.cpf;
  }

  get editando() {
    return Boolean(this.id.value);
  }

  buscarMotorista(id: number) {
    this.motoristaService.getMotoristaById(id)
      .subscribe(motorista => {
        this.formGroupMotorista.setValue(motorista);
        this.atualizarTituloEdicao();
      });
  }

  atualizarTituloEdicao() {
    this.title.setTitle(`Edição de motorista: ${this.formGroupMotorista.get('nome').value}`);
  }

  salvar() {
    if (this.editando) {
      this.atualizarMotorista();
    } else {
      this.adicionarMotorista();
    }
  }

  adicionarMotorista() {
    if (this.formGroupMotorista.valid) {
      return this.motoristaService.createMotorista(this.formGroupMotorista.value)
      .subscribe(motorista => {
        this.router.navigate(['/motoristas']);
        this.toastr.success('O motorista foi cadastrado');
      });
    }
  }

  atualizarMotorista() {
    if (this.formGroupMotorista.valid) {
      this.motoristaService.updateMotorista(this.formGroupMotorista.value)
      .subscribe(motorista => {
        this.router.navigate(['/motoristas']);
        this.toastr.success('O motorista foi atualizado');
      });
    }
  }
}
