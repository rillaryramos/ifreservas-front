import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Motorista } from 'src/app/core/models/motorista.model';

@Component({
  selector: 'app-remover-motorista',
  templateUrl: 'remover-motorista.component.html',
})
export class RemoverMotoristaComponent {

  constructor(
    public dialogRef: MatDialogRef<RemoverMotoristaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Motorista) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
