import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Motorista } from 'src/app/core/models/motorista.model';

@Component({
  selector: 'status-motorista',
  templateUrl: 'status-motorista.component.html',
})
export class StatusComponent {

  constructor(
    public dialogRef: MatDialogRef<StatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Motorista) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
