import { RecuperacaoSenhaComponent } from './recuperacao-senha.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: RecuperacaoSenhaComponent,
  },
  {
    path: ':token',
    component: RecuperacaoSenhaComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecuperacaoSenhaRoutingModule { }
