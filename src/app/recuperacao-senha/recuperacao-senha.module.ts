import { SharedModule } from './../shared/shared.module';
import { RecuperacaoSenhaRoutingModule } from './recuperacao-senha-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoMaterialModule } from './../demo-material-module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecuperacaoSenhaComponent } from './recuperacao-senha.component';

@NgModule({
  declarations: [
    RecuperacaoSenhaComponent
  ],
  imports: [
    CommonModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
    // RecuperacaoSenhaRoutingModule
  ]
})
export class RecuperacaoSenhaModule { }
