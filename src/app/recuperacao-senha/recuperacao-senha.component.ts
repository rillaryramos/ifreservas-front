import { Title } from '@angular/platform-browser';
import { ConfirmValidator, ConfirmValidParentMatcherSenha } from './../core/validators/confirm.validator';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorHandlerService } from './../core/services/error-handler.service';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthNoticeService } from './../core/services/auth/auth-notice.service';
import { UsuarioService } from 'src/app/core/services/usuario.service';
import { FormControl, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recuperacao',
  templateUrl: './recuperacao-senha.component.html',
  styleUrls: ['./recuperacao-senha.component.css']
})
export class RecuperacaoSenhaComponent implements OnInit {
  formGroupAlteracao: FormGroup;
  formGroupRedefinicao: FormGroup;
  tokenExpirado: boolean;
  confirmValidParentMatcher: ConfirmValidParentMatcherSenha;
  emailRecuperacao: string;
  botaoHabilitado: boolean;

  constructor(
    private usuarioService: UsuarioService,
    public authNoticeService: AuthNoticeService,
    private errorHandler: ErrorHandlerService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private title: Title
  ) {
    this.botaoHabilitado = true;
    this.confirmValidParentMatcher = new ConfirmValidParentMatcherSenha();
   }

  ngOnInit() {
    this.title.setTitle('IFReservas | Recuperação de senha');
    this.configurarForm();
    if (!this.authNoticeService.onNoticeChanged$.getValue()) {
      this.authNoticeService.setNotice(null, 'success');
    }
    const token = this.activatedRoute.snapshot.params['token'];
    if (token) {
      this.validarToken(token);
    }
  }

  configurarForm() {
    this.formGroupAlteracao = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email])
    });
    this.formGroupRedefinicao = new FormGroup({
      novaSenha: new FormControl('', [Validators.required, Validators.minLength(6)]),
      confirmacaoSenha: new FormControl('', Validators.required)
    },
    { validators: ConfirmValidator.childrenEqual });
  }

  get novaSenha(): AbstractControl {
    return this.formGroupRedefinicao.controls.novaSenha;
  }

  get confirmacaoSenha(): AbstractControl {
    return this.formGroupRedefinicao.controls.confirmacaoSenha;
  }

  get editando() {
    return Boolean(this.activatedRoute.snapshot.params['token']);
  }

  ngOnDestroy(): void {
    this.authNoticeService.setNotice(null);
  }

  recuperar() {
    this.authNoticeService.setNotice(null, 'success');
    if (this.formGroupAlteracao.valid) {
      this.botaoHabilitado = false;
      this.usuarioService.enviarLinkParaAlteracaoDeSenha(this.formGroupAlteracao.controls.email.value)
      .pipe(
        catchError(err => {
          this.botaoHabilitado = true;
          if (err.status === 404) {
            this.authNoticeService.setNotice('E-mail informado não encontrado', 'error');
            return throwError(err.statusText);
          } else {
            throw this.errorHandler.handle(err);
          }
        })
      )
      .subscribe(() => {
        this.botaoHabilitado = true;
        this.authNoticeService.setNotice('Link para alteração de senha enviado para o e-mail', 'success');
      });
    }
  }

  validarToken(token) {
    this.usuarioService.validarToken(token)
      .pipe(
        catchError(err => {
          if (err.status === 404) {
            this.tokenExpirado = true;
            return throwError(err.statusText);
          } else {
            throw this.errorHandler.handle(err);
          }
        })
      )
      .subscribe(retorno => {
        this.emailRecuperacao = retorno.email;
      });
  }

  redefinirSenha() {
    if (this.formGroupRedefinicao.valid) {
      this.botaoHabilitado = false;
      this.usuarioService.redefinirSenha(this.activatedRoute.snapshot.params['token'], this.emailRecuperacao, this.novaSenha.value)
      .pipe(
        catchError(err => {
          this.botaoHabilitado = true;
          if (err.status === 404) {
            this.authNoticeService.setNotice('Sua solicitação para redefinir a senha expirou ou o link já foi usado.', 'error');
            return throwError(err.statusText);
          } else {
            throw this.errorHandler.handle(err);
          }
        })
      )
      .subscribe(() => {
        this.botaoHabilitado = true;
        this.authNoticeService.setNotice('Senha redefinida com sucesso', 'success');
      });
      // this.botaoHabilitado = true;
    }
  }

  voltar() {
    this.router.navigateByUrl('/login');
  }

}
