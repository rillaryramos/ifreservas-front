import { RemoverDependenciaComponent } from './remover-dependencia/remover-dependencia.component';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { DemoMaterialModule } from '../demo-material-module';
import { DependenciasRoutingModule } from './dependencias-routing.module';

import { DependenciasComponent } from './dependencias.component';
import { CadastroDependenciaComponent } from './cadastro-dependencia/cadastro-dependencia.component';
import { StatusComponent } from './status-dependencia/status-dependencia.component';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    DependenciasRoutingModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    NgxMaskModule
  ],
  providers: [],
  entryComponents: [
    RemoverDependenciaComponent,
    StatusComponent
  ],
  declarations: [
    DependenciasComponent,
    CadastroDependenciaComponent,
    RemoverDependenciaComponent,
    StatusComponent
  ]
})
export class DependenciasModule {}
