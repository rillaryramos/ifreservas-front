import { Title } from '@angular/platform-browser';
import { Filtro } from './../core/models/filtro.model';
import { FiltroComponent } from './../shared/filtro/filtro.component';
import { RemoverDependenciaComponent } from './remover-dependencia/remover-dependencia.component';
import { DependenciaService } from './../core/services/dependencia.service';
import { Dependencia } from './../core/models/dependencia.model';
import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { StatusComponent } from './status-dependencia/status-dependencia.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dependencias',
  templateUrl: './dependencias.component.html',
  styleUrls: ['./dependencias.component.scss']
})
export class DependenciasComponent implements OnInit {

  selected = 'all';
  filtro: Filtro;
  displayedColumns: string[] = ['id', 'nome', 'bloco', 'ativo', 'actions'];
  dependencias = new MatTableDataSource<Dependencia>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('busca') busca: ElementRef;

  constructor(
    private dependenciaService: DependenciaService,
    public dialog: MatDialog,
    private title: Title,
    private toastr: ToastrService
  ) {
    this.filtro = { filtros: ['Nome', 'Bloco'], filtroSelecionado: 'Nome' };
  }

  ngOnInit() {
    this.title.setTitle('Dependências');
    this.listarDependencias();
  }

  ngAfterViewInit() {
    this.dependencias.paginator = this.paginator;
  }

  listarDependencias() {
    this.dependenciaService.getDependencias()
      .subscribe((dependencias: Dependencia[]) => {
        this.dependencias.data = dependencias;
      });
  }

  changeStatus(dependencia: Dependencia) {
    const novoStatus = !dependencia.ativo;
    this.dependenciaService.alterarStatus(dependencia.id, novoStatus).subscribe(() => {
      const acao = novoStatus ? 'ativada' : 'inativada';
      dependencia.ativo = novoStatus;
      this.toastr.success(`Dependência ${acao} com sucesso`);
    });

  }

  openFiltroDialog() {
    const dialogRef = this.dialog.open(FiltroComponent, {
      data: this.filtro,
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.filtro.filtroSelecionado = result ? result : this.filtro.filtroSelecionado;
      this.filtroStatus();
    });
  }

  filtroStatus() {
    const busca = this.busca.nativeElement.value.toLowerCase();
    this.dependencias.filterPredicate = (dependencia: Dependencia, filter: string) => {
      return ((
        dependencia.ativo ? 'true' : 'false'
      )
        .includes(this.selected) || this.selected === 'all') && ((
          this.filtro.filtroSelecionado === 'Nome' ? dependencia.nome : dependencia.bloco.nome
        )
          .toLowerCase()
          .includes(busca));
    };
    this.dependencias.filter = this.selected + busca;
    if (this.dependencias.paginator) {
      this.dependencias.paginator.firstPage();
    }
  }

  openAtivacaoDialog(dependencia: Dependencia): void {
    const dialogRef = this.dialog.open(StatusComponent, {
      data: dependencia,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
        this.changeStatus(result);
      }
    });
  }

  openExclusaoDialog(dependencia: Dependencia): void {
    const dialogRef = this.dialog.open(RemoverDependenciaComponent, {
      data: dependencia,
      width: '350px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(result);
      }
    });
  }

  excluir(dependencia: Dependencia) {
    this.dependenciaService.excluir(dependencia.id).subscribe(() => {
      this.listarDependencias();
      this.toastr.success('Dependência excluída com sucesso');
    });
  }
}
