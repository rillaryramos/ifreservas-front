import { Dependencia } from './../../core/models/dependencia.model';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-status-dependencia',
  templateUrl: 'status-dependencia.component.html',
})
export class StatusComponent {

  constructor(
    public dialogRef: MatDialogRef<StatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Dependencia) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
