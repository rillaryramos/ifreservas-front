import { Dependencia } from './../../core/models/dependencia.model';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-remover-dependencia',
  templateUrl: 'remover-dependencia.component.html',
})
export class RemoverDependenciaComponent {

  constructor(
    public dialogRef: MatDialogRef<RemoverDependenciaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Dependencia) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
