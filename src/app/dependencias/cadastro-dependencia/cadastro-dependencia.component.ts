import { Bloco } from './../../core/models/bloco.model';
import { BlocoService } from './../../core/services/bloco.service';
import { DependenciaService } from './../../core/services/dependencia.service';
import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { SnackbarService } from 'src/app/core/services/snackbar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cadastro-dependencia',
  templateUrl: './cadastro-dependencia.component.html',
  styleUrls: ['./cadastro-dependencia.component.scss']
})
export class CadastroDependenciaComponent implements OnInit {
  formGroupDependencia: FormGroup;
  blocos: Bloco[];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dependenciaService: DependenciaService,
    private title: Title,
    private blocoService: BlocoService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.title.setTitle('Cadastro de dependência');
    this.configurarForm();
    const idDependencia = this.activatedRoute.snapshot.params['id'];
    if (idDependencia) {
      // (<FormGroup>this.formGroupDependencia.controls.bloco)
      // .setControl('id', new FormControl({ value: null, disabled: true }));
      this.buscarDependencia(idDependencia);
    }
    this.carregarBlocos();
  }

  configurarForm() {
    this.formGroupDependencia = new FormGroup({
      id: new FormControl(null),
      nome: new FormControl(null, Validators.required),
      bloco: new FormGroup({
        id: new FormControl (null, Validators.required),
        nome: new FormControl()
      }),
      ativo: new FormControl(null)
    });
  }

  get id(): AbstractControl {
    return this.formGroupDependencia.controls.id;
  }

  get editando() {
    return Boolean(this.id.value);
  }

  buscarDependencia(id: number) {
    this.dependenciaService.getDependenciaById(id)
      .subscribe(dependencia => {
        this.formGroupDependencia.patchValue(dependencia);
        this.atualizarTituloEdicao();
      });
  }

  atualizarTituloEdicao() {
    this.title.setTitle(`Edição de dependência: ${this.formGroupDependencia.get('nome').value}`);
  }

  salvar() {
    if (this.editando) {
      this.atualizarDependencia();
    } else {
      this.adicionarDependencia();
    }
  }

  adicionarDependencia() {
    if (this.formGroupDependencia.valid) {
      return this.dependenciaService.cadastrarDependencia(this.formGroupDependencia.value)
      .subscribe(motorista => {
        this.router.navigate(['/dependencias']);
        this.toastr.success('A dependência foi cadastrada');
      });
    }
  }

  atualizarDependencia() {
    if (this.formGroupDependencia.valid) {
      this.dependenciaService.atualizarDependencia(this.formGroupDependencia.value)
      .subscribe(dependencia => {
        this.router.navigate(['/dependencias']);
        this.toastr.success('A dependência foi atualizada');
      });
    }
  }

  carregarBlocos() {
    return this.blocoService.getBlocos()
      .subscribe(blocos => {
        this.blocos = blocos;
      });
  }
}
