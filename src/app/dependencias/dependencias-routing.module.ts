import { AuthGuard } from './../seguranca/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DependenciasComponent } from './dependencias.component';
import { CadastroDependenciaComponent } from './cadastro-dependencia/cadastro-dependencia.component';

const routes: Routes = [
  {
    path: '',
    component: DependenciasComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  },
  {
    path: 'nova',
    component: CadastroDependenciaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  },
  {
    path: ':id',
    component: CadastroDependenciaComponent,
    canActivate: [AuthGuard],
    data: { roles: ['ROLE_MASTER'] }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DependenciasRoutingModule { }
